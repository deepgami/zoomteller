//
//  AppDelegate.swift
//  ZoomTeller
//
//  Created by ZERONES on 16/03/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import LGSideMenuController
import StatusAlert
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import FirebaseAuth
import GoogleSignIn
import UserNotifications
import TwitterKit
import GoogleMaps
import GooglePlaces
import NetCorePush
import UserNotificationsUI
import StoreKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate , GIDSignInDelegate, NVActivityIndicatorViewable  {
    
    var window: UIWindow?
    var dotsView: NVActivityIndicatorView!
    var googleSignInClientKey = "96848875785-imv5p4p9jq4786at4n0hvid3d2760i15.apps.googleusercontent.com"
    var deviceToken: String = "121212121212121212"
    //var googleApiKey =  "AIzaSyAs85RaUPwU0ahI-xEARVO1bz1Tvfn0xIU"
    var googleApiKey =  "AIzaSyANgFgiEUvsqAnm0rY6aJpX5HbKJglLRsM"
    var locationManager : CLLocationManager!
    var currentLocation : CLLocation!
    
    
//    let inAppPurchaseKeyThreeMonth = "com.zoomteller.threemonth"
//    let inAppPurchaseKeySixMonth = "com.zoomteller.sixmonth"
//    let inAppPurchaseKeyOneYear = "com.zoomteller.oneyear"
    //let inAppPurchaseKeyOneMonth = "com.zoomteller.onemonth"
    
    let inAppPurchaseKeyTwoMonth = "com.zoomteller.fulltwo"
    let inAppPurchaseKeyFourMonth = "com.zoomteller.fullfour"
    let inAppPurchaseKeySixMonth = "com.zoomteller.fullsix"
    let inAppPurchaseKeyOneYear = "com.zoomteller.fulloneyear"
    var T_consumerKEy = "X3ds5TG7m6sfI8SKtZEanb2Ti"
    var T_consumerSecretKEy = "pmXx7C8Ml961nbwC1ZYl04PtWlS1FwQuXcj61IrIN5HSKGfun2"
    
    var strFilterMinimumDispense : String = ""
    var strFilterMaximumDispense : String = ""
    var strFilterDistance : String = "500"
    var strFilterBank : String = ""
    var strFilterBankCode : String = ""
    var strTemp : String = ""
    let kSmartechAppID = "e9c5e07658f3ee471fe7ed848afcdfde"
    
    var product_id :String!
    var arrPlanList = Set(["com.zoomteller.fulltwo","com.zoomteller.fullfour",
                           "com.zoomteller.fullsix","com.zoomteller.fulloneyear" ])
    
    var dicStorePlanPrice = [String:String]()
    var planFetchCount = 0
    var isFetchProductHudShow = false
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        DispatchQueue.main.async {
            self.checkLoginStatus()
        }
        
        if appDelegate.dicStorePlanPrice.count < 4 {
            DispatchQueue.main.async {
                self.isFetchProductHudShow = false
                self.fatchProductList()
            }
        }
        
        NetCoreSharedManager.sharedInstance().handleApplicationLaunchEvent(launchOptions, forApplicationId: kSmartechAppID)
        NetCorePushTaskManager.sharedInstance().delegate = self
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
        
        GMSServices.provideAPIKey(googleApiKey)
        GMSPlacesClient.provideAPIKey(googleApiKey)
        
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = googleSignInClientKey
        GIDSignIn.sharedInstance().delegate = self
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        TWTRTwitter.sharedInstance().start(withConsumerKey:T_consumerKEy, consumerSecret:T_consumerSecretKEy)
        
        
        
        UserDefaults.standard.set(true, forKey: "isShowSecondTab")
        
        
        let appGroup = "group.com.zoomteller"
        NetCoreSharedManager.sharedInstance().setUpAppGroup(appGroup)
        
        let netCore_AppID = "e9c5e07658f3ee471fe7ed848afcdfde"
        NetCoreSharedManager.sharedInstance().handleApplicationLaunchEvent(launchOptions, forApplicationId: netCore_AppID)
        
        //set up push delegate
        NetCorePushTaskManager.sharedInstance().delegate = self as? NetCorePushTaskManagerDelegate
        
        
        //
        // self.registerForPushNotifications()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if nsuserDefault.getIsLoggin() {
            self.paidSubscriptionActiveData()
            self.locationEnableToCallVC()
        }
        self.getInitialLocationPopup()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        NetCorePushTaskManager.sharedInstance().didReceiveRemoteNotification(userInfo)
        
    }
    
    // MARK: - didReceiveLocalNotification method
    func application(_ application: UIApplication, didReceive notification: UILocalNotification){
        NetCorePushTaskManager.sharedInstance().didReceiveLocalNotification(notification.userInfo)
    }
    
    //MARK: - Push Notification Methods
    //MARK: -
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        self.deviceToken = token
        USERDEFAULT.set(token, forKey: "deviceToken")
        USERDEFAULT.synchronize()
        print("Device Token: \(token)")
        
        // Register device token with third party SDK as per their document
        //Identity must be “”(blank) or as per Primary key which defined on smartech Panel
        //NetCoreInstallation.sharedInstance().netCorePushRegisteration("deep.gami07@gmail.com", withDeviceToken: deviceToken) { (status) in }
        //NetCoreSharedManager.sharedInstance()?.printDeviceToken()
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
        
        //        if(chatClass != nil)
        //        {
        //            chatClass?.getChatData()
        //        }
        let aps = notification.request.content.userInfo[AnyHashable("aps")] as? NSDictionary
        
        if let name = (aps?["notification_type"] as? String)
        {
            print(name)
            
            if(name=="cancel" || name=="pendding")
            {
                //                if(homeClass != nil)
                //                {
                //                    homeClass?.getRideRequest()
                //                }
            }
        }
    }
    
    //MARK: - CUSTOM METHODS
    //MARK: -
    
    func checkLoginStatus ()
    {
        if nsuserDefault.getIsLoggin() {
            
            if nsuserDefault.getIsRemember() {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller : LGSideMenuController = storyboard.instantiateViewController(withIdentifier: "SP_Main") as! LGSideMenuController
                controller.sideMenuController?.hideLeftViewAnimated()
                let nav : UINavigationController = UINavigationController(rootViewController: controller)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav;
            }
            else
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller : LGSideMenuController = storyboard.instantiateViewController(withIdentifier: "SP_Main") as! LGSideMenuController
                controller.sideMenuController?.hideLeftViewAnimated()
                let nav : UINavigationController = UINavigationController(rootViewController: controller)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav;
            }
            self.paidSubscriptionActiveData()
        }
        else
        {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller : LoginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            
            let nav : UINavigationController = UINavigationController(rootViewController: controller)
            nav.setNavigationBarHidden(true, animated: false)
            nav.navigationBar.isHidden = true
            self.window?.rootViewController = nav;
            self.getInitialLocation()
            
        }
    }
    
    //MARK: - sharedAppDelegate
    //MARK: -
    
    class func sharedAppDelegate() -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    //MARK: - Toast Methods
    //MARK: -
    
    func showToast(title: String, message: String){
        let statusAlert = StatusAlert()
        statusAlert.title = title
        statusAlert.message = message
        statusAlert.canBePickedOrDismissed = true
        statusAlert.alertShowingDuration = 4
        
        // Presenting created instance
        statusAlert.showInKeyWindow()
    }
    
    //MARK: - HUD Methods
    //MARK: -
    
    func showHUD(){
        
        DispatchQueue.main.async {
            
            for view in (self.window?.subviews)!
            {
                if view .isKind(of: NVActivityIndicatorView.self)
                {
                    view.removeFromSuperview()
                }
            }//AMDots(frame:
            
            self.dotsView = NVActivityIndicatorView(frame: UIScreen.main.bounds, type: .ballPulseSync, color: UIColor.init(white: 1, alpha: 0.9), padding: 150.0)
            self.dotsView.backgroundColor = UIColor.init(white: 0.5, alpha: 0.7)
            self.dotsView.center = (self.window?.rootViewController?.view.center)!
            self.window?.addSubview(self.dotsView)
            
            self.dotsView.startAnimating()
        }
    }
    
    func hideHUD(){
        if self.dotsView != nil {
            self.dotsView.stopAnimating()
            self.dotsView.removeFromSuperview()
        }else {
            print("nil")
        }
        //self.dotsView = nil
    }
    
    //MARK: - Reachability
    //Mark: -
    
    func connected() -> Bool
    {
        return Reachability.isConnectedToNetwork()
    }
    
    //MARK: - App Skip
    //MARK: -
    
    func isSkipUser() -> String? {
        if nsuserDefault.getSkipUserDetails()?.userId == nil || nsuserDefault.getSkipUserDetails()?.userId == "" {
            return nil
        }
        return nsuserDefault.getSkipUserDetails()?.userId ?? nil
    }
    
    
    func appSkipAlert(vc viewContrller : UIViewController) {
        let msg = "You need to Login/Register. Registering will enable you to access the content of ZoomTeller from any of your iOS devices."
        let alert = UIAlertController(title: "ZoomTeller", message: msg, preferredStyle: .alert)
        let login = UIAlertAction(title: "Login/Register", style: .default) { (_) in
            let defualt = UserDefaults.standard
            let dic = defualt.dictionaryRepresentation()
            dic.keys.forEach({ (key) in
                defualt.removeObject(forKey: key)
                defualt.synchronize()
            })
            defualt.synchronize()
            appDelegate.checkLoginStatus()
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(cancel)
        alert.addAction(login)
        viewContrller.present(alert, animated: true)
    }
    
    
    
    //MARK: - Plan Subscription
    //MARK: -
    
    func isUserLoginRestoreSubcription() -> Bool {
        return USERDEFAULT.value(forKey: "isUserLoginRestoreSubcription") as? Bool ?? false
    }
    
    func isRestorePurchasedPlan() -> Bool {
        return USERDEFAULT.value(forKey: "restorePurchased") as? Bool ?? false
    }
    
    func subscriptionPlanActiveTimeSkipUser() -> Double {
        let dddd = Date()
        let df1 = DateFormatter()
        df1.timeZone = TimeZone(secondsFromGMT: 0)
        df1.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        let dddd1 = df1.date(from: self.expiryPlanDateSkipUser())
        let intertttttt = dddd1!.timeIntervalSince(dddd)
        return intertttttt // 100
    }
    
    func expiryPlanDateSkipUser() -> String {
        let dateF = DateFormatter()
        dateF.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        let dString = dateF.string(from: USERDEFAULT.value(forKey: "expirationDateFromSubscription") as? Date ?? Date())
        return dString //"2019-08-30 12:30:30 +0000"
    }
    
    func restoreSubscriptionPlan() -> String? {
        //USERDEFAULT.set("com.zoomteller.fulloneyear", forKey: "productIdentifier")
        //USERDEFAULT.synchronize()
        let identi = USERDEFAULT.value(forKey: "productIdentifier") as? String
        if identi == "com.zoomteller.fulltwo" || identi == "com.zoomteller.fullfour" || identi == "com.zoomteller.fullsix" || identi == "com.zoomteller.fulloneyear" {
            return identi
        }else {
            return nil
        }
    }
    
    func isPlanSubscribe() -> Bool {
        return USERDEFAULT.value(forKey: IS_PLAN_SUBSCRIBE) as? Bool ?? false
    }
    
    func subscriptionPlanActiveTime() -> Double {
        let currentDate = USERDEFAULT.value(forKey: "todayDate") as? String ?? ""
        //let d = Date()
        let df = DateFormatter()
        df.timeZone = TimeZone(secondsFromGMT: 0)
        df.dateFormat = "yyyy-MM-dd"
        //let s = df.string(from: d)
        let dddd = df.date(from: currentDate) ?? Date()
        
        //let d1 = Date()
        let df1 = DateFormatter()
        df1.timeZone = TimeZone(secondsFromGMT: 0)
        df1.dateFormat = "yyyy-MM-dd"
        //let s1 = df1.string(from: d1)
        let dddd1 = df1.date(from: nsuserDefault.getUserDetails().subScribeEndDate) ?? Date()
        
        let intertttttt = dddd1.timeIntervalSince(dddd)
        
        //return USERDEFAULT.value(forKey: SUBSCRIPTION_PLAN_ACTIVE_TIME) as? Double ?? 0.0
        return intertttttt
    }
    
    //MARK: - Notification Banner Methods
    //MARK: -
    
    //    func ShowBannerView(title: String, Subtitle: String, style : BannerStyle){
    //
    //        switch style {
    //        case .success:
    //            let leftView = UIImageView(image: #imageLiteral(resourceName: "success"))
    //            let banner = NotificationBanner(title: title, subtitle: Subtitle, leftView: leftView, style: style)
    //            banner.show()
    //        case .danger:
    //            let leftView = UIImageView(image: #imageLiteral(resourceName: "danger"))
    //            let banner = NotificationBanner(title: title, subtitle: Subtitle, leftView: leftView, style: style)
    //            banner.show()
    //        default:
    //            break
    //        }
    //
    //    }
    
    
    //MARK: - Location Methods
    //MARK: -
    
    func locationEnableToCallVC()  {
        
        if parentATMMapVC != nil {
            parentATMMapVC.viewWill()
        }
        
        if paerntATMListVC != nil {
            paerntATMListVC.viewWillList()
        }
    }
    
    func userLocationEnable() -> Bool {
        return CLLocationManager.locationServicesEnabled()
    }
    
    func startLocationManager()  {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
    }
    
    
    func locationPermissionIsValid() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                return true
            }
        } else {
            print("Location services are not enabled")
            return false
            
        }
    }
    
    func getInitialLocationPopup() {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    func getInitialLocation() {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestAlwaysAuthorization()
        
        if (!CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .denied)
        {
            let alert = UIAlertController.init(title: "Location Services Disabled!", message: "Please enable Location Based Services for better results! We promise to keep your location private, To view active ATMs nearby, location needs to be enabled", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Settings", style: .default)
            { (action) in
                if !CLLocationManager.locationServicesEnabled()
                {
                    self.openAppLocationSetting()
                }
                else
                {
                    self.openAppLocationSetting()
                }
            }
            
            //let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
            let cancel = UIAlertAction(title: "Cancel", style: .default) { (_) in
                //self.getInitialLocation()
               // self.showToast(title: "", message: "To view active ATMs nearby, location needs to be enabled")
            }
            
            alert.addAction(ok)
            alert.addAction(cancel)
           // self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            
        }
        else
        {
            if nsuserDefault.getIsLoggin() {
                if let parentAMV = parentATMMapVC {
                    parentAMV.viewWill()
                }
            }
            locationManager.startUpdatingLocation()
        }
    }
    
    func stopUpdatingLiveLocation() {
        locationManager.stopUpdatingLocation()
    }
    
    func openAppLocationSetting()
    {
        UIApplication.shared.open(URL.init(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        self.currentLocation = locations.last
        
        let latitude = String(self.currentLocation.coordinate.latitude)
        let longitude = String(self.currentLocation.coordinate.longitude)
        
        USERDEFAULT.set(latitude, forKey: "latitude")
        USERDEFAULT.set(longitude, forKey: "longitude")
        USERDEFAULT.synchronize()
        
        strTemp = String(self.currentLocation.coordinate.latitude)
        
    }
    
    func getLocation() -> String? {
        
        return USERDEFAULT.value(forKey: "latitude") as? String ?? nil
    }
    
    //MARK: - SocialLogin Methods
    //MARK: -
    
    func makeGoogleSDKReady(){
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        let handleG = GIDSignIn.sharedInstance().handle(url,
                                                        sourceApplication: sourceApplication,
                                                        annotation: annotation)
        
        let handleF = ApplicationDelegate.shared.application(
            application,
            open: (url as URL?)!,
            sourceApplication: sourceApplication,
            annotation: annotation)
        
        let options: [String: AnyObject] = [UIApplication.OpenURLOptionsKey.sourceApplication.rawValue: sourceApplication as AnyObject, UIApplication.OpenURLOptionsKey.annotation.rawValue: annotation as AnyObject]
        
        let handleT = TWTRTwitter.sharedInstance().application(application, open: url, options: options)
        
        return handleG || handleF || handleT
        
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        
        let valueFB = ApplicationDelegate.shared.application(app, open: url, options: options)
        
        let valueTwitter =  TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        
        return valueFB || valueTwitter
        
    }
    
    func application(_ application: UIApplication, shouldAllowExtensionPointIdentifier extensionPointIdentifier: UIApplication.ExtensionPointIdentifier) -> Bool {
        if (extensionPointIdentifier == UIApplication.ExtensionPointIdentifier.keyboard) {
            return false
        }
        return true
    }
    
    
    //MARK: - Google Delegate Methods
    //MARK: -
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        // ...
        if let error = error {
            // ...
            NSLog("Error:- %@", error.localizedDescription)
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                // ...
                NSLog("Error:- %@", error.localizedDescription)
                return
            }
            // User is signed in
            // ...
            // Perform any operations on signed in user here.
            
        }
        // ...
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
        
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    
    fileprivate func paidSubscriptionActiveData() {
        if appDelegate.isSkipUser() != nil {
            if nsuserDefault.getUserDetails().activePlan.count != 0 {
                let details = activePlanDetails(dic: nsuserDefault.getUserDetails().activePlan)
                if details.plan_type == "Paid" {
                    //self.receiptValidation()
                }
            }
        }else {
            //self.receiptValidation()
        }
    }
    
    func receiptValidation() {
        
        //Submit this JSON object as the payload of an HTTP POST request. In the test environment, use https://sandbox.itunes.apple.com/verifyReceipt as the URL. In production, use https://buy.itunes.apple.com/verifyReceipt as the URL.
        
        let SUBSCRIPTION_SECRET = "253cbf5eb81d4d65bb7b4f756422b493"
        let receiptPath = Bundle.main.appStoreReceiptURL?.path
        if FileManager.default.fileExists(atPath: receiptPath!){
            var receiptData:NSData?
            do{
                receiptData = try NSData(contentsOf: Bundle.main.appStoreReceiptURL!, options: NSData.ReadingOptions.alwaysMapped)
            }
            catch{
                print("ERROR: " + error.localizedDescription)
            }
            //let receiptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            let base64encodedReceipt = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithCarriageReturn)
            
            print(base64encodedReceipt!)
            
            
            let requestDictionary = ["receipt-data":base64encodedReceipt!,"password":SUBSCRIPTION_SECRET]
            
            guard JSONSerialization.isValidJSONObject(requestDictionary) else {  print("requestDictionary is not valid JSON");  return }
            do {
                let requestData = try JSONSerialization.data(withJSONObject: requestDictionary)
                let validationURLString = "https://sandbox.itunes.apple.com/verifyReceipt"  // this works but as noted above it's best to use your own trusted server
                guard let validationURL = URL(string: validationURLString) else { print("the validation url could not be created, unlikely error"); return }
                let session = URLSession(configuration: URLSessionConfiguration.default)
                var request = URLRequest(url: validationURL)
                request.httpMethod = "POST"
                request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
                let task = session.uploadTask(with: request, from: requestData) { (data, response, error) in
                    if let data = data , error == nil {
                        do {
                            let appReceiptJSON = try JSONSerialization.jsonObject(with: data)
                            //print("success. here is the json representation of the app receipt: \(appReceiptJSON)")
                            if let addReceipt = appReceiptJSON as? NSDictionary {
                                let _ = self.expirationDateFromResponse(jsonResponse: addReceipt)
                            }
                            // if you are using your server this will be a json representation of whatever your server provided
                        } catch let error as NSError {
                            print("json serialization failed with error: \(error)")
                        }
                    } else {
                        print("the upload task returned an error: \(String(describing: error))")
                    }
                }
                task.resume()
            } catch let error as NSError {
                print("json serialization failed with error: \(error)")
            }
        }
    }
    
    func expirationDateFromResponse(jsonResponse: NSDictionary) -> Date? {
        if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {
            let lastReceipt = receiptInfo.lastObject as! NSDictionary
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            var expirationDate: Date = (formatter.date(from: lastReceipt["purchase_date_pst"] as! String))!
            //formatter.dateFormat = "yyyy-MM-dd"
           // let dateFormateChangeString =  formatter.string(from: expirationDate)
            //let dateFormateChangeDate = formatter.date(from: dateFormateChangeString)
            let identi = lastReceipt["product_id"] as? String ?? ""
            let oneMonth : Double = 60 * 60 * 24 * 30
            if identi == "com.zoomteller.fulltwo"  {
              expirationDate = expirationDate.addingTimeInterval(oneMonth * 2)
            }else if  identi == "com.zoomteller.fullfour" {
                expirationDate = expirationDate.addingTimeInterval(oneMonth * 4)
            }else if identi == "com.zoomteller.fullsix" {
                expirationDate = expirationDate.addingTimeInterval(oneMonth * 6)
            }else if identi == "com.zoomteller.fulloneyear" {
                expirationDate = expirationDate.addingTimeInterval(oneMonth * 12)
            }
            USERDEFAULT.set(expirationDate, forKey: "expirationDateFromSubscription")
            USERDEFAULT.synchronize()
            
            
            
//            let d = Date()
//            let df = DateFormatter()
//            df.timeZone = TimeZone(secondsFromGMT: 0)
//            df.dateFormat = "yyyy-MM-dd"
//            let s = df.string(from: d)
//            let dddd = df.date(from: nsuserDefault.getUserDetails().today)
            
            
            //let asdas = DateFormatter()
            //asdas.dateFormat = "yyyy-MM-dd HH:mm:ss z"
            // let asdadadaasd = asdas.date(from: "2019-07-05 09:10:11 +0000")
            // let asdadadaasdddddd = asdas.date(from: "2019-07-05 09:10:10 +0000")
            //let asdadadaasdaaaaaa = asdas.date(from: "2019-07-05 09:10:12 +0000")
            
            //let intertttttt = expirationDate.timeIntervalSince(dddd!)
            //print(intertttttt)
            //let interval = Double(intertttttt)
            //if interval <= 0 {
            
            if self.isSkipUser() != nil {
//                if let penddingStatus = jsonResponse["pending_renewal_info"] as? NSArray {
//                    if let autoRenew = penddingStatus.lastObject as? NSDictionary {
//                        if let status = autoRenew["auto_renew_status"] as? String {
//                            if status == "0" {
                                //print("unsubscribtion")
                                if subscriptionPlanActiveTime() <= 0  {
                                    self.unSubscribePlan()
                                    print("unsubscribtion")
                                }
//                            }
//                        }
//                    }
//                }
            }
            
            //}
            
            //USERDEFAULT.set(Double(intertttttt), forKey: SUBSCRIPTION_PLAN_ACTIVE_TIME)
            //USERDEFAULT.synchronize()
            if let parentAMV = parentATMMapVC {
                parentAMV.checkSubscriptionDetails()
            }
            return expirationDate as Date
        } else {
            return nil
            
        }
    }
    
    
    func unSubscribePlan() {
        
        let url = BASE_URL + API_UNSUBSCRIBE
        
        let param = ["user_id" : nsuserDefault.getUserDetails().userId!]
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, error) in
            ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, status) in
                appDelegate.hideHUD()
                if status == nil
                {
                    let res = response
                    if res?.value(forKey: "status") as! Bool == true
                    {
                        
                        let plan = activePlanDetails(dic: nsuserDefault.getUserDetails().activePlan)
                        let payloadDict = NSMutableDictionary()
                        payloadDict["user_email"] = nsuserDefault.getUserDetails().email ?? ""
                        payloadDict["user_name"]  = nsuserDefault.getUserDetails().name ?? ""
                        payloadDict["plan_id"] = plan.plan_id
                        payloadDict["plan_amount"]  = plan.amount
                        payloadDict["plan_amount_currency"] = plan.currency
                        payloadDict["plan_name"]  = plan.plan_name
                        payloadDict["plan_duration"] = plan.duration
                        payloadDict["plan_type"]  = plan.plan_type
                        payloadDict["plan_save_amount"] = plan.save_amount
                        payloadDict["plan_expire_date"]  = nsuserDefault.getUserDetails().subScribeEndDate ?? ""
                        
                        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Unsubscribe Current Plan", payload: payloadDict, block: nil)
                        
                        let data = res?.value(forKey: "data") as? [String:Any] ?? [String:Any]()
                        nsuserDefault.setUserDetails(logindata: LoginClass(fromDictionary: data))
                    }
                    else
                    {
                        appDelegate.showToast(title: "Error", message: res!.value(forKey: "message") as! String)
                        
                    }
                }
                else
                {
                    appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
                    
                }
            }
            
        }
        
    }
}

extension AppDelegate : SKProductsRequestDelegate {

  
    func fatchProductList() {
        DispatchQueue.main.async {
           // if self.arrPlanList.count > self.planFetchCount {
                //self.product_id = self.arrPlanList[self.planFetchCount]
                if (SKPaymentQueue.canMakePayments())
                {
                    let productID:NSSet = self.arrPlanList as NSSet
                    let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
                    productsRequest.delegate = self;
                    productsRequest.start();
                    print("Fetching Products");
                    if self.isFetchProductHudShow == true {
                        self.showHUD()
                    }
                }else{
                    print("Can't make purchases");
                    self.hideHUD()
                }
               // self.planFetchCount += 1
           // }else {
                //self.hideHUD()
            //}
        }
    }

    func productsRequest (_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        DispatchQueue.main.async {
            let count : Int = response.products.count
            if (count>0) {
                for data in response.products {
                    print(data.productIdentifier)
                    print(data.localizedTitle)
                    print(data.localizedDescription)
                    print(data.price)
                    if data.productIdentifier == "com.zoomteller.fulltwo" {
                        self.dicStorePlanPrice["7"] = "\(data.price)"
                    }else if data.productIdentifier == "com.zoomteller.fullfour" {
                        self.dicStorePlanPrice["8"] = "\(data.price)"
                    }else if data.productIdentifier == "com.zoomteller.fullsix" {
                        self.dicStorePlanPrice["5"] = "\(data.price)"
                    }else if data.productIdentifier == "com.zoomteller.fulloneyear" {
                        self.dicStorePlanPrice["6"] = "\(data.price)"
                    }
                    if parentSubscriptionVC != nil {
                        parentSubscriptionVC.collectionForSubscription.reloadData()
                    }
                }
                self.hideHUD()
            } else {
                print("nothing")
                self.hideHUD()
            }
        }
    }
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    // called when application is open when user click on notification
    @objc(userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler :) @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        NetCorePushTaskManager.sharedInstance().userNotificationdidReceive(response)
    }
}

extension AppDelegate : NetCorePushTaskManagerDelegate{
    func handleNotificationOpenAction(_ userInfo: [AnyHashable : Any]?, deepLinkType strType: String?) {
        if strType?.lowercased().contains ("smartechdeeplink") ?? false {
            // handle deep link here
            print("Deeplink received:\(strType ?? "" )")
        }
    }
}

