//
//  ApiHelper.swift
//  ZoomTeller
//
//  Created by ZERONES on 18/03/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class ApiHelper: NSObject {
    
    static let sharedInstance = ApiHelper()
    
    func CallWebservice(url : String, param : [String:String], completion: @escaping (NSDictionary?,String?) -> ()) {

        appDelegate.showHUD()
        
        if appDelegate.connected() == true{
            
            Alamofire.request(url, method: .post, parameters: param)
                .responseJSON { response in
                    print(response)
                    
                    appDelegate.hideHUD()
                    
                    switch response.result {
                    case .success:
                        let Res = response.result.value! as! NSDictionary
                        completion(Res,nil)
                        
                    case .failure(let error):
                        print(error)
                        print(String(decoding: response.data!, as: UTF8.self))
                        completion(nil,error.localizedDescription)
                    }
            }
        }else {
            
            appDelegate.hideHUD()
            appDelegate.showToast(title: "", message: "Please check internet connection")
        }
        
        
    }
}
