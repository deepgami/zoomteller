//
//  LoginClass.swift
//  ZoomTeller
//
//  Created by ZERONES on 16/03/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import Foundation

class LoginClass : NSObject, NSCoding{
    
    var address : String!
    var age : String!
    var name : String!
    var categoryId : String!
    var categoryName : String!
    var contactEmail : String!
    var contactFirstName : String!
    var contactJobPosition : String!
    var contactLastName : String!
    var contactNameTitle : String!
    var contactNationality : String!
    var contactTelephone : String!
    var email : String!
    var facebook : String!
    var firstName : String!
    var jobPosition : String!
    var lastName : String!
    var latitude : String!
    var logo : String!
    var longitude : String!
    var nameOfOrganization : String!
    var nameTitle : String!
    var natureOfBusiness : String!
    var notYetRegister : String!
    var numberOfEmployees : String!
    var otherNote : String!
    var profilePicture : String!
    var registerType : String!
    var signature : String!
    var telFax : String!
    var userId : String!
    var website : String!
    var yearOfEstablishment : String!
    var yearlyPaten : String!
    var promoCode : String!
    var isPromoCode : String!
    var activePlan : NSDictionary!
    var subScribeEndDate : String!
    var fb_id : String!
    var google_id : String!
    var twitter_id : String!
    var login_type : String!
    var today : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        address = dictionary["address"] as? String
        age = dictionary["age"] as? String
        name = dictionary["name"] as? String
        categoryId = dictionary["category_id"] as? String
        categoryName = dictionary["category_name"] as? String
        contactEmail = dictionary["contact_email"] as? String
        contactFirstName = dictionary["contact_first_name"] as? String
        contactJobPosition = dictionary["contact_job_position"] as? String
        contactLastName = dictionary["contact_last_name"] as? String
        contactNameTitle = dictionary["contact_name_title"] as? String
        contactNationality = dictionary["contact_nationality"] as? String
        contactTelephone = dictionary["contact_telephone"] as? String
        email = dictionary["email"] as? String
        facebook = dictionary["facebook"] as? String
        firstName = dictionary["first_name"] as? String
        jobPosition = dictionary["job_position"] as? String
        lastName = dictionary["last_name"] as? String
        latitude = dictionary["latitude"] as? String
        logo = dictionary["logo"] as? String
        longitude = dictionary["longitude"] as? String
        nameOfOrganization = dictionary["name_of_organization"] as? String
        nameTitle = dictionary["name_title"] as? String ?? ""
        natureOfBusiness = dictionary["nature_of_business"] as? String
        notYetRegister = dictionary["not_yet_register"] as? String
        numberOfEmployees = dictionary["number_of_employees"] as? String
        otherNote = dictionary["other_note"] as? String
        profilePicture = dictionary["profile_picture"] as? String
        registerType = dictionary["register_type"] as? String
        signature = dictionary["signature"] as? String
        telFax = dictionary["tel_fax"] as? String
        userId = dictionary["user_id"] as? String
        website = dictionary["website"] as? String
        yearOfEstablishment = dictionary["year_of_establishment"] as? String
        yearlyPaten = dictionary["yearly_paten"] as? String
        promoCode = dictionary["promo_code"] as? String
        isPromoCode = dictionary["is_promocode"] as? String
        activePlan = dictionary["active_plan"] as? NSDictionary
        subScribeEndDate = dictionary["subscription_end_date"] as? String
        fb_id = dictionary["fb_id"] as? String
        google_id = dictionary["google_id"] as? String
        twitter_id = dictionary["twitter_id"] as? String
        login_type = dictionary["login_type"] as? String
        today = dictionary["today"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if age != nil{
            dictionary["age"] = age
        }
        if name != nil{
            dictionary["name"] = name
        }
        if categoryId != nil{
            dictionary["category_id"] = categoryId
        }
        if categoryName != nil{
            dictionary["category_name"] = categoryName
        }
        if contactEmail != nil{
            dictionary["contact_email"] = contactEmail
        }
        if contactFirstName != nil{
            dictionary["contact_first_name"] = contactFirstName
        }
        if contactJobPosition != nil{
            dictionary["contact_job_position"] = contactJobPosition
        }
        if contactLastName != nil{
            dictionary["contact_last_name"] = contactLastName
        }
        if contactNameTitle != nil{
            dictionary["contact_name_title"] = contactNameTitle
        }
        if contactNationality != nil{
            dictionary["contact_nationality"] = contactNationality
        }
        if contactTelephone != nil{
            dictionary["contact_telephone"] = contactTelephone
        }
        if email != nil{
            dictionary["email"] = email
        }
        if facebook != nil{
            dictionary["facebook"] = facebook
        }
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if jobPosition != nil{
            dictionary["job_position"] = jobPosition
        }
        if lastName != nil{
            dictionary["last_name"] = lastName
        }
        if latitude != nil{
            dictionary["latitude"] = latitude
        }
        if logo != nil{
            dictionary["logo"] = logo
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        }
        if nameOfOrganization != nil{
            dictionary["name_of_organization"] = nameOfOrganization
        }
        if nameTitle != nil{
            dictionary["name_title"] = nameTitle
        }
        if natureOfBusiness != nil{
            dictionary["nature_of_business"] = natureOfBusiness
        }
        if notYetRegister != nil{
            dictionary["not_yet_register"] = notYetRegister
        }
        if numberOfEmployees != nil{
            dictionary["number_of_employees"] = numberOfEmployees
        }
        if otherNote != nil{
            dictionary["other_note"] = otherNote
        }
        if profilePicture != nil{
            dictionary["profile_picture"] = profilePicture
        }
        if registerType != nil{
            dictionary["register_type"] = registerType
        }
        if signature != nil{
            dictionary["signature"] = signature
        }
        if telFax != nil{
            dictionary["tel_fax"] = telFax
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        if website != nil{
            dictionary["website"] = website
        }
        if yearOfEstablishment != nil{
            dictionary["year_of_establishment"] = yearOfEstablishment
        }
        if yearlyPaten != nil{
            dictionary["yearly_paten"] = yearlyPaten
        }
        if promoCode != nil {
            dictionary["promo_code"] = promoCode
        }
        if isPromoCode != nil {
            dictionary["is_promocode"] = isPromoCode
        }
        if activePlan != nil {
            dictionary["active_plan"] = activePlan
        }
        if subScribeEndDate != nil {
            dictionary["subscription_end_date"] = subScribeEndDate
        }
        if fb_id != nil {
            dictionary["fb_id"] = fb_id
        }
        if google_id != nil {
            dictionary["google_id"] = google_id
        }
        if twitter_id != nil {
            dictionary["twitter_id"] = twitter_id
        }
        if login_type != nil {
            dictionary["login_type"] = login_type
        }
        if today != nil {
            dictionary["today"] = today
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        age = aDecoder.decodeObject(forKey: "age") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        categoryId = aDecoder.decodeObject(forKey: "category_id") as? String
        categoryName = aDecoder.decodeObject(forKey: "category_name") as? String
        contactEmail = aDecoder.decodeObject(forKey: "contact_email") as? String
        contactFirstName = aDecoder.decodeObject(forKey: "contact_first_name") as? String
        contactJobPosition = aDecoder.decodeObject(forKey: "contact_job_position") as? String
        contactLastName = aDecoder.decodeObject(forKey: "contact_last_name") as? String
        contactNameTitle = aDecoder.decodeObject(forKey: "contact_name_title") as? String
        contactNationality = aDecoder.decodeObject(forKey: "contact_nationality") as? String
        contactTelephone = aDecoder.decodeObject(forKey: "contact_telephone") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        facebook = aDecoder.decodeObject(forKey: "facebook") as? String
        firstName = aDecoder.decodeObject(forKey: "first_name") as? String
        jobPosition = aDecoder.decodeObject(forKey: "job_position") as? String
        lastName = aDecoder.decodeObject(forKey: "last_name") as? String
        latitude = aDecoder.decodeObject(forKey: "latitude") as? String
        logo = aDecoder.decodeObject(forKey: "logo") as? String
        longitude = aDecoder.decodeObject(forKey: "longitude") as? String
        nameOfOrganization = aDecoder.decodeObject(forKey: "name_of_organization") as? String
        nameTitle = aDecoder.decodeObject(forKey: "name_title") as? String
        natureOfBusiness = aDecoder.decodeObject(forKey: "nature_of_business") as? String
        notYetRegister = aDecoder.decodeObject(forKey: "not_yet_register") as? String
        numberOfEmployees = aDecoder.decodeObject(forKey: "number_of_employees") as? String
        otherNote = aDecoder.decodeObject(forKey: "other_note") as? String
        profilePicture = aDecoder.decodeObject(forKey: "profile_picture") as? String
        registerType = aDecoder.decodeObject(forKey: "register_type") as? String
        signature = aDecoder.decodeObject(forKey: "signature") as? String
        telFax = aDecoder.decodeObject(forKey: "tel_fax") as? String
        userId = aDecoder.decodeObject(forKey: "user_id") as? String
        website = aDecoder.decodeObject(forKey: "website") as? String
        yearOfEstablishment = aDecoder.decodeObject(forKey: "year_of_establishment") as? String
        yearlyPaten = aDecoder.decodeObject(forKey: "yearly_paten") as? String
        promoCode = aDecoder.decodeObject(forKey: "promo_code") as? String
        isPromoCode = aDecoder.decodeObject(forKey: "is_promocode") as? String
        activePlan = aDecoder.decodeObject(forKey: "active_plan") as? NSDictionary
        subScribeEndDate =  aDecoder.decodeObject(forKey: "subscription_end_date") as? String
        fb_id =  aDecoder.decodeObject(forKey: "fb_id") as? String
        google_id =  aDecoder.decodeObject(forKey: "google_id") as? String
        twitter_id =  aDecoder.decodeObject(forKey: "twitter_id") as? String
        login_type = aDecoder.decodeObject(forKey: "login_type") as? String
        today = aDecoder.decodeObject(forKey: "today") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if age != nil{
            aCoder.encode(age, forKey: "age")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if categoryId != nil{
            aCoder.encode(categoryId, forKey: "category_id")
        }
        if categoryName != nil{
            aCoder.encode(categoryName, forKey: "category_name")
        }
        if contactEmail != nil{
            aCoder.encode(contactEmail, forKey: "contact_email")
        }
        if contactFirstName != nil{
            aCoder.encode(contactFirstName, forKey: "contact_first_name")
        }
        if contactJobPosition != nil{
            aCoder.encode(contactJobPosition, forKey: "contact_job_position")
        }
        if contactLastName != nil{
            aCoder.encode(contactLastName, forKey: "contact_last_name")
        }
        if contactNameTitle != nil{
            aCoder.encode(contactNameTitle, forKey: "contact_name_title")
        }
        if contactNationality != nil{
            aCoder.encode(contactNationality, forKey: "contact_nationality")
        }
        if contactTelephone != nil{
            aCoder.encode(contactTelephone, forKey: "contact_telephone")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if facebook != nil{
            aCoder.encode(facebook, forKey: "facebook")
        }
        if firstName != nil{
            aCoder.encode(firstName, forKey: "first_name")
        }
        if jobPosition != nil{
            aCoder.encode(jobPosition, forKey: "job_position")
        }
        if lastName != nil{
            aCoder.encode(lastName, forKey: "last_name")
        }
        if latitude != nil{
            aCoder.encode(latitude, forKey: "latitude")
        }
        if logo != nil{
            aCoder.encode(logo, forKey: "logo")
        }
        if longitude != nil{
            aCoder.encode(longitude, forKey: "longitude")
        }
        if nameOfOrganization != nil{
            aCoder.encode(nameOfOrganization, forKey: "name_of_organization")
        }
        if nameTitle != nil{
            aCoder.encode(nameTitle, forKey: "name_title")
        }
        if natureOfBusiness != nil{
            aCoder.encode(natureOfBusiness, forKey: "nature_of_business")
        }
        if notYetRegister != nil{
            aCoder.encode(notYetRegister, forKey: "not_yet_register")
        }
        if numberOfEmployees != nil{
            aCoder.encode(numberOfEmployees, forKey: "number_of_employees")
        }
        if otherNote != nil{
            aCoder.encode(otherNote, forKey: "other_note")
        }
        if profilePicture != nil{
            aCoder.encode(profilePicture, forKey: "profile_picture")
        }
        if registerType != nil{
            aCoder.encode(registerType, forKey: "register_type")
        }
        if signature != nil{
            aCoder.encode(signature, forKey: "signature")
        }
        if telFax != nil{
            aCoder.encode(telFax, forKey: "tel_fax")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "user_id")
        }
        if website != nil{
            aCoder.encode(website, forKey: "website")
        }
        if yearOfEstablishment != nil{
            aCoder.encode(yearOfEstablishment, forKey: "year_of_establishment")
        }
        if yearlyPaten != nil{
            aCoder.encode(yearlyPaten, forKey: "yearly_paten")
        }
        if promoCode != nil {
            aCoder.encode(promoCode, forKey: "promo_code")
        }
        if isPromoCode != nil {
            aCoder.encode(isPromoCode, forKey: "is_promocode")
        }
        if activePlan != nil {
            aCoder.encode(activePlan, forKey: "active_plan")
        }
        if subScribeEndDate != nil {
         aCoder.encode(subScribeEndDate, forKey: "subscription_end_date")
        }
        if fb_id != nil {
            aCoder.encode(fb_id, forKey: "fb_id")
        }
        if google_id != nil {
            aCoder.encode(google_id, forKey: "google_id")
        }
        if twitter_id != nil {
            aCoder.encode(twitter_id, forKey: "twitter_id")
        }
        if login_type != nil {
            aCoder.encode(login_type, forKey: "login_type")
        }
        if today != nil {
            aCoder.encode(today, forKey: "today")
        }
        
    }
    
}



struct activePlanDetails {

     let amount : String
     let save_amount : String
     let plan_type : String
    let plan_name : String
     let duration : String
     let createdAt : String
     let plan_id : String
     let currency : String
    
    init(dic : NSDictionary) {
        self.amount = dic["amount"] as? String ?? "0"
        self.save_amount = dic["save_amount"] as? String ?? "0"
        self.plan_type = dic["plan_type"] as? String ?? ""
        self.duration = dic["duration"] as? String ?? ""
        self.createdAt = dic["createdAt"] as? String ?? ""
        self.plan_id = dic["plan_id"] as? String ?? ""
        self.currency = dic["currency"] as? String ?? ""
        self.plan_name = dic["plan_name"] as? String ?? ""
    }
}
