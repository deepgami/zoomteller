//
//  NSUserDefaultClass.swift
//  ZoomTeller
//
//  Created by ZERONES on 16/03/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import Foundation

class NSUserDefaultClass: NSObject {
    
    // MARK: - Shared Instance
    // MARK: -
    
    static let sharedInstance: NSUserDefaultClass = {
        let instance = NSUserDefaultClass()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    // MARK: -
    
    override init() {
        super.init()
    }
    
    //Login
    public func setIsLoggin(isLoggin: Bool){
        
        let defaults = UserDefaults.standard
        defaults.set(isLoggin, forKey: "isLoggin")
        defaults.synchronize()
    }
    
    public func getIsLoggin()->Bool{
        return  self.isKeyPresentInUserDefaults(key: "isLoggin")
    }
    
    //Remember Me
    public func setIsRemember(isLoggin: Bool){
        
        let defaults = UserDefaults.standard
        defaults.set(isLoggin, forKey: "isRemember")
        defaults.synchronize()
    }
    
    public func getIsRemember()->Bool{
        return  self.isKeyPresentInUserDefaults(key: "isRemember")
    }
    
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        
        if  (UserDefaults.standard.object(forKey: key) == nil) {
            return false
        }
        let defaults = UserDefaults.standard
        return defaults.object(forKey: "isLoggin")as! Bool
    }
    
  
    
    //Set-Get data
    public func setUserDetails(logindata:LoginClass){
        
        let defaults = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: logindata)
        defaults.set(encodedData, forKey: "USER")
        
        defaults.synchronize()
    }
    
    public func getUserDetails()-> LoginClass{
        let defaults = UserDefaults.standard
        let decoded  = defaults.object(forKey: "USER") as? Data ?? Data()
        let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded)
        return decodedTeams as! LoginClass
    }
   
    public func getSkipUserDetails()-> LoginClass? {
        let defaults = UserDefaults.standard
        let decoded  = defaults.object(forKey: "USER") as? Data ?? Data()
        let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded)
        return decodedTeams as? LoginClass ?? nil
        
    }
   
    public func removeUserDetails()->Void{
        let defaults = UserDefaults.standard
        defaults.set(nil, forKey: "USER")
        defaults.synchronize()
    }
    
}


