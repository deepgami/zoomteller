//
//  Constant.swift
//  ZoomTeller
//
//  Created by ZERONES on 18/03/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import Foundation
import UIKit

let nsuserDefault = NSUserDefaultClass.sharedInstance
let appDelegate : AppDelegate = AppDelegate.sharedAppDelegate()
let USERDEFAULT = UserDefaults.standard

var parentATMMapVC : ATMMapVC!
var paerntATMListVC : ATMListVC!
var parentSp_MenuVC : Sp_MenuVC!
var parentSubscriptionVC : SubscriptionVC!

// KEY

let IS_PLAN_SUBSCRIBE = "is_plan_subscribe"
let SUBSCRIPTION_PLAN_ACTIVE_TIME = "subscription_plan_active_status"


//<!-- colors using in app -->

let blue : UIColor = UIColor.hexFromColorAndAlpha(hex: "#720098", alpha: 1.0)

let white : UIColor = UIColor.hexFromColorAndAlpha(hex: "#FFF", alpha: 1.0)
let black : UIColor = UIColor.hexFromColorAndAlpha(hex: "#000", alpha: 1.0)
let light_theme_color : UIColor = UIColor.hexFromColorAndAlpha(hex: "#9b99a9", alpha: 1.0)
let text_color_dark : UIColor = UIColor.hexFromColorAndAlpha(hex: "#4b4b4d", alpha: 1.0)
let hint_color : UIColor = UIColor.hexFromColorAndAlpha(hex: "#5f5d70", alpha: 1.0)
let edit_bg : UIColor = UIColor.hexFromColorAndAlpha(hex: "#F6F6F9", alpha: 1.0)
let invalid_line_color : UIColor = UIColor.hexFromColorAndAlpha(hex: "#D62438", alpha: 1.0)
let map_route_color : UIColor = UIColor.hexFromColorAndAlpha(hex: "#4A90E2", alpha: 1.0)
let light_grey : UIColor = UIColor.hexFromColorAndAlpha(hex: "#F6F6F9", alpha: 1.0)

let slate_grey : UIColor = UIColor.hexFromColorAndAlpha(hex: "#5f5d70", alpha: 1.0)
let charcol_grey : UIColor = UIColor.hexFromColorAndAlpha(hex: "#35343d", alpha: 1.0)


let filter_pink : UIColor = UIColor.hexFromColorAndAlpha(hex: "#9900C9", alpha: 1.0)
let filter_pink_disable : UIColor = UIColor.hexFromColorAndAlpha(hex: "#9900C9", alpha: 0.5)

//<!-- dots inactive colors -->
let dot_dark_screen : UIColor = UIColor.hexFromColorAndAlpha(hex: "#bb61ff", alpha: 1.0)

//<!-- dots active colors -->
let dot_light_screenblue : UIColor = UIColor.hexFromColorAndAlpha(hex: "#720098", alpha: 1.0)


/*----------------------- API For Application --------------------------*/


//let BASE_URL : String = "http://zerones.biz/client/zoom_teller/zoom_teller_live/apis/"
let BASE_URL : String = "http://zoomteller.com/apis/" // Live


//let BASE_URL : String = "http://zerones.biz/client/zoom_teller/apis/"

//Module-1
let API_SIGNUP : String = "sign_up.php"
let API_LOGIN : String = "login.php"
let API_SOCIAL_LOGIN : String = "social_login.php"
let API_FORGOT_PASSWORD = "forgot_password.php"
let API_CHANGE_PASSWORD = "change_password.php"
let API_GET_ATM : String = "get_ATM.php"
let API_GET_BANK : String = "get_bank.php"
let API_EDIT_PROFILE : String = "edit_profile.php"
let API_GET_HISTORY : String = "get_history.php"


//T4dZlv
//Module-1


let API_GET_INSTRUMENTS = "get_instruments.php"
let API_GET_MUSIC_STYLE = "get_music_styles.php"
let API_GET_FAVORITE = "get_favorite_gig.php"
let API_GET_MY_EVENTS = "get_event.php"
let API_CREATE_EVENT = "create_event.php"
let API_EDIT_EVENT = "edit_event.php"
let API_GET_MY_GIGS = "get_gig.php"
let API_CREATE_GIG = "create_gig.php"
let API_EDIT_GIG = "edit_gig.php"
let API_GET_BLOG = "get_blog.php"
let API_SEND_MESSAGE = "chat.php"
let API_GET_CHAT_HISTORY = "get_chat_history.php"
let API_GET_INVITE_EVENT = "get_invite_event.php"
let API_ANSWER_EVENT = "answer_event.php"
let API_FEEDBACK = "feedback.php"
let API_FILTER_GIG = "filter_gig.php"


let API_APPLY_PROMOCODE = "apply_promocode.php"
let API_SUBSCRIPTION = "subscription.php"
let API_GET_PLAN = "get_plan.php"
let API_HISTORY = "history.php"
let API_UNSUBSCRIBE = "unsubscribe.php"

class Constant {
    
    func SYSTEM_VERSION_EQUAL_TO(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                      options: NSString.CompareOptions.numeric) == ComparisonResult.orderedSame
    }
    
    func SYSTEM_VERSION_GREATER_THAN(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                            options: NSString.CompareOptions.numeric) == ComparisonResult.orderedDescending
    }
    
    func SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                      options: NSString.CompareOptions.numeric) != ComparisonResult.orderedAscending
    }
    
    func SYSTEM_VERSION_LESS_THAN(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                      options: NSString.CompareOptions.numeric) == ComparisonResult.orderedAscending
    }
    
    func SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                            options: NSString.CompareOptions.numeric) != ComparisonResult.orderedDescending
    }

}

public enum FilterViewSection {
    case none
    case musicalInstrumnets
    case musicStyles
    case expr
    case performerLvl
    case language
}
