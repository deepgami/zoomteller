//
//  BankFilterCell.swift
//  ZoomTeller
//
//  Created by Deep Zerones on 5/3/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit

class BankFilterCell: UICollectionViewCell {
    
    @IBOutlet weak var imgvBankIcon: UIImageView!
    @IBOutlet weak var imgvCheck: UIImageView!

}
