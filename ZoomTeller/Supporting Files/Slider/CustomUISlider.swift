//
//  CustomUISlider.swift
//  ZoomTeller
//
//  Created by Deep Zerones on 5/3/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit

class CustomUISlider: UISlider {

    var thumbTextLabel: UILabel = UILabel()

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    private var thumbFrame: CGRect {
        return thumbRect(forBounds: bounds, trackRect: trackRect(forBounds: bounds), value: value)
    }

    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        
        //keeps original origin and width, changes height, you get the idea
        let customBounds = CGRect(origin: bounds.origin, size: CGSize(width: bounds.size.width, height: 4.0))
        super.trackRect(forBounds: customBounds)
        return customBounds
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        thumbTextLabel.frame = thumbFrame
        
        var strTitle = String()
        
        strTitle = String(format: "%.0f m", value)
        
        thumbTextLabel.text = strTitle
    }
    
    //while we are here, why not change the image here as well? (bonus material)
    override func awakeFromNib() {
        
        
        super.awakeFromNib()
        thumbTextLabel.textColor = .gray
        thumbTextLabel.font = UIFont(name:"Arial",size:13)

        addSubview(thumbTextLabel)
        thumbTextLabel.textAlignment = .center
        thumbTextLabel.layer.zPosition = layer.zPosition + 1
        
        self.setThumbImage(UIImage(named: "slider_icon"), for: .normal)
        super.awakeFromNib()
    }
}
