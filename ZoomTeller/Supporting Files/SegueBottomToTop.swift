//
//  SegueBottomToTop.swift
//  ZoomTeller
//
//  Created by Deep Zerones on 4/12/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit

class SegueBottomToTop: UIStoryboardSegue {

    override func perform() {
        let src = self.source
        let dst = self.destination
        
        src.view.superview?.insertSubview(dst.view, aboveSubview: src.view)
        dst.view.transform = CGAffineTransform(translationX: 0, y: dst.view.frame.size.height)
        
        if(UIScreen.main.nativeBounds.height == 2436 || UIScreen.main.nativeBounds.height == 2688)
        {
            UIView.animate(withDuration: 0.4,
                           delay: 0.0,
                           options: .curveEaseInOut,
                           animations: {
                            dst.view.transform = CGAffineTransform(translationX: 0, y: 40)
            },
                           completion: { finished in
                            // src.present(dst, animated: false, completion: nil)
                            src.navigationController?.pushViewController(dst, animated: false)
                            
            }
            )       
            
        }
        else
        {
            UIView.animate(withDuration: 0.4,
                           delay: 0.0,
                           options: .curveEaseInOut,
                           animations: {
                            dst.view.transform = CGAffineTransform(translationX: 0, y: 20)
            },
                           completion: { finished in
                            // src.present(dst, animated: false, completion: nil)
                            src.navigationController?.pushViewController(dst, animated: false)
                            
            }
            )
        }
        
    }
}
