//
//  SubscriptionVC.swift
//  ZoomTeller
//
//  Created by Deep Gami on 15/06/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import StoreKit
import NetCorePush

class SubscriptionVC: UIViewController , SKPaymentTransactionObserver{
    
    
    @IBOutlet weak var collectionForSubscription: UICollectionView!
    @IBOutlet weak var lblCurrentPlan: UILabel!
    @IBOutlet weak var btnSubscribe: UIButton!
    
    var dispatchGroup = DispatchGroup()
    var product_id :String!
    var arrGetPlan = [Any]()
    var arrGetPlanDuplicated = [Any]()
    var selectIndex = 0
    var isCVReload = false
    var planeType : String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        parentSubscriptionVC = self
        SKPaymentQueue.default().add(self)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getPlanWebService()
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Subscription Activity Open", payload: payloadDict, block: nil)
       
    }
    
    @IBAction func btnSubscribe_touchUpInside(_ sender: UIButton) {
        self.btnSubscribeClick(index: selectIndex)
    }
    
  
    @IBAction func btnRestore(_ sender: UIButton) {
        
        SKPaymentQueue.default().add(self)
        if (SKPaymentQueue.canMakePayments()) {
            SKPaymentQueue.default().restoreCompletedTransactions()
        }
        
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getPlanWebService() {
        
        let url = BASE_URL + API_GET_PLAN
        let param = ["user_id" : nsuserDefault.getUserDetails().userId!,
                    "device_type":"iPhone"]
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, error) in
            if error == nil {
                
                let res = response
                
                if res?.value(forKey: "status") as! Bool == true {
                    let data = res?.object(forKey: "data") as! [Any]
                    self.arrGetPlan = data
                    self.arrGetPlanDuplicated = data
                    self.setUpDetailsCV()
                    //self.collectionForSubscription.reloadData()
                }else {
                    appDelegate.showToast(title: "", message: res?.value(forKey: "message") as? String ?? "")
                }
            }else {
                
            }
            if appDelegate.dicStorePlanPrice.count < 4 {
                DispatchQueue.main.async {
                    appDelegate.isFetchProductHudShow = true
                    appDelegate.fatchProductList()
                }
            }
        }
    }
    
    func subscriptionWebService(planId:String, planName:String, transactionId:String, endDate:String) {
        let url = BASE_URL + API_SUBSCRIPTION
        
        appDelegate.showHUD()
        
        let param = ["user_id" : nsuserDefault.getUserDetails().userId,
                     "plan_id" : planId,
                     "plan_name" : planName,
                     "transaction_id" : transactionId,
                     "device_type":"iPhone",
                     "subscription_end_date":endDate] as [String:String]
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, error) in
            
            if error == nil {
                
                let res = response
                
                if res?.value(forKey: "status") as! Bool == true {
                    
                    let data = res?.object(forKey: "data") as? [String:Any]
                    nsuserDefault.setUserDetails(logindata: LoginClass(fromDictionary: data!))
                    //self.arrGetPlan = self.arrGetPlanDuplicated
                    //self.collectionForSubscription.reloadData()
                    self.getPlanWebService()
                    appDelegate.hideHUD()
                    
                    let plan = activePlanDetails(dic: nsuserDefault.getUserDetails().activePlan)
                    let payloadDict = NSMutableDictionary()
                    payloadDict["user_email"] = nsuserDefault.getUserDetails().email ?? ""
                    payloadDict["user_name"]  = nsuserDefault.getUserDetails().name ?? ""
                    payloadDict["plan_id"] = plan.plan_id
                    payloadDict["plan_amount"]  = plan.amount
                    payloadDict["plan_amount_currency"] = plan.currency
                    payloadDict["plan_name"]  = plan.plan_name
                    payloadDict["plan_duration"] = plan.duration
                    payloadDict["plan_type"]  = plan.plan_type
                    payloadDict["plan_save_amount"] = plan.save_amount
                    payloadDict["plan_expire_date"]  = nsuserDefault.getUserDetails().subScribeEndDate ?? ""
                    
                    NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Subscribe Plan", payload: payloadDict, block: nil)
                    
                }else {
                    appDelegate.hideHUD()
                    appDelegate.showToast(title: "", message: res?.value(forKey: "message") as? String ?? "")
                }
                
            }else {
                
            }
        }
    }
}

// MARK: - UICollectionViewDelegate method

extension SubscriptionVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrGetPlan.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionForSubscription.dequeueReusableCell(withReuseIdentifier: "SubscriptionCell", for: indexPath) as! SubscriptionCell
        cell.backgroundColor = UIColor.clear
        
        if self.arrGetPlan.count > indexPath.row {
            
            let ad = activePlanDetails(dic: self.arrGetPlan[indexPath.row] as! NSDictionary)
            let amountPromo :String?
            
            if appDelegate.isSkipUser() == nil && appDelegate.restoreSubscriptionPlan() != nil {
                
                if Int(ad.save_amount) == 0 {
                    
                    amountPromo = "Pay ₦\(ad.amount) for \(ad.duration)"
                }else {
                    amountPromo = "Pay ₦\(ad.amount) for \(ad.duration)\nSave ₦\(ad.save_amount)"
                }
                
                if(indexPath.row == 0){
                        cell.viewBG.layer.borderColor = slate_grey.cgColor
                        cell.viewBG.layer.borderWidth = 2.0
                        cell.lblPlanDay.textColor = slate_grey
                        cell.lblPlanName.textColor = slate_grey
                        cell.imgvPromoBG.isHidden = true
                        cell.imgvPlanBG.isHidden = true
                        cell.lblPlanDay.text = ad.plan_name
                        //cell.lblPlanName.text = "₦" + ad.amount
                        cell.lblPlanName.text = "₦" + String(appDelegate.dicStorePlanPrice[ad.plan_id] ?? "")
                        self.planeType = "Paid"
                    
                    
                    let day = appDelegate.expiryPlanDateSkipUser().countDaySkipUser(date: appDelegate.expiryPlanDateSkipUser())
                    if day > 0 {
                        let d:String!
                        if day == 1 {
                            d = "day"
                        }else {
                            d = "days"
                        }
                        cell.lblPlanStatus.text = "\(day) \(String(describing: d!)) left"
                        cell.lblPlanStatus.textColor = slate_grey
                        cell.imgvPlanActiveTick.isHidden = false
                    }else {
                        cell.lblPlanStatus.text = "Expired"
                        cell.lblPlanStatus.textColor = .red
                        cell.imgvPlanActiveTick.isHidden = true
                        cell.lblPlanName.textColor = slate_grey
                    }
                    
                }else {
                    cell.imgvPromoBG.isHidden = true
                    cell.imgvPlanBG.isHidden = false
                    //cell.lblPlanName.text = "₦" + ad.amount
                    cell.lblPlanName.text = "₦" + String(appDelegate.dicStorePlanPrice[ad.plan_id] ?? "")
                    cell.lblPlanDay.text = ad.plan_name
                    cell.lblPlanStatus.text = amountPromo
                    cell.lblPlanName.textColor = .yellow
                    cell.lblPlanStatus.textColor = .white
                    cell.lblPlanDay.textColor = .white
                    cell.imgvPlanActiveTick.isHidden = true
                    
                }
                
                if self.isCVReload == true {
                    if self.selectIndex == indexPath.row {
                        cell.viewBG.layer.borderColor = UIColor.black.cgColor
                        cell.viewBG.layer.borderWidth = 2
                    }else {
                        if indexPath.row == 0 {
                            cell.viewBG.layer.borderColor = slate_grey.cgColor
                            cell.viewBG.layer.borderWidth = 2.0
                        }else {
                            cell.viewBG.layer.borderColor = UIColor.clear.cgColor
                            cell.viewBG.layer.borderWidth = 0
                        }
                    }
                }else {
                    if indexPath.row == 0 {
                        cell.viewBG.layer.borderColor = UIColor.black.cgColor
                        cell.viewBG.layer.borderWidth = 2
                    }
                }
                
            }else if nsuserDefault.getUserDetails().activePlan != nil && appDelegate.isSkipUser() != nil {
                
                if nsuserDefault.getUserDetails().activePlan.count != 0 {
                    
                    if Int(ad.save_amount) == 0{
                        amountPromo = "Pay ₦\(ad.amount) for \(ad.duration)"
                    }else {
                        amountPromo = "Pay ₦\(ad.amount) for \(ad.duration)\nSave ₦\(ad.save_amount)"
                    }
                    let endDate = nsuserDefault.getUserDetails().subScribeEndDate
                    
                    let day = endDate!.countDay(date:endDate!)
                    
                    if(indexPath.row == 0){
                        
                        if ad.plan_type == "trial" {
                            cell.viewBG.layer.borderColor = slate_grey.cgColor
                            cell.viewBG.layer.borderWidth = 2.0
                            cell.lblPlanDay.textColor = slate_grey
                            cell.lblPlanName.textColor = slate_grey
                            cell.imgvPromoBG.isHidden = true
                            cell.imgvPlanBG.isHidden = true
                            cell.lblPlanDay.text = ad.plan_name
                            cell.lblPlanName.text = "Trial"
                            self.planeType = "trial"
                        }else if ad.plan_type == "promo" {
                            cell.viewBG.layer.borderColor = UIColor.clear.cgColor
                            cell.viewBG.layer.borderWidth = 0
                            cell.lblPlanName.textColor = .black
                            cell.lblPlanStatus.textColor = .black
                            cell.lblPlanDay.textColor = .black
                            cell.imgvPromoBG.isHidden = false
                            cell.imgvPlanBG.isHidden = true
                            cell.lblPlanDay.text = ad.plan_name
                            cell.lblPlanName.text = "Promo"
                            self.planeType = "promo"
                        }else if ad.plan_type == "Paid" {
                            cell.viewBG.layer.borderColor = slate_grey.cgColor
                            cell.viewBG.layer.borderWidth = 2.0
                            cell.lblPlanDay.textColor = slate_grey
                            cell.lblPlanName.textColor = slate_grey
                            cell.imgvPromoBG.isHidden = true
                            cell.imgvPlanBG.isHidden = true
                            cell.lblPlanDay.text = ad.plan_name
                            //cell.lblPlanName.text = "₦" + ad.amount
                            cell.lblPlanName.text = "₦" + String(appDelegate.dicStorePlanPrice[ad.plan_id] ?? "")
                            self.planeType = "Paid"
                        }
                        if day > 0 {
                            let d:String!
                            if day == 1 {
                                d = "day"
                            }else {
                                d = "days"
                            }
                            cell.lblPlanStatus.text = "\(day) \(String(describing: d!)) left"
                            cell.lblPlanStatus.textColor = slate_grey
                            cell.imgvPlanActiveTick.isHidden = false
                        }else {
                            cell.lblPlanStatus.text = "Expired"
                            cell.lblPlanStatus.textColor = .red
                            cell.imgvPlanActiveTick.isHidden = true
                            cell.lblPlanName.textColor = slate_grey
                        }
                        
                    } else if(indexPath.row == 1) {
                        
                        if ad.plan_type == "trial" {
                            cell.lblPlanName.text = "Trial"
                            cell.imgvPromoBG.isHidden = true
                            cell.imgvPlanBG.isHidden = true
                        }else if ad.plan_type == "promo" {
                            cell.lblPlanName.text = "Promo"
                            cell.lblPlanStatus.text = "Free for 1 month"
                            cell.lblPlanDay.text = ad.plan_name
                            cell.lblPlanName.textColor = .black
                            cell.lblPlanStatus.textColor = .black
                            cell.lblPlanDay.textColor = .black
                            cell.imgvPromoBG.isHidden = false
                            cell.imgvPlanBG.isHidden = true
                        }else if ad.plan_type == "Paid" {
                            //cell.lblPlanName.text = "₦" + ad.amount
                            cell.lblPlanName.text = "₦" + String(appDelegate.dicStorePlanPrice[ad.plan_id] ?? "")
                            cell.lblPlanStatus.text = amountPromo
                            cell.lblPlanDay.text = ad.plan_name
                            cell.imgvPromoBG.isHidden = true
                            cell.imgvPlanBG.isHidden = false
                            cell.lblPlanName.textColor = .yellow
                            cell.lblPlanStatus.textColor = .white
                            cell.lblPlanDay.textColor = .white
                            cell.lblPlanStatus.textColor = .white
                            
                        }
                        cell.imgvPlanActiveTick.isHidden = true
                        
                    } else {
                        cell.imgvPromoBG.isHidden = true
                        cell.imgvPlanBG.isHidden = false
                        //cell.lblPlanName.text = "₦" + ad.amount
                        cell.lblPlanName.text = "₦" + String(appDelegate.dicStorePlanPrice[ad.plan_id] ?? "")
                        cell.lblPlanDay.text = ad.plan_name
                        cell.lblPlanStatus.text = amountPromo
                        cell.lblPlanName.textColor = .yellow
                        cell.lblPlanStatus.textColor = .white
                        cell.lblPlanDay.textColor = .white
                        cell.imgvPlanActiveTick.isHidden = true
                    }
                    
                    cell.layer.cornerRadius = 5
                    
                    if self.isCVReload == true {
                        if self.selectIndex == indexPath.row {
                            if indexPath.row == 0 {
                                if ad.plan_type == "promo" {
                                    cell.viewBG.layer.borderColor = UIColor.black.cgColor
                                    cell.viewBG.layer.borderWidth = 2
                                    cell.lblPlanName.textColor = .black
                                }else {
                                    cell.viewBG.layer.borderColor = UIColor.black.cgColor
                                    cell.lblPlanName.textColor = slate_grey
                                    cell.viewBG.layer.borderWidth = 2
                                }
                            }else {
                                cell.viewBG.layer.borderColor = UIColor.black.cgColor
                                cell.viewBG.layer.borderWidth = 2
                            }
                        }else {
                            if indexPath.row == 0 {
                                if ad.plan_type == "promo" {
                                    cell.viewBG.layer.borderColor = UIColor.clear.cgColor
                                    cell.viewBG.layer.borderWidth = 0
                                    cell.lblPlanName.textColor = .black
                                }else {
                                    cell.viewBG.layer.borderColor = slate_grey.cgColor
                                    cell.lblPlanName.textColor = slate_grey
                                    cell.viewBG.layer.borderWidth = 2.0
                                }
                            }else {
                                cell.viewBG.layer.borderColor = UIColor.clear.cgColor
                                cell.viewBG.layer.borderWidth = 2
                            }
                        }
                    }
                }else {
                    
                    if Int(ad.save_amount) == 0 {
                        amountPromo = "Pay ₦\(ad.amount) for \(ad.duration)"
                    }else {
                        amountPromo = "Pay ₦\(ad.amount) for \(ad.duration)\nSave ₦\(ad.save_amount)"
                    }
                    
                    cell.imgvPlanActiveTick.isHidden = true
                    //if nsuserDefault.getUserDetails().isPromoCode == "yes" &&  indexPath.row == 0 {
                    if nsuserDefault.getUserDetails().isPromoCode == "no" &&  indexPath.row == 0 && appDelegate.isSkipUser() != nil {
                        cell.lblPlanName.text = "Promo"
                        cell.lblPlanStatus.text = "Free for 1 month"
                        cell.lblPlanDay.text = ad.plan_name
                        cell.lblPlanName.textColor = .black
                        cell.lblPlanStatus.textColor = .black
                        cell.lblPlanDay.textColor = .black
                        cell.imgvPromoBG.isHidden = false
                        cell.imgvPlanBG.isHidden = true
                    }else {
                        //cell.lblPlanName.text = "₦" + ad.amount
                        cell.lblPlanName.text = "₦" + String(appDelegate.dicStorePlanPrice[ad.plan_id] ?? "")
                        cell.lblPlanStatus.text = amountPromo
                        cell.lblPlanDay.text = ad.plan_name
                        cell.imgvPromoBG.isHidden = true
                        cell.imgvPlanBG.isHidden = false
                        cell.lblPlanName.textColor = .yellow
                        cell.lblPlanStatus.textColor = .white
                        cell.lblPlanDay.textColor = .white
                        cell.lblPlanStatus.textColor = .white
                    }
                    
                    cell.layer.cornerRadius = 5
                    
                    if self.isCVReload == true {
                        if self.selectIndex == indexPath.row {
                            cell.viewBG.layer.borderColor = UIColor.black.cgColor
                            cell.viewBG.layer.borderWidth = 2
                        }else {
                            cell.viewBG.layer.borderColor = UIColor.clear.cgColor
                            cell.viewBG.layer.borderWidth = 0
                        }
                    }else {
                        if indexPath.row == 0 {
                            cell.viewBG.layer.borderColor = UIColor.black.cgColor
                            cell.viewBG.layer.borderWidth = 2
                        }
                    }
                }
                
            }else {
                
                
                if Int(ad.save_amount) == 0 {
                    amountPromo = "Pay ₦\(ad.amount) for \(ad.duration)"
                }else {
                    amountPromo = "Pay ₦\(ad.amount) for \(ad.duration)\nSave ₦\(ad.save_amount)"
                }
                
                cell.imgvPlanActiveTick.isHidden = true
                //if nsuserDefault.getUserDetails().isPromoCode == "yes" &&  indexPath.row == 0 {
                if nsuserDefault.getUserDetails().isPromoCode == "no" &&  indexPath.row == 0 && appDelegate.isSkipUser() != nil {
                    cell.lblPlanName.text = "Promo"
                    cell.lblPlanStatus.text = "Free for 1 month"
                    cell.lblPlanDay.text = ad.plan_name
                    cell.lblPlanName.textColor = .black
                    cell.lblPlanStatus.textColor = .black
                    cell.lblPlanDay.textColor = .black
                    cell.imgvPromoBG.isHidden = false
                    cell.imgvPlanBG.isHidden = true
                }else {
                    //cell.lblPlanName.text = "₦" + ad.amount
                    cell.lblPlanName.text = "₦" + String(appDelegate.dicStorePlanPrice[ad.plan_id] ?? "")
                    cell.lblPlanStatus.text = amountPromo
                    cell.lblPlanDay.text = ad.plan_name
                    cell.imgvPromoBG.isHidden = true
                    cell.imgvPlanBG.isHidden = false
                    cell.lblPlanName.textColor = .yellow
                    cell.lblPlanStatus.textColor = .white
                    cell.lblPlanDay.textColor = .white
                    cell.lblPlanStatus.textColor = .white
                }
                
                cell.layer.cornerRadius = 5
                
                if self.isCVReload == true {
                    if self.selectIndex == indexPath.row {
                        cell.viewBG.layer.borderColor = UIColor.black.cgColor
                        cell.viewBG.layer.borderWidth = 2
                    }else {
                        cell.viewBG.layer.borderColor = UIColor.clear.cgColor
                        cell.viewBG.layer.borderWidth = 0
                    }
                }else {
                    if indexPath.row == 0 {
                        cell.viewBG.layer.borderColor = UIColor.black.cgColor
                        cell.viewBG.layer.borderWidth = 2
                    }
                }
            }
        }
        return cell
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 140, height: 130)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.isCVReload = true
        self.collectionForSubscription.reloadData()
        self.selectActivePlane(index: indexPath.row)
    }
    
    
    
    func setUpDetailsCV() {
        
        var planId = [Int]()
        
        for i in 0..<self.arrGetPlan.count {
            let plan = activePlanDetails(dic: self.arrGetPlan[i] as! NSDictionary)
            if "3" == plan.plan_id {
                planId.append(i)
            }else if "4" == plan.plan_id {
                planId.append(i)
            }
        }
        
        for id in planId {
            self.arrGetPlan.remove(at: id)
        }
        
        var freePlan : Any!
        var promoPlan:Any!
        var twoMonthPlan: Any!
        var fourMonthPlan: Any!
        var sixMonthPlan: Any!
        var oneYearPlan: Any!
        
        for pl in self.arrGetPlan {
            let plan = activePlanDetails(dic: pl as! NSDictionary)
            if plan.plan_id == "5" {
                sixMonthPlan = pl
            }else if plan.plan_id == "6" {
                oneYearPlan = pl
            }else if plan.plan_id == "7" {
                twoMonthPlan = pl
            }else if plan.plan_id == "8" {
               fourMonthPlan = pl
            }else if plan.plan_id == "1" {
                freePlan = pl
            }else if plan.plan_id == "2" {
                promoPlan = pl
            }
        }
        
        self.arrGetPlan = [freePlan, promoPlan, twoMonthPlan, fourMonthPlan, sixMonthPlan, oneYearPlan]
        
        if appDelegate.isSkipUser() == nil {
            
            self.skipUserSetUpPlan()
            
        } else {
            
            if nsuserDefault.getUserDetails().activePlan.count != 0 {
                
                let ad = activePlanDetails(dic: nsuserDefault.getUserDetails().activePlan)
                
                if ad.plan_id != "1" {
                    self.arrGetPlan.remove(at: 0)
                }
                
                var indexx :Int?
                //if nsuserDefault.getUserDetails().isPromoCode == "yes" && ad.plan_type != "promo" {
                if nsuserDefault.getUserDetails().isPromoCode == "yes" && ad.plan_type != "promo" {
                    for i in 0...(self.arrGetPlan.count - 1) {
                        if let data = self.arrGetPlan[i] as? NSDictionary {
                            let dat = activePlanDetails(dic: data)
                            if dat.plan_id == "2" {
                                indexx = i
                            }
                        }
                    }
                }
                
                if let index = indexx {
                    self.arrGetPlan.remove(at: index)
                }
                
                var arr : Any?
                var index :Int?
                for i in 0...(self.arrGetPlan.count - 1) {
                    if let data = self.arrGetPlan[i] as? NSDictionary {
                        
                        let dat = activePlanDetails(dic: data)
                        let ad = activePlanDetails(dic: nsuserDefault.getUserDetails().activePlan)
                        if dat.plan_id == ad.plan_id {
                            index = i
                            arr = data
                        }
                    }
                }
                
                if let index = index {
                    self.arrGetPlan.remove(at: index)
                    if let arr = arr {
                        self.arrGetPlan.insert(arr, at: 0)
                    }
                }
                
                DispatchQueue.main.async {
                    self.collectionForSubscription.reloadData()
                }
                
                if arrGetPlan.count > 0{
                    
                    let add = activePlanDetails(dic: self.arrGetPlan[0] as! NSDictionary)
                    let dateFor = self.dateFormatterrrr(date: nsuserDefault.getUserDetails().subScribeEndDate)
                    
                    let endDate = nsuserDefault.getUserDetails().subScribeEndDate
                    let day = endDate!.countDay(date:endDate!)
                    
                    
                    let formattedString = NSMutableAttributedString()
                    formattedString
                        .normal("You are on a \(add.plan_name.lowercased()) plan and will expire on the ")
                        .bold("\(dateFor).")
                    
                    if (ad.plan_type == "Paid" && appDelegate.subscriptionPlanActiveTime() > 0.0) || ((ad.plan_type == "trial" || ad.plan_type == "promo") && day > 0) {
                        self.lblCurrentPlan.attributedText = formattedString
                    }else {
                        self.lblCurrentPlan.text = "You have no active plan, your \(add.plan_name.lowercased()) \(add.plan_type.lowercased()) has expired."
                    }
                    
                    if add.plan_type == "trial" {
                        btnSubscribe.isHidden = true
                    }else if add.plan_type == "promo" {
                        btnSubscribe.isHidden = false
                        btnSubscribe.setImage(UIImage(named: "subscribe_white_icon"), for: .normal)
                        btnSubscribe.layer.cornerRadius = btnSubscribe.frame.height / 2
                        btnSubscribe.layer.borderColor = slate_grey.cgColor
                        btnSubscribe.layer.borderWidth = 1
                        btnSubscribe.isEnabled = false
                    }else if add.plan_type == "Paid" {
                        btnSubscribe.isHidden = false
                        btnSubscribe.setImage(UIImage(named: "unsubscribe_icon"), for: .normal)
                        btnSubscribe.layer.cornerRadius = btnSubscribe.frame.height / 2
                        btnSubscribe.layer.borderColor = UIColor.clear.cgColor
                        btnSubscribe.layer.borderWidth = 0
                        btnSubscribe.isEnabled = true
                    }
                }
            } else {
                self.arrGetPlan.remove(at: 0)
                
                var indexx :Int?
                //if nsuserDefault.getUserDetails().isPromoCode == "no" {
                if nsuserDefault.getUserDetails().isPromoCode == "yes" {
                    for i in 0...(self.arrGetPlan.count - 1) {
                        if let data = self.arrGetPlan[i] as? NSDictionary {
                            let dat = activePlanDetails(dic: data)
                            if dat.plan_id == "2" {
                                indexx = i
                            }
                        }
                    }
                }
                
                if let index = indexx {
                    self.arrGetPlan.remove(at: index)
                }
                
                self.lblCurrentPlan.text = "You have no active plan."
                
                DispatchQueue.main.async {
                    self.collectionForSubscription.reloadData()
                }
            }
        }
    }
    
    
    fileprivate func skipUserSetUpPlan() {
        
        let endDate = appDelegate.expiryPlanDateSkipUser()
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        let dDate = dateFormate.date(from: endDate) ?? Date()
        dateFormate.dateFormat = "yyyy-MM-dd"
        let dString = dateFormate.string(from: dDate)
        
        var planName :String?
        var filterArr = [Any]()
        let planRemoveId = ["1", "2", "3", "4"]
        for i in 0..<self.arrGetPlan.count {
            let plan = activePlanDetails(dic: self.arrGetPlan[i] as! NSDictionary)
            if  planRemoveId.contains(plan.plan_id) == false {
                filterArr.append(self.arrGetPlan[i])
            }
        }
        self.arrGetPlan = filterArr
        
        if appDelegate.restoreSubscriptionPlan() == "com.zoomteller.fulltwo"  {
            self.restoreplanActiveFirst(id: "7")
             planName = "Two Months"
        }else if  appDelegate.restoreSubscriptionPlan() == "com.zoomteller.fullfour" {
            self.restoreplanActiveFirst(id: "8")
            planName = "Four Months"
        }else if appDelegate.restoreSubscriptionPlan() == "com.zoomteller.fullsix" {
            self.restoreplanActiveFirst(id: "5")
            planName = "Six Months"
        }else if appDelegate.restoreSubscriptionPlan() == "com.zoomteller.fulloneyear" {
            self.restoreplanActiveFirst(id: "6")
            planName = "One Year"
        }else {
            self.collectionForSubscription.reloadData()
        }
        
        let formattedString = NSMutableAttributedString()
        formattedString
            .normal("You are on a \((planName ?? "").lowercased()) plan and will expire on the ")
            .bold("\(dString).")
        
        if (appDelegate.subscriptionPlanActiveTimeSkipUser() > 0.0) && appDelegate.restoreSubscriptionPlan()  != nil {
            self.lblCurrentPlan.attributedText = formattedString
        }else {
            self.lblCurrentPlan.text = "You have no active plan."
        }
        
        if appDelegate.isSkipUser() == nil && appDelegate.restoreSubscriptionPlan() != nil {
            btnSubscribe.isHidden = false
            btnSubscribe.setImage(UIImage(named: "unsubscribe_icon"), for: .normal)
            btnSubscribe.layer.cornerRadius = btnSubscribe.frame.height / 2
            btnSubscribe.layer.borderColor = UIColor.clear.cgColor
            btnSubscribe.layer.borderWidth = 0
            btnSubscribe.isEnabled = true
        }
        
    }
    
    func restoreplanActiveFirst(id:String) {
        var planFirst : Any?
        var planRemoveIndex : Int?
        for i in 0..<self.arrGetPlan.count {
            let plan = activePlanDetails(dic: self.arrGetPlan[i] as! NSDictionary)
            if id == plan.plan_id {
                planFirst = self.arrGetPlan[i]
                planRemoveIndex = i
            }
        }
        
        if let index = planRemoveIndex {
            self.arrGetPlan.remove(at: index )
        }
        
        if let p = planFirst {
            self.arrGetPlan.insert(p, at: 0)
        }
        self.collectionForSubscription.reloadData()
    }
    
    func dateFormatterrrr(date:String) -> String {
        let dF = DateFormatter()
        dF.dateFormat = "yyyy-MM-dd"
        let dateaa = dF.date(from: date) ?? Date()
        dF.dateFormat = "dd"
        let day = dF.string(from: dateaa)
        dF.dateFormat = "MMM"
        let month = dF.string(from: dateaa)
        dF.dateFormat = "yyyy"
        let year = dF.string(from: dateaa)
        let d = "\(day)th of \(month), \(year)"
        return d
    }
    
    
    func selectActivePlane(index:Int) {
        
        let ad = activePlanDetails(dic: self.arrGetPlan[index] as! NSDictionary)
        
        self.selectIndex = index
        
        if appDelegate.isSkipUser() == nil {
            DispatchQueue.main.async {
                self.btnSubscribe.layer.borderColor = UIColor.clear.cgColor
                self.btnSubscribe.layer.borderWidth = 0
                print(index)
                if appDelegate.restoreSubscriptionPlan() != nil && index == 0 {
                    self.btnSubscribe.isHidden = false
                    self.btnSubscribe.setImage(UIImage(named: "unsubscribe_icon"), for: .normal)
                    self.btnSubscribe.isEnabled = true
                }else {
                    self.btnSubscribe.isHidden = false
                    self.btnSubscribe.setImage(UIImage(named: "subscribe_icon"), for: .normal)
                    self.btnSubscribe.isEnabled = true
                }
            }
        }else {
            
            if nsuserDefault.getUserDetails().activePlan.count != 0 {
                
                if index == 0 {
                    
                    if ad.plan_type == "trial" {
                        btnSubscribe.isHidden = true
                    }else if ad.plan_type == "promo" {
                        btnSubscribe.layer.cornerRadius = btnSubscribe.frame.height / 2
                        btnSubscribe.layer.borderColor = slate_grey.cgColor
                        btnSubscribe.layer.borderWidth = 1
                        btnSubscribe.isHidden = false
                        btnSubscribe.setImage(UIImage(named: "subscribe_white_icon"), for: .normal)
                        btnSubscribe.isEnabled = false
                    }else if ad.plan_type == "Paid" {
                        btnSubscribe.layer.borderColor = UIColor.clear.cgColor
                        btnSubscribe.layer.borderWidth = 0
                        btnSubscribe.isHidden = false
                        btnSubscribe.setImage(UIImage(named: "unsubscribe_icon"), for: .normal)
                        btnSubscribe.isEnabled = true
                    }
                }else {
                    btnSubscribe.layer.borderColor = UIColor.clear.cgColor
                    btnSubscribe.layer.borderWidth = 0
                    let ad = activePlanDetails(dic: self.arrGetPlan[index] as! NSDictionary)
                    if ad.plan_type == "trial" {
                        
                    }else if ad.plan_type == "promo" {
                        btnSubscribe.isHidden = false
                        btnSubscribe.setImage(UIImage(named: "active_icon"), for: .normal)
                        btnSubscribe.isEnabled = true
                    }else if ad.plan_type == "Paid" {
                        btnSubscribe.isHidden = false
                        btnSubscribe.setImage(UIImage(named: "subscribe_icon"), for: .normal)
                        btnSubscribe.isEnabled = true
                    }
                }
                
            }else {
                
                if ad.plan_type == "promo" {
                    btnSubscribe.isHidden = false
                    btnSubscribe.setImage(UIImage(named: "active_icon"), for: .normal)
                    btnSubscribe.isEnabled = true
                }else if ad.plan_type == "Paid" {
                    btnSubscribe.isHidden = false
                    btnSubscribe.setImage(UIImage(named: "subscribe_icon"), for: .normal)
                    btnSubscribe.isEnabled = true
                }
            }
        }
    }
    
    func btnSubscribeClick(index:Int) {
        
        var activePlan : String?
        
        if appDelegate.isSkipUser() == nil {
            if btnSubscribe.imageView?.image == UIImage(named: "unsubscribe_icon") {
                let alert = UIAlertController(title: "You're about to unsubscribe from an active plan", message: "You will lose out of the subscription benefits and deactive your current plan if you continue.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { (_) in
                    UIApplication.shared.open(URL(string: "https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/manageSubscriptions")!, options: [:], completionHandler: nil)
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else if btnSubscribe.imageView?.image == UIImage(named: "subscribe_icon") {
                if self.planeType == "Paid" && appDelegate.subscriptionPlanActiveTimeSkipUser() > 0.0 {
                    appDelegate.showToast(title: "", message: "You have already subscribe")
                }else {
                    DispatchQueue.main.async {
                        self.selectPlanId(index)
                    }
                }
            }
            
        }else {
            
            let add = activePlanDetails(dic: nsuserDefault.getUserDetails().activePlan)
            if add.plan_type == "trial" {
                activePlan = "you have an trial pack active"
            }else if add.plan_type == "promo" {
                activePlan = "you have an promo pack active"
            }else  {
                activePlan = "you have an paid pack active"
            }
            
            var alert = UIAlertController()
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            var done = UIAlertAction()
            
            if nsuserDefault.getUserDetails().activePlan.count != 0 {
                if btnSubscribe.imageView?.image == UIImage(named: "active_icon") {
                    if add.plan_type == "trial" {
                        alert = UIAlertController(title: "", message: activePlan!, preferredStyle: .alert)
                        done = UIAlertAction(title: "Continue", style: .default, handler: { (_) in
                            self.performSegue(withIdentifier: "promotionSegue", sender: self)
                        })
                        alert.addAction(done)
                        alert.addAction(cancel)
                        self.present(alert, animated: true, completion: nil)
                    }else {
                        self.performSegue(withIdentifier: "promotionSegue", sender: self)
                    }
                    
                }else if btnSubscribe.imageView?.image == UIImage(named: "subscribe_icon") {
                    let endDate = nsuserDefault.getUserDetails().subScribeEndDate
                    let day = endDate!.countDay(date:endDate!)
                    if ((add.plan_type == "trial" || add.plan_type == "promo") && day > 0){
                        alert = UIAlertController(title: "", message: activePlan!, preferredStyle: .alert)
                        done = UIAlertAction(title: "Continue", style: .default, handler: { (_) in
                            DispatchQueue.main.async {
                                self.selectPlanId(index)
                            }
                        })
                        alert.addAction(done)
                        alert.addAction(cancel)
                        self.present(alert, animated: true, completion: nil)
                    }else {
                        if self.planeType == "Paid" && appDelegate.subscriptionPlanActiveTime() > 0.0 {
                            appDelegate.showToast(title: "", message: "You have already subscribe")
                        }else {
                            DispatchQueue.main.async {
                                self.selectPlanId(index)
                            }
                        }
                    }
                }else if btnSubscribe.imageView?.image == UIImage(named: "unsubscribe_icon") {
                    
                    let alert = UIAlertController(title: "You're about to unsubscribe from an active plan", message: "You will lose out of the subscription benefits and deactive your current plan if you continue.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { (_) in
                        UIApplication.shared.open(URL(string: "https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/manageSubscriptions")!, options: [:], completionHandler: nil)
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    //appDelegate.showToast(title: "", message: "Plan Unsubscribe")
                    
                }
            }else {
                if btnSubscribe.imageView?.image == UIImage(named: "subscribe_icon") {
                    if self.planeType == "Paid" && appDelegate.subscriptionPlanActiveTime() > 0.0 {
                        appDelegate.showToast(title: "", message: "You have already subscribe")
                    }else {
                        DispatchQueue.main.async {
                            self.selectPlanId(index)
                        }
                    }
                }else if btnSubscribe.imageView?.image == UIImage(named: "active_icon") {
                    self.performSegue(withIdentifier: "promotionSegue", sender: self)
                }
            }
        }
    }
}



extension SubscriptionVC : SKProductsRequestDelegate {
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        for transaction in queue.transactions {
            let t: SKPaymentTransaction = transaction
            let prodID = t.payment.productIdentifier as String
            print(prodID)
            switch prodID {
            case "ProductID1": break
            // implement the given in-app purchase as if it were bought
            case "ProductID2": break
            // implement the given in-app purchase as if it were bought
            default: break
                //print("iap not found")
            }
        }
    }
    
    func selectPlanId(_ index:Int){
        let dicc = self.arrGetPlan[index] as! NSDictionary
        let dic = activePlanDetails(dic: dicc)
        if dic.plan_id == "7" {
            self.product_id = appDelegate.inAppPurchaseKeyTwoMonth
        }else if dic.plan_id == "8" {
            self.product_id = appDelegate.inAppPurchaseKeyFourMonth
        }else if dic.plan_id == "5" {
            self.product_id = appDelegate.inAppPurchaseKeySixMonth
        }else if dic.plan_id == "6" {
            self.product_id = appDelegate.inAppPurchaseKeyOneYear
        }
        self.fatchProductList()
    }
    
    func fatchProductList() {
        appDelegate.showHUD()
        if (SKPaymentQueue.canMakePayments())
        {
            let productID:NSSet = NSSet(object: self.product_id);
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            productsRequest.delegate = self;
            productsRequest.start();
            print("Fetching Products");
        }else{
            print("Can't make purchases");
        }
    }
    
    func buyProduct(product: SKProduct){
        DispatchQueue.main.async {
            print("Sending the Payment Request to Apple");
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(payment);
            appDelegate.hideHUD()
        }
    }
    
    func productsRequest (_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        let count : Int = response.products.count
        if (count>0) {
            let validProduct: SKProduct = response.products[0] as SKProduct
            
            if (validProduct.productIdentifier == self.product_id) {
                print(validProduct.localizedTitle)
                print(validProduct.localizedDescription)
                print(validProduct.price)
                buyProduct(product: validProduct);
                
            } else {
                appDelegate.hideHUD()
                print(validProduct.productIdentifier)
            }
        } else {
            print("nothing")
            appDelegate.hideHUD()
        }
    }
    
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Error Fetching product information");
    }
    
    func paymentQueue(_ queue: SKPaymentQueue,
                      updatedTransactions transactions: [SKPaymentTransaction])
        
    {
        //appDelegate.showHUD()
        print("Received Payment Transaction Response from Apple");
        
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction {
                switch trans.transactionState {
                case .purchased :
                    print("Product Purchased");
                    let identi = trans.payment.productIdentifier
                    
                    var expirationDate = Date()
                    let oneMonth : Double = 60 * 60 * 24 * 30
                    if identi == "com.zoomteller.fulltwo"  {
                        expirationDate = expirationDate.addingTimeInterval(oneMonth * 2)
                    }else if  identi == "com.zoomteller.fullfour" {
                        expirationDate = expirationDate.addingTimeInterval(oneMonth * 4)
                    }else if identi == "com.zoomteller.fullsix" {
                        expirationDate = expirationDate.addingTimeInterval(oneMonth * 6)
                    }else if identi == "com.zoomteller.fulloneyear" {
                        expirationDate = expirationDate.addingTimeInterval(oneMonth * 12)
                    }
                    
                    if appDelegate.isSkipUser() != nil {
                        appDelegate.showToast(title: "Product Purchased", message: "")
                        let details = activePlanDetails(dic: self.arrGetPlan[selectIndex] as! NSDictionary)
                        if transaction.transactionIdentifier! != nil {
                            //appDelegate.receiptValidation()
                            if identi == "com.zoomteller.fulltwo" || identi == "com.zoomteller.fullfour" || identi == "com.zoomteller.fullsix" || identi == "com.zoomteller.fulloneyear" {
                                USERDEFAULT.set(expirationDate, forKey: "expirationDateFromSubscription")
                                USERDEFAULT.set(identi, forKey: "productIdentifier")
                                USERDEFAULT.synchronize()
                                self.subscriptionWebService( planId: details.plan_id, planName: details.plan_name, transactionId: transaction.transactionIdentifier!, endDate: "")
                            }
                        }
                    }else {
                        if identi == "com.zoomteller.fulltwo" || identi == "com.zoomteller.fullfour" || identi == "com.zoomteller.fullsix" || identi == "com.zoomteller.fulloneyear" {
                            USERDEFAULT.set(expirationDate, forKey: "expirationDateFromSubscription")
                            USERDEFAULT.set(true, forKey: "restorePurchased")
                            USERDEFAULT.set(identi, forKey: "productIdentifier")
                            appDelegate.showToast(title: "Product Purchased", message: "")
                        }else {
                            USERDEFAULT.set(nil, forKey: "productIdentifier")
                            appDelegate.showToast(title: "Not any purchase", message: "")
                        }
                        USERDEFAULT.synchronize()
                        self.getPlanWebService()
                    }
                    
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    // Handle the purchase
                    //appDelegate.receiptValidation()
                    //adView.hidden = true
                    //appDelegate.showHUD()
                    break;
                case .failed:
                    print("Purchased Failed");
                    appDelegate.showToast(title: "Purchased Failed", message: "")
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    //appDelegate.showHUD()
                    break;
                case .restored:
                    
                    DispatchQueue.main.async {
                        appDelegate.receiptValidation()
                    }
                    
                    let identi = trans.payment.productIdentifier
                    if identi == "com.zoomteller.fulltwo" || identi == "com.zoomteller.fullfour" || identi == "com.zoomteller.fullsix" || identi == "com.zoomteller.fulloneyear" {
                        USERDEFAULT.set(true, forKey: "restorePurchased")
                        USERDEFAULT.set(identi, forKey: "productIdentifier")
                        appDelegate.showToast(title: "Restore Purchased", message: "")
                    }else {
                        USERDEFAULT.set(nil, forKey: "productIdentifier")
                        appDelegate.showToast(title: "Nothing to restore", message: "")
                    }
                    USERDEFAULT.synchronize()
                    
                    if appDelegate.isSkipUser() != nil  && appDelegate.restoreSubscriptionPlan() == nil && identi != appDelegate.restoreSubscriptionPlan() {
                        
                        let endDate = appDelegate.expiryPlanDateSkipUser()
                        let dateFormate = DateFormatter()
                        dateFormate.dateFormat = "yyyy-MM-dd HH:mm:ss z"
                        let dDate = dateFormate.date(from: endDate) ?? Date()
                        dateFormate.dateFormat = "yyyy-MM-dd"
                        let dString = dateFormate.string(from: dDate)
                        
                        if appDelegate.restoreSubscriptionPlan() == "com.zoomteller.fulltwo"  {
                            self.restoreplanActiveFirst(id: "7")
                            self.subscriptionWebService(planId: "7", planName: "Two Months", transactionId: trans.transactionIdentifier ?? "", endDate:dString)
                        }else if  appDelegate.restoreSubscriptionPlan() == "com.zoomteller.fullfour" {
                            self.restoreplanActiveFirst(id: "8")
                            self.subscriptionWebService(planId: "8", planName: "Four Months", transactionId: trans.transactionIdentifier ?? "", endDate:dString)
                        }else if appDelegate.restoreSubscriptionPlan() == "com.zoomteller.fullsix" {
                            self.restoreplanActiveFirst(id: "5")
                            self.subscriptionWebService(planId: "5", planName: "Six Months", transactionId: trans.transactionIdentifier ?? "", endDate:dString)
                        }else if appDelegate.restoreSubscriptionPlan() == "com.zoomteller.fulloneyear" {
                            self.restoreplanActiveFirst(id: "6")
                            self.subscriptionWebService(planId: "6", planName: "One Year", transactionId: trans.transactionIdentifier ?? "", endDate:dString)
                        }
                    }
                    break;
                default:
                    break;
                }
            }
        }
        
    }
}


extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: "AvenirNext-Medium", size: 12)!]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        
        return self
    }
}


extension String {
    
    func countDay(date:String) -> Int{
        if let today = nsuserDefault.getUserDetails().today , today != "" {
            let dF = DateFormatter()
            dF.dateFormat = "yyyy-MM-dd"
            let dateaa = dF.date(from: date ) ?? Date()
            let calendar = Calendar.current
            let dataDate = dF.date(from: today) ?? Date()
            let date1 = calendar.startOfDay(for: dataDate)
            let date2 = calendar.startOfDay(for: dateaa)
            let components = calendar.dateComponents([.day], from: date1, to: date2)
            return components.day ?? 0
        }else {
            let dF = DateFormatter()
            dF.dateFormat = "yyyy-MM-dd"
            let dateaa = dF.date(from: date) ?? Date()
            let calendar = Calendar.current
            let date1 = calendar.startOfDay(for: Date())
            let date2 = calendar.startOfDay(for: dateaa)
            let components = calendar.dateComponents([.day], from: date1, to: date2)
            return components.day ?? 0
        }
    }
    
    func countDaySkipUser(date:String) -> Int{
            let dF = DateFormatter()
            dF.dateFormat = "yyyy-MM-dd HH:mm:ss z"
            let dateaa = dF.date(from: date) ?? Date()
            let calendar = Calendar.current
            let date1 = calendar.startOfDay(for: Date())
            let date2 = calendar.startOfDay(for: dateaa)
            let components = calendar.dateComponents([.day], from: date1, to: date2)
            return components.day ?? 0
    }

}
