//
//  SubscriptionCell.swift
//  ZoomTeller
//
//  Created by Deep Gami on 15/06/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit

class SubscriptionCell: UICollectionViewCell {

    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var imgvPromoBG: UIImageView!
    @IBOutlet weak var imgvPlanActiveTick: UIImageView!
    @IBOutlet weak var imgvPlanBG: UIImageView!
    @IBOutlet weak var lblPlanName: UILabel!
    @IBOutlet weak var lblPlanDay: UILabel!
    @IBOutlet weak var lblPlanStatus: UILabel!

}

