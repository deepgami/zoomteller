//
//  C_MenuTableViewCell.swift
//  JEOZI
//
//  Created by ZERONES on 17/12/18.
//  Copyright © 2018 ZERONES. All rights reserved.
//

import UIKit

class C_MenuTableViewCell: UITableViewCell {

    @IBOutlet var lblMenu: UILabel!
    @IBOutlet var imgMenu: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
