//
//  Sp_MenuVC.swift
//  JEOZI
//
//  Created by ZERONES on 19/12/18.
//  Copyright © 2018 ZERONES. All rights reserved.
//

import UIKit

class Sp_MenuVC: UIViewController {
    
    @IBOutlet var sp_menu_table: UITableView!
    
    var rootVC = UITabBarController()
    
    var menuArray = ["History","Subscription","Promotion ","Support","About"]
    
    
    override func viewDidLoad() { super.viewDidLoad() }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        parentSp_MenuVC = self
        self.sp_menu_table.tableFooterView = UIView(frame: CGRect.zero)
        self.sp_menu_table.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let navVC = self.sideMenuController?.rootViewController as? UINavigationController
        self.rootVC = (navVC?.children.first as? UITabBarController)!
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}


//MARK: - UITableViewDelegate Methods
//MARK: -

extension Sp_MenuVC : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = sp_menu_table.dequeueReusableCell(withIdentifier:  "C_MenuTableViewCell", for: indexPath) as! C_MenuTableViewCell
        
        cell.lblMenu.text = self.menuArray[indexPath.row]
        // cell.imgMenu.image = self.menuImgArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(self.rootVC)
        
        if appDelegate.isSkipUser() == nil {
            
            if indexPath.row == 3 {
                self.sideMenuController?.hideLeftView(animated: true, completionHandler: {
                    self.rootVC.performSegue(withIdentifier: "segueToSupport", sender: self.rootVC)
                })
            }else if indexPath.row == 4 {
                self.sideMenuController?.hideLeftView(animated: true, completionHandler: {
                    if let url = URL(string: "http://zoomteller.com/about") {
                        UIApplication.shared.open(url)
                    }
                })
            }else if indexPath.row == 1 {
                self.sideMenuController?.hideLeftView(animated: true, completionHandler: {
                    self.rootVC.performSegue(withIdentifier: "segueToSubscription", sender: self.rootVC)
                })
                
            }else{
                self.sideMenuController?.hideLeftView(animated: true, completionHandler: {
                    appDelegate.appSkipAlert(vc: self)
                })
            }
            return
        }else {
            
            switch indexPath.row {
            case 0:
                //self.rootVC.previousView = self.rootVC
                self.rootVC.performSegue(withIdentifier: "segueToHistory", sender: self.rootVC)
                self.sideMenuController?.hideLeftView(animated: true, completionHandler: {
                    
                })
                break
            case 1:
                self.sideMenuController?.hideLeftView(animated: true, completionHandler: {
                    self.rootVC.performSegue(withIdentifier: "segueToSubscription", sender: self.rootVC)
                })
                
                break
            case 2:
                self.sideMenuController?.hideLeftView(animated: true, completionHandler: {
                    self.rootVC.performSegue(withIdentifier: "promotionSegue", sender: self.rootVC)
                })
                break
            case 3:
                self.sideMenuController?.hideLeftView(animated: true, completionHandler: {
                    self.rootVC.performSegue(withIdentifier: "segueToSupport", sender: self.rootVC)
                })
                break
            case 4:
                self.sideMenuController?.hideLeftView(animated: true, completionHandler: {
                    if let url = URL(string: "http://zoomteller.com/about") {
                        UIApplication.shared.open(url)
                    }
                })
            break //
            case 5:
                self.sideMenuController?.hideLeftView(animated: true, completionHandler: {
                    self.rootVC.performSegue(withIdentifier: "privacySegue", sender: self.rootVC)
                })
                break
            case 6:print("FAQs")
                break
            case 7:
                self.sideMenuController?.hideLeftView(animated: true, completionHandler: {
                    
                })
                break
            default:
                break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if appDelegate.isSkipUser() != nil {
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 150))
            let image = UIImage(named: "icon_check")
            let imageView = UIImageView(image: image!)
            imageView.contentMode = .scaleAspectFill
            imageView.frame = CGRect(x: 15, y: 0, width: 70, height: 70)
            imageView.layer.cornerRadius = 35
            imageView.clipsToBounds = true
            
            imageView.layer.masksToBounds = true
            imageView.layer.borderWidth = 1.5
            imageView.layer.borderColor = UIColor.white.cgColor
            
            let userDetail = nsuserDefault.getUserDetails()
            
            if let string = userDetail.profilePicture, !userDetail.profilePicture.isEmpty {
                imageView.af_setImage(withURL: URL.init(string: userDetail.profilePicture! )!)
            }
            
            
            headerView.addSubview(imageView)
            
            let label = UILabel()
            label.frame = CGRect.init(x: 15, y: 80, width: headerView.frame.width - 10, height: 20)
            label.text = "User name"
            label.font = UIFont.init(name: "Arial", size: 20)
            label.textColor = UIColor.white
            
            label.text = userDetail.name;
            
            
            let lblViewProfile = UILabel()
            lblViewProfile.frame = CGRect.init(x: 15, y: 100, width: headerView.frame.width - 10, height: 20)
            lblViewProfile.font = UIFont.init(name: "Arial", size: 13)
            lblViewProfile.textColor = UIColor.white
            lblViewProfile.text = "View Profile"
            
            
            let btn = UIButton()
            btn.frame = headerView.frame
            btn.titleLabel?.text = ""
            btn.addTarget(self, action: #selector(self
                .btnViewProfileClicked(_:)), for: .touchUpInside)
            
            
            headerView.addSubview(btn)
            headerView.addSubview(label)
            headerView.addSubview(lblViewProfile)
            
            return headerView
        }else {
            
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 0))
            return headerView
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if appDelegate.isSkipUser() != nil {
            return 150
        }else {
            return 75
        }
        
    }
    
    @objc func btnViewProfileClicked(_ sender : UIButton)
    {
        
        sideMenuController?.hideLeftViewAnimated()
        self.rootVC.selectedIndex = 2
    }
}
