//
//  ForgotPassVC.swift
//  ZoomTeller
//
//  Created by ZERONES on 18/03/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import NetCorePush

class ForgotPassVC: UIViewController {
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var viewEmail: UIView!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var btnSendLink: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.viewEmail.borderColor = UIColor.clear
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if #available(iOS 11.0, *){
            viewEmail.clipsToBounds = false
            viewEmail.layer.cornerRadius = 7
            viewEmail.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }else{
            let rectShape = CAShapeLayer()
            rectShape.bounds = viewEmail.frame
            rectShape.position = viewEmail.center
            rectShape.path = UIBezierPath(roundedRect: viewEmail.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 7, height: 7)).cgPath
            viewEmail.layer.mask = rectShape
        }
        
        
        self.txtEmail.attributedPlaceholder = NSAttributedString(string: "Email Address",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
            case UISwipeGestureRecognizer.Direction.down:
                keyBoardDownOnSwipeDown()
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    func keyBoardDownOnSwipeDown(){
        
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5) {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    //MARK: - Button Methods
    //MARK: -
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendLinkClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5) {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
        
        
        let email = txtEmail.text
        
        if !isValidEmail(testStr: email!)
        {
//            appDelegate.ShowBannerView(title: "Error", Subtitle: "Entered email address is invalid", style: .danger)
            self.alertView(message: "Entered email address is invalid")
            self.viewEmail.borderColor = invalid_line_color

        }
        else
        {
            self.view.endEditing(true)
            
            if(Reachability.isConnectedToNetwork() == true)
            {
                self.forgotPasswordWebservice(email: email!)
            }
            else
            {
                print("*******************************-: Network Reachability Error :-*******************************")
                appDelegate.showToast(title: "Error", message: "Please check internet connection")
            }
        }
    }
    
    // MARK: - Custom Click Events
    // MARK: -
    
    func forgotPasswordWebservice(email: String) {
        
        appDelegate.showHUD()
        
        let url = BASE_URL + API_FORGOT_PASSWORD
        
        let param = ["email":email]
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, status) in
            
            appDelegate.hideHUD()
            
            if status == nil
            {
                let res = response
                let status = res!["status"] as! Bool
                let msg = res!["message"] as! String
                
                print(status , msg)
                if status
                {
                    appDelegate.showToast(title: "Successful", message: res!.value(forKey: "message") as! String)
                    NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "\(email) User Forgot Password", payload: nil, block: nil)
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    appDelegate.showToast(title: "Error", message: res!.value(forKey: "message") as! String)
                }
            }
            else
            {
                appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK: - UITextFieldDelegate Methods
// MARK: -

extension ForgotPassVC : UITextFieldDelegate
{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        var y = 0
        
        if textField == txtEmail
        {
            y = -60
        }
        
        textField.becomeFirstResponder()
        
        UIView.animate(withDuration: 0.5) {
            self.view.frame = CGRect(x: 0, y: CGFloat(y), width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        UIView.animate(withDuration: 0.5) {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
        
        if textField == txtEmail
        {
            txtEmail.resignFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
}
