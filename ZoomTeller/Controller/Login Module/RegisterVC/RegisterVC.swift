//
//  RegisterVC.swift
//  ZoomTeller
//
//  Created by ZERONES on 18/03/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import NetCorePush
import UserNotificationsUI


class RegisterVC: UIViewController {
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewConfirmPass: UIView!
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var txtConfirmPass: UITextField!
    
    @IBOutlet weak var btnRegister: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.viewEmail.borderColor = UIColor.clear
        self.viewName.borderColor = UIColor.clear
        self.viewPassword.borderColor = UIColor.clear
        self.viewConfirmPass.borderColor = UIColor.clear
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if #available(iOS 11.0, *){
            viewName.clipsToBounds = false
            viewName.layer.cornerRadius = 7
            viewName.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            
            viewConfirmPass.clipsToBounds = false
            viewConfirmPass.layer.cornerRadius = 7
            viewConfirmPass.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            
        }else{
            let rectShape = CAShapeLayer()
            rectShape.bounds = viewName.frame
            rectShape.position = viewName.center
            rectShape.path = UIBezierPath(roundedRect: viewName.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 7, height: 7)).cgPath
            viewName.layer.mask = rectShape
            
            
            let rectShapeP = CAShapeLayer()
            rectShapeP.bounds = viewConfirmPass.frame
            rectShapeP.position = viewConfirmPass.center
            rectShapeP.path = UIBezierPath(roundedRect: viewConfirmPass.bounds,    byRoundingCorners: [.bottomLeft , .bottomRight], cornerRadii: CGSize(width: 7, height: 7)).cgPath
            viewConfirmPass.layer.mask = rectShapeP
        }
        
        
        self.txtName.attributedPlaceholder = NSAttributedString(string: "Name",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        self.txtEmail.attributedPlaceholder = NSAttributedString(string: "Email Address",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        
        self.txtPass.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        self.txtConfirmPass.attributedPlaceholder = NSAttributedString(string: "Confirm Password",
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
            case UISwipeGestureRecognizer.Direction.down:
                keyBoardDownOnSwipeDown()
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    func keyBoardDownOnSwipeDown(){
        
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5) {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    
    func performBack() {
        
        let n: Int! = self.navigationController?.viewControllers.count
        let myUIViewController = self.navigationController?.viewControllers[n-2] as! UIViewController
        
        let src = self
        let dst = myUIViewController
        
        src.view.superview?.insertSubview(dst.view, aboveSubview: src.view)
        dst.view.transform = CGAffineTransform(translationX: 0, y: -dst.view.frame.size.height)
        
        UIView.animate(withDuration: 0.4,
                       delay: 0.0,
                       options: .curveEaseInOut,
                       animations: {
                        dst.view.transform = CGAffineTransform(translationX: 0, y: 0)
        },
                       completion: { finished in
                        self.navigationController?.popViewController(animated: false)
                        
        }
        )
    }
    //MARK: - Button Methods
    //MARK: -
    
    @IBAction func btnSkipClicked(_ sender: Any) {
        let d = UserDefaults.standard
        let details = d.dictionaryRepresentation()
        details.keys.forEach { (key) in
            if key == "USER" {
                d.removeObject(forKey: key)
            }
        }
        let data = ["user_id" : ""]
        nsuserDefault.setUserDetails(logindata: LoginClass(fromDictionary: data))
        nsuserDefault.setIsLoggin(isLoggin: true)
        self.performSegue(withIdentifier: "homeSegue", sender: self)
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
        performBack()
    }
    
    @IBAction func btnRegisterClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5) {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
        
        
        let name = txtName.text
        let email = txtEmail.text
        let password = txtPass.text
        let confiemPass = txtConfirmPass.text
        
        if (name?.isEmpty)!
        {
            //            appDelegate.showToast(title: "Error", message: "Please enter email address")
            
            self.alertView(message: "Please enter name")
            self.viewName.borderColor = invalid_line_color
            self.viewPassword.borderColor = UIColor.clear
            self.viewEmail.borderColor = UIColor.clear
            self.viewConfirmPass.borderColor = UIColor.clear
            
        }
        else if (email?.isEmpty)!
        {
            //            appDelegate.showToast(title: "Error", message: "Please enter email address")
            
            self.alertView(message: "Please enter email address")
            //            Entered email address is invalid
            self.viewEmail.borderColor = invalid_line_color
            self.viewName.borderColor = UIColor.clear
            self.viewPassword.borderColor = UIColor.clear
            self.viewConfirmPass.borderColor = UIColor.clear
            
        }
        else if !isValidEmail(testStr: email!)
        {
            //            appDelegate.showToast(title: "Error", message: "Please enter email address")
            
            self.alertView(message: "Entered email address is invalid")
            
            self.viewEmail.borderColor = invalid_line_color
            self.viewName.borderColor = UIColor.clear
            self.viewPassword.borderColor = UIColor.clear
            self.viewConfirmPass.borderColor = UIColor.clear
            
        }
        else if (password?.isEmpty)!
        {
            //            appDelegate.showToast(title: "Error", message: "Please enter password")
            
            self.alertView(message: "Please enter password")
            
            self.viewPassword.borderColor = invalid_line_color
            self.viewName.borderColor = UIColor.clear
            self.viewEmail.borderColor = UIColor.clear
            self.viewConfirmPass.borderColor = UIColor.clear
        }
        else if (password?.count)! < 6
        {
            self.alertView(message: "Password must be 6 characters long")
            
            self.viewPassword.borderColor = invalid_line_color
            self.viewName.borderColor = UIColor.clear
            self.viewEmail.borderColor = UIColor.clear
            self.viewConfirmPass.borderColor = UIColor.clear
        }
        else if (confiemPass?.isEmpty)!
        {
            //            appDelegate.showToast(title: "Error", message: "Please enter password")
            
            self.alertView(message: "Please enter confirm password")
            self.viewConfirmPass.borderColor = invalid_line_color
            self.viewName.borderColor = UIColor.clear
            self.viewEmail.borderColor = UIColor.clear
            self.viewPassword.borderColor = UIColor.clear
        }
        else if !isValidConfirmPassword(password!, conform: confiemPass!)
        {
            self.alertView(message: "Password and confirm password must match")
            self.viewConfirmPass.borderColor = invalid_line_color
            self.viewName.borderColor = UIColor.clear
            self.viewEmail.borderColor = UIColor.clear
            self.viewPassword.borderColor = UIColor.clear
        }
        else
        {
            self.view.endEditing(true)
            
            self.viewEmail.borderColor = UIColor.clear
            self.viewPassword.borderColor = UIColor.clear
            self.viewName.borderColor = UIColor.clear
            self.viewConfirmPass.borderColor = UIColor.clear
            
            
            if(Reachability.isConnectedToNetwork() == true)
            {
                self.registerWebservice(name: name!, email: email!, password: password!)
            }
            else
            {
                print("*******************************-: Network Reachability Error :-*******************************")
                appDelegate.showToast(title: "Error", message: "Please check internet connection")
            }
        }
    }
    
    
    
    //MARK: - Webservice Methods
    //MARK: -
    
    fileprivate func netCoerTrackActivity() {
        
        UserDefaults.standard.set(true, forKey: "isShowSecondTab")
        UserDefaults.standard.synchronize()
        
        let payloadDict = NSMutableDictionary()
        payloadDict["USER_ID"] = nsuserDefault.getUserDetails().userId ?? ""
        payloadDict["EMAIL"]  = nsuserDefault.getUserDetails().email ?? ""
        payloadDict["PROFILE_IMAGE"]  = nsuserDefault.getUserDetails().profilePicture ?? ""
        payloadDict["USER_NAME"]  = nsuserDefault.getUserDetails().name ?? ""
        payloadDict["FB_ID"]  = nsuserDefault.getUserDetails().fb_id ?? ""
        payloadDict["TWITTER_ID"]  = nsuserDefault.getUserDetails().twitter_id ?? ""
        payloadDict["GOOGLE_ID"]  = nsuserDefault.getUserDetails().google_id ?? ""
        
        let user_id = nsuserDefault.getUserDetails().userId ?? ""
        let e_mail = nsuserDefault.getUserDetails().email ?? ""
        let profile_picture = nsuserDefault.getUserDetails().profilePicture ?? ""
        let user_name = nsuserDefault.getUserDetails().name ?? ""
        let fb_id = nsuserDefault.getUserDetails().fb_id ?? ""
        let twitter_id = nsuserDefault.getUserDetails().twitter_id ?? ""
        let google_id = nsuserDefault.getUserDetails().google_id ?? ""
        
        let profilePushDictionary = ["USER_ID": user_id,
                                     "EMAIL": e_mail,
                                     "PROFILE_IMAGE":profile_picture,
                                     "USER_NAME": user_name,
                                     "FB_ID": fb_id,
                                     "TWITTER_ID": twitter_id,
                                     "GOOGLE_ID": google_id]
        
        if let deviceToken = USERDEFAULT.value(forKey: "deviceToken") as? Data {
            NetCoreInstallation.sharedInstance()?.netCorePushRegisteration(e_mail, withDeviceToken: deviceToken) { (status) in }
            NetCoreSharedManager.sharedInstance()?.printDeviceToken()
            NetCoreSharedManager.sharedInstance().setUpIdentity(e_mail)
            NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "User Register", payload: payloadDict, block: nil)
            NetCoreInstallation.sharedInstance().netCoreProfilePush(e_mail, payload:profilePushDictionary, block:nil)
        }
        
    }
    
    func registerWebservice(name: String, email: String, password: String) {
        
        appDelegate.showHUD()
        
        let url = BASE_URL + API_SIGNUP
        
        let param = ["email":email,
                     "password":password,
                     "name" : name,
                     "device_type":"iPhone",
                     "device_token":appDelegate.deviceToken]
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, status) in
            
            appDelegate.hideHUD()
            
            if status == nil
            {
                let res = response
                
                if res?.value(forKey: "status") as! Bool == true
                {
                    // appDelegate.showToast(title: "Successful", message: res!.value(forKey: "message") as! String)
                    
                    let data = res?.object(forKey: "data") as? [String:Any]
                    
                    UserDefaults.standard.set(true, forKey: "isShowSecondTab")
                    UserDefaults.standard.synchronize()
                    
                    nsuserDefault.setUserDetails(logindata: LoginClass(fromDictionary: data!))
                    nsuserDefault.setIsLoggin(isLoggin: true)
                    self.netCoerTrackActivity()
                    self.performSegue(withIdentifier: "introSegue", sender: self)
                }
                else
                {
                    appDelegate.showToast(title: "Error", message: res!.value(forKey: "message") as! String)
                    
                }
            }
            else
            {
                appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
// MARK: - UITextFieldDelegate Methods
// MARK: -

extension RegisterVC : UITextFieldDelegate
{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        var y = 0
        
        if textField == txtName
        {
            y = -60
        }
        else if textField == txtEmail
        {
            y = -90
        }
        else if textField == txtPass
        {
            y = -150
        }
        else if textField == txtConfirmPass
        {
            y = -200
        }
        
        
        textField.becomeFirstResponder()
        
        UIView.animate(withDuration: 0.5) {
            self.view.frame = CGRect(x: 0, y: CGFloat(y), width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        UIView.animate(withDuration: 0.5) {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
        
        if textField == txtName
        {
            txtEmail.becomeFirstResponder()
        }
        else if textField == txtEmail
        {
            txtPass.becomeFirstResponder()
        }
        else if textField == txtPass
        {
            txtConfirmPass.becomeFirstResponder()
        }
        else if textField == txtConfirmPass
        {
            txtConfirmPass.resignFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
}
