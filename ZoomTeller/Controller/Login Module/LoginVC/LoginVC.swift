//
//  LoginVC.swift
//  ZoomTeller
//
//  Created by ZERONES on 18/03/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import TwitterKit
import NetCorePush
import CoreLocation

class LoginVC: UIViewController {

    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    
    @IBOutlet weak var btnRemember: UIButton!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    var isRemember : Bool = false
    
    var userProfileImage = String()
    var useremail = String()
    var userName = String()
    var userLastname = String()
    var userID = String()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        
        // set delegates
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        self.viewEmail.borderColor = UIColor.clear
        self.viewPassword.borderColor = UIColor.clear
        
        self.txtEmail.attributedPlaceholder = NSAttributedString(string: "Email Address",
                                                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        
        self.txtPass.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        nsuserDefault.setIsRemember(isLoggin: true)
        
        //self.locationDisableAlert()

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if #available(iOS 11.0, *){
            viewEmail.clipsToBounds = false
            viewEmail.layer.cornerRadius = 7
            viewEmail.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            
            viewPassword.clipsToBounds = false
            viewPassword.layer.cornerRadius = 7
            viewPassword.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            
        }else{
            let rectShape = CAShapeLayer()
            rectShape.bounds = viewEmail.frame
            rectShape.position = viewEmail.center
            rectShape.path = UIBezierPath(roundedRect: viewEmail.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 7, height: 7)).cgPath
            viewEmail.layer.mask = rectShape
            
            
            let rectShapeP = CAShapeLayer()
            rectShapeP.bounds = viewPassword.frame
            rectShapeP.position = viewPassword.center
            rectShapeP.path = UIBezierPath(roundedRect: viewPassword.bounds,    byRoundingCorners: [.bottomLeft , .bottomRight], cornerRadii: CGSize(width: 7, height: 7)).cgPath
            viewPassword.layer.mask = rectShapeP
            
        }
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @objc func locationDisableAlert() {

        let alert = UIAlertController.init(title: "Location Services Disabled!", message: "Please enable Location Based Services for better results! We promise to keep your location private, To view active ATMs nearby, location needs to be enabled", preferredStyle: .alert)

        let ok = UIAlertAction.init(title: "Settings", style: .default)
        { (action) in
            if !CLLocationManager.locationServicesEnabled()
            {
                appDelegate.openAppLocationSetting()
            }
            else
            {
                appDelegate.openAppLocationSetting()
            }
            
        }

        //let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (_) in
            //self.getInitialLocation()
            appDelegate.showToast(title: "", message: "To view active ATMs nearby, location needs to be enabled")
           
        }

        alert.addAction(ok)
        alert.addAction(cancel)

        self.present(alert, animated: true, completion: nil)

    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
            case UISwipeGestureRecognizer.Direction.down:
                keyBoardDownOnSwipeDown()
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    func keyBoardDownOnSwipeDown(){
        
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5) {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    //MARK: - Button Methods
    //MARK: -

    @IBAction func btnSkipClicked(_ sender: Any) {
        let d = UserDefaults.standard
        let details = d.dictionaryRepresentation()
        details.keys.forEach { (key) in
            if key == "USER" {
                d.removeObject(forKey: key)
            }
        }
        let data = ["user_id" : ""]
        nsuserDefault.setUserDetails(logindata: LoginClass(fromDictionary: data))
        nsuserDefault.setIsLoggin(isLoggin: true)
        self.performSegue(withIdentifier: "homeSegue", sender: self)
    }
    
    @IBAction func btnRememberClicked(_ sender: Any) {
        if self.isRemember
        {
            self.isRemember = false
            self.btnRemember.setBackgroundImage(UIImage(named: "icon_uncheck"), for: .normal)
            
            nsuserDefault.setIsRemember(isLoggin: false)
            
        }
        else
        {
            self.isRemember = true
            self.btnRemember.setBackgroundImage(UIImage(named: "icon_check"), for: .normal)
            
            nsuserDefault.setIsRemember(isLoggin: true)

        }
    }
    
    @IBAction func btnLoginClicked(_ sender: Any) {
        
        
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5) {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
        
        
        let email = txtEmail.text
        let password = txtPass.text
        
        if !isValidEmail(testStr: email!)
        {
//            appDelegate.showToast(title: "Error", message: "Please enter email address")
            
            self.alertView(message: "Please enter email address")
            self.viewEmail.borderColor = invalid_line_color
            self.viewPassword.borderColor = UIColor.clear
            
        }
        else if (password?.isEmpty)!
        {
//            appDelegate.showToast(title: "Error", message: "Please enter password")
            
            self.alertView(message: "Please enter password")
            self.viewPassword.borderColor = invalid_line_color
            self.viewEmail.borderColor = UIColor.clear
        }
        else
        {
            self.view.endEditing(true)
            
            self.viewEmail.borderColor = UIColor.clear
            self.viewPassword.borderColor = UIColor.clear
            
            if(Reachability.isConnectedToNetwork() == true)
            {
                self.customLoginWebservice(email: email!, password: password!)
            }
            else
            {
                print("*******************************-: Network Reachability Error :-*******************************")
                appDelegate.showToast(title: "Error", message: "Please check internet connection")
            }
        }
    }
    
    @IBAction func btnFBClicked(_ sender: Any) {
        
        if(Reachability.isConnectedToNetwork() == true)
        {
            let fbLoginManager : LoginManager = LoginManager()
            
            fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) in
                if (error == nil){
                    let fbloginresult : LoginManagerLoginResult = result!
                    if fbloginresult.grantedPermissions != nil {
                        if(fbloginresult.grantedPermissions.contains("email"))
                        {
                            self.getFBUserData()
                            fbLoginManager.logOut()
                        }
                    }
                }
            }
        }
        else
        {
            print("*******************************-: Network Reachability Error :-*******************************")
            appDelegate.showToast(title: "Error", message: "Please check internet connection")
        }
    }
    
    @IBAction func btnGmailClicked(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func btnTwitterClicked(_ sender: Any) {
        
        TWTRTwitter.sharedInstance().logIn { (session, error) in
            if (session != nil) {
                self.userName = session?.userName ?? ""
                self.userLastname = session?.userName ?? ""
                let client = TWTRAPIClient.withCurrentUser()
                client.requestEmail { email, error in
                    if (email != nil) {
                        print("signed in as \(String(describing: session?.userName))");
                        self.userName = session?.userName ?? ""
                        self.userLastname = session?.userName ?? ""
                        let recivedEmailID = email ?? ""
                        self.userID = session?.userID ?? ""
                        self.useremail = recivedEmailID
                        
                        if(Reachability.isConnectedToNetwork() == true)
                        {
                            self.callFB_GoogleLogin(id: self.userID, email: self.useremail, name: self.userName, lname: self.userLastname , loginType: "twitter")
                        }
                        else
                        {
                            print("*******************************-: Network Reachability Error :-*******************************")
                            appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
                        }
                        
                        print(session!)
                        print(self.userName)
                        print(self.userLastname)
                        print(recivedEmailID)
                        
                    }else {
                        print("error: \(String(describing: error?.localizedDescription))");
                    }
                }
            }else {
                print("error: \(String(describing: error?.localizedDescription))");
            }
        }
    }
    
    // MARK: - Custom Click Events 
    // MARK: -
    
    func getFBUserData(){
        
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, first_name, last_name,picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let dict = result as! [String : AnyObject]
                    print(dict)
                    
                    if let mail = (dict["email"] as? String)
                    {
                        self.useremail = mail
                    }
                    
                    if let name = (dict["first_name"] as? String)
                    {
                        self.userName = name
                    }
                    
                    if let lname = (dict["last_name"] as? String)
                    {
                        self.userLastname = lname
                    }
                    
                    if let id = (dict["id"] as? String)
                    {
                        self.userID = id
                    }
                    
                    if let pic = dict["picture"] as? NSDictionary
                    {
                        let profilePictureObj = pic
                        
                        let data = profilePictureObj["data"] as! NSDictionary
                        self.userProfileImage  = data["url"] as! String
                    }
                    
                    print(self.userID,self.useremail,self.userName,self.userProfileImage)
                    
                    if (self.userID == "" || self.useremail == "" || self.userName == "" || self.userLastname == ""){
                        appDelegate.showToast(title: "Error", message: "Please Check FacebookLogin")
                        return
                    }
                    else
                    {
                        self.view.endEditing(true)
                        
                        if(Reachability.isConnectedToNetwork() == true)
                        {
                            self.callFB_GoogleLogin(id: self.userID, email: self.useremail, name: self.userName, lname: self.userLastname , loginType: "facebook")
                        }
                        else
                        {
                            print("*******************************-: Network Reachability Error :-*******************************")
                            appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
                        }
                    }
                }
            })
        }
    }
    
    
    //MARK: - Webservice Methods
    //MARK: -

    func callFB_GoogleLogin(id : String, email : String, name : String, lname : String, loginType : String)
    {
        appDelegate.showHUD()
        
        let Login = BASE_URL + API_SOCIAL_LOGIN
        
        var loginId = String()
        
        if loginType == "google"
        {
            loginId = "google_id"
        }
        else if loginType == "facebook"
        {
            loginId = "fb_id"
        }
        else
        {
            loginId = "twitter_id"
        }
        
//        parameter :`name`, `email`,device_type(Android,iPhone),device_token,login_type('facebook', 'google', 'custom',twitter),fb_id,google_id,twitter_id
        
        let param = ["name": name,
                     "email": email,
                     "\(loginId)" : id,
                     "login_type" : loginType,
                     "device_type":"iPhone",
                     "device_token":appDelegate.deviceToken]
        
        print(param)
        
        ApiHelper.sharedInstance.CallWebservice(url: Login, param: param, completion: { (response, status) in
            
            appDelegate.hideHUD()
            
            if  status == nil
            {
                let res = response
                let status = res!["status"] as! Bool
                let msg = res!["message"] as! String
                
                print(status , msg)
                
                if status
                {
                    appDelegate.showToast(title: "Successful", message: res!.value(forKey: "message") as! String)
                    
                    let data = res?.object(forKey: "data") as? [String:Any]
                    
                    UserDefaults.standard.set(true, forKey: "isShowSecondTab")
                    UserDefaults.standard.synchronize()
                    nsuserDefault.setUserDetails(logindata: LoginClass(fromDictionary: data!))
                    nsuserDefault.setIsLoggin(isLoggin: true)
                    UserDefaults.standard.synchronize()
                    self.netCoerTrackActivity()
                    
                    self.performSegue(withIdentifier: "homeSegue", sender: self)
                }
                else
                {
                    appDelegate.showToast(title: "Warning", message: res!.value(forKey: "message") as! String)
                }
            }
            else
            {
                appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
            }
            
        })
    }
    
    
    func customLoginWebservice(email: String, password: String) {
        
        appDelegate.showHUD()
        
        let url = BASE_URL + API_LOGIN
        
        let param = ["email":email,
                     "password":password,
                     "login_type" : "custom",
                     "device_type":"iPhone",
                     "device_token":appDelegate.deviceToken]
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, status) in
            
            appDelegate.hideHUD()
            
            if status == nil
            {
                let res = response
                
                if res?.value(forKey: "status") as! Bool == true
                {
                   // appDelegate.showToast(title: "Successful", message: res!.value(forKey: "message") as! String)

                    let data = res?.object(forKey: "data") as? [String:Any]
                    
                    UserDefaults.standard.set(true, forKey: "isShowSecondTab")
                    nsuserDefault.setUserDetails(logindata: LoginClass(fromDictionary: data!))
                    nsuserDefault.setIsLoggin(isLoggin: true)
                    UserDefaults.standard.synchronize()
                    self.netCoerTrackActivity()

                    
                    self.performSegue(withIdentifier: "homeSegue", sender: self)
                }
                else
                {
                    appDelegate.showToast(title: "Error", message: res!.value(forKey: "message") as! String)
                    
                }
            }
            else
            {
                appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
            }
        }
    }

    fileprivate func netCoerTrackActivity() {
        
        UserDefaults.standard.set(true, forKey: "isShowSecondTab")
        UserDefaults.standard.synchronize()
        
        let payloadDict = NSMutableDictionary()
        payloadDict["USER_ID"] = nsuserDefault.getUserDetails().userId ?? ""
        payloadDict["EMAIL"]  = nsuserDefault.getUserDetails().email ?? ""
        payloadDict["PROFILE_IMAGE"]  = nsuserDefault.getUserDetails().profilePicture ?? ""
        payloadDict["USER_NAME"]  = nsuserDefault.getUserDetails().name ?? ""
        payloadDict["FB_ID"]  = nsuserDefault.getUserDetails().fb_id ?? ""
        payloadDict["TWITTER_ID"]  = nsuserDefault.getUserDetails().twitter_id ?? ""
        payloadDict["GOOGLE_ID"]  = nsuserDefault.getUserDetails().google_id ?? ""
        
        let user_id = nsuserDefault.getUserDetails().userId ?? ""
        let e_mail = nsuserDefault.getUserDetails().email ?? ""
        let profile_picture = nsuserDefault.getUserDetails().profilePicture ?? ""
        let user_name = nsuserDefault.getUserDetails().name ?? ""
        let fb_id = nsuserDefault.getUserDetails().fb_id ?? ""
        let twitter_id = nsuserDefault.getUserDetails().twitter_id ?? ""
        let google_id = nsuserDefault.getUserDetails().google_id ?? ""
        
        let profilePushDictionary = ["USER_ID": user_id,
                                     "EMAIL": e_mail,
                                     "PROFILE_IMAGE":profile_picture,
                                     "USER_NAME": user_name,
                                     "FB_ID": fb_id,
                                     "TWITTER_ID": twitter_id,
                                     "GOOGLE_ID": google_id]
        
        if let deviceToken = USERDEFAULT.value(forKey: "deviceToken") as? Data {
            NetCoreInstallation.sharedInstance().netCorePushRegisteration(e_mail, withDeviceToken: deviceToken) { (status) in }
            NetCoreSharedManager.sharedInstance()?.printDeviceToken()
            NetCoreSharedManager.sharedInstance().setUpIdentity(e_mail)
            NetCoreInstallation.sharedInstance().netCoreProfilePush(e_mail, payload:profilePushDictionary, block:nil)
            NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "User Login", payload: payloadDict, block: nil)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UITextFieldDelegate Methods
// MARK: -

extension LoginVC : UITextFieldDelegate
{
   
    func textFieldDidBeginEditing(_ textField: UITextField) {
        var y = 0
        
        if textField == txtEmail
        {
            y = -60
        }
        else if textField == txtPass
        {
            y = -90
        }
        
        textField.becomeFirstResponder()
        
        UIView.animate(withDuration: 0.5) {
            self.view.frame = CGRect(x: 0, y: CGFloat(y), width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        UIView.animate(withDuration: 0.5) {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
        
        if textField == txtEmail
        {
            txtPass.becomeFirstResponder()
        }
        else if textField == txtPass
        {
            txtPass.resignFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
}

// MARK: - GIDSignInDelegate Methods
// MARK: -

extension LoginVC : GIDSignInDelegate , GIDSignInUIDelegate
{
    //email,password,device_type(Android,iPhone),device_token,login_type(Custom,Facebook,Google),name,profile_picture,fb_id,google_id
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        //if any error stop and print the error
        if error != nil{
            print(error ?? "google error")
            return
        }
        
        let userId = user.userID
        let idToken = user.authentication.idToken // Safe to send to the server
        let name = user.profile.givenName
        let lname = user.profile.familyName
        let email = user.profile.email
        var profilePicture : URL?
        
        if user.profile.hasImage
        {
            profilePicture = user.profile.imageURL(withDimension: 500)
        }
        else
        {
        }
        
        print(userId!)// For client-side use only!
        print(idToken!)
        print(name!)
        print(lname!)
        print(email!)
        print(profilePicture!)
        
        if (userId == "" || name == "" || lname == "" || email == ""){
            appDelegate.showToast(title: "Error", message: "Please Check GoogleLogin")
            return
        }
        else
        {
            self.view.endEditing(true)
            
            if(Reachability.isConnectedToNetwork() == true)
            {
                self.callFB_GoogleLogin(id: userId!, email: email!, name: name!, lname: lname!, loginType: "google")
                
            }
            else
            {
                print("*******************************-: Network Reachability Error :-*******************************")
                appDelegate.showToast(title: "Error", message: "Please check internet connection")
            }
        }
        
    }
}
