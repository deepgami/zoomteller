//
//  EnterCurrentPasswordVC.swift
//  ZoomTeller
//
//  Created by ZerOnes on 27/04/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import NetCorePush

class EnterCurrentPasswordVC: UIViewController ,UITextFieldDelegate{

    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var viewPassword: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtPassword.delegate = self
        // Do any additional setup after loading the view.
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "edit profile activity open", payload: payloadDict, block: nil)
        
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtPassword.resignFirstResponder()
        return true
    }
    
    @IBAction func onClickNext(_ sender: Any) {
        let password = txtPassword.text
        
        if (password?.isEmpty)!
        {
            self.alertView(message: "Please enter password")
            self.viewPassword.borderColor = invalid_line_color
        }else{
            self.view.endEditing(true)
            
            self.viewPassword.borderColor = UIColor.clear
            
            if(Reachability.isConnectedToNetwork() == true)
            {
               CheckCurrentPassword()
            }
            else
            {
                appDelegate.showToast(title: "Error", message: "Please check internet connection")
            }
        }
    }
    
    func CheckCurrentPassword() {
        
        appDelegate.showHUD()
        
        let url = BASE_URL + API_CHANGE_PASSWORD
        
        let userDetail = nsuserDefault.getUserDetails()
        
        let param = ["user_id":userDetail.userId as String,
                     "old_password": self.txtPassword.text!,
                     "new_password":""]
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, status) in
            appDelegate.hideHUD()
            if status == nil
            {
                let res = response
                if res?.value(forKey: "status") as! Bool == true
                {
                    
                    self.performSegue(withIdentifier: "goToChangePassword", sender: self)
                }
                else
                {
                    self.viewPassword.borderColor = invalid_line_color
                    
                    appDelegate.showToast(title: "Error", message: res!.value(forKey: "message") as! String)
                    
                }
            }
            else
            {
                appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
            }
        }
    }
    
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToChangePassword"
        {
            let cpvc = segue.destination as! ChangePasswordVC;
            cpvc.oldPassword = self.txtPassword.text;
        }
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
