//
//  UpdateUserNameVC.swift
//  ZoomTeller
//
//  Created by ZerOnes on 27/04/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import NetCorePush

class UpdateUserNameVC: UIViewController ,UITextFieldDelegate{

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var viewUsername: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userDetail = nsuserDefault.getUserDetails()
        txtUsername.delegate = self
        self.txtUsername.text = userDetail.name
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "edit profile activity open", payload: payloadDict, block: nil)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtUsername.resignFirstResponder()
        return true
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onClickNext(_ sender: Any) {
        let password = txtUsername.text
        
        if (password?.isEmpty)!
        {
            self.alertView(message: "Please enter password")
            self.viewUsername.borderColor = invalid_line_color
        }else{
            self.view.endEditing(true)
            
            self.viewUsername.borderColor = UIColor.clear
            
            if(Reachability.isConnectedToNetwork() == true)
            {
                updateUserName()
            }
            else
            {
                appDelegate.showToast(title: "Error", message: "Please check internet connection")
            }
        }
    }
    
    func updateUserName() {
        
        appDelegate.showHUD()
        
        let url = BASE_URL + API_EDIT_PROFILE
        
        let userDetail = nsuserDefault.getUserDetails()
        
        let param = ["user_id":userDetail.userId as String,
                     "name": self.txtUsername.text!]
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, status) in
            appDelegate.hideHUD()
            if status == nil
            {
                let res = response
                if res?.value(forKey: "status") as! Bool == true
                {
                    
                    let data = res?.value(forKey: "data") as? [String:Any] ?? [String:Any]()
                    nsuserDefault.setUserDetails(logindata: LoginClass(fromDictionary: data))
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    self.viewUsername.borderColor = invalid_line_color
                    appDelegate.showToast(title: "Error", message: res!.value(forKey: "message") as! String)
                    
                }
            }
            else
            {
                appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
            }
        }
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
