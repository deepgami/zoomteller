//
//  ProfileVC.swift
//  ZoomTeller
//
//  Created by ZerOnes on 26/04/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
import Alamofire
import NetCorePush
import TwitterKit
import FBSDKLoginKit
import GoogleSignIn

class ProfileVC: UIViewController {
    @IBOutlet weak var constriantViewChangePassword: NSLayoutConstraint!
    @IBOutlet weak var imgTabBrowse: UIImageView!
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var imgTabMap: UIImageView!
    @IBOutlet weak var imgTabProfile: UIImageView!
    @IBOutlet weak var imgPlanActive: UIImageView!

    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var btnMenu: UIButton!

    @IBOutlet weak var btnChangeProfilePicture: UIButton!
    @IBOutlet weak var lblExpires: UILabel!
    @IBOutlet weak var lblCurrentPlan: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var viewChangePassword: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgProfilePic.layer.cornerRadius = imgProfilePic.frame.width/2
        imgTabProfile.clipsToBounds = true;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if nsuserDefault.getUserDetails().login_type == "custom" {
            constriantViewChangePassword.constant = 70
            viewChangePassword.isHidden = false
        }else {
            constriantViewChangePassword.constant = 0
            viewChangePassword.isHidden = true
        }
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "my profile activity open", payload: payloadDict, block: nil)
        
        setData()                                                              
        if let _ = parentSp_MenuVC {
            parentSp_MenuVC.sp_menu_table.reloadData()
        }
    }
    
    func setData(){
        
        let userDetail = nsuserDefault.getUserDetails()
        lblEmail.text = userDetail.email
        lblUserName.text = userDetail.name;
        
        if let _ = userDetail.profilePicture, !userDetail.profilePicture.isEmpty {
            imgProfilePic.af_setImage(withURL: URL.init(string: userDetail.profilePicture! )!)
        }
        
        
        if nsuserDefault.getUserDetails().activePlan.count != 0 {
            imgPlanActive.isHidden = false
            let plan = activePlanDetails(dic: nsuserDefault.getUserDetails().activePlan)
            if plan.plan_type == "trial" {
                lblCurrentPlan.text = "Trial" + " \(plan.duration)"
                lblExpires.text = "Expires :" + "\(nsuserDefault.getUserDetails().subScribeEndDate ?? "")"
            }else if plan.plan_type == "promo" {
                lblCurrentPlan.text = "Promo" + " \(plan.duration)"
                lblExpires.text = "Expires :" + "\(nsuserDefault.getUserDetails().subScribeEndDate ?? "")"
            }else if plan.plan_type == "Paid" {
                lblCurrentPlan.text = "Paid" + " \(plan.duration)"
                lblExpires.text = "Expires :" + "\(nsuserDefault.getUserDetails().subScribeEndDate ?? "")"
            }
        }else {
            
            //lblCurrentPlan.text =  "You have no active plan."
            
            if  nsuserDefault.getUserDetails().subScribeEndDate != "" {
                if nsuserDefault.getUserDetails().subScribeEndDate.countDay(date: nsuserDefault.getUserDetails().subScribeEndDate) > 0  {
                    lblCurrentPlan.text = nsuserDefault.getUserDetails().subScribeEndDate ?? "" // "You have no active plan."
                }else {
                     lblCurrentPlan.text =  "You have no active plan."
                }
            } else {
                lblCurrentPlan.text =  "You have no active plan."
            }
            
            lblExpires.text = ""
            imgPlanActive.isHidden = true
        }
        
      
    }
    
    // MARK: - IBAction method
    
    
    
    @IBAction func onClickMenu(_ sender: Any) {
        self.sideMenuController?.showLeftViewAnimated()
    }
    
    @IBAction func onClickChangeProfilePicture(_ sender: Any) {
        let alert = UIAlertController(title: "Select for profile", message: "", preferredStyle: .actionSheet)
        
        let CameraAction = UIAlertAction(title: "Camera", style: .default) { (camera) in
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
            {
                self.checkCamera()
            }
            else
            {
                appDelegate.showToast(title: "Error", message: "You don't have camera")
            }
        }
        
        let GalleryAction = UIAlertAction(title: "Gallery", style: .default) { (gallery) in
            self.openPhotoGallary()
        }
        
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.modalPresentationStyle = .popover
        alert.addAction(CameraAction)
        alert.addAction(GalleryAction)
        alert.addAction(CancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnTabBarClicked(_ sender: Any) {
        let button = sender as! UIButton
        tabBarController?.selectedIndex = button.tag
        
        
    }
    
    @IBAction func onClickChangeUserName(_ sender: Any) {
         self.performSegue(withIdentifier: "goToUpdateUserName", sender: self)
    }
    
    @IBAction func btnChangePassword(_ sender: Any) {
        self.performSegue(withIdentifier: "goToCurrentPassword", sender: self)
    }
    
    @IBAction func onClickLogout(_ sender: Any) {
        let alertController = UIAlertController(title: "Are you sure ?", message: "", preferredStyle: .alert)
        
        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
       
        }
        
        let action3 = UIAlertAction(title: "Log out", style: .destructive) { (action:UIAlertAction) in
            let payloadDict = NSMutableDictionary()
            payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
            NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "User Logout", payload: payloadDict, block: nil)
            NetCoreInstallation.sharedInstance().netCorePushLogout(nil)
            NetCoreSharedManager.sharedInstance().clearIdentity()
            let defualt = UserDefaults.standard
            let dic = defualt.dictionaryRepresentation()
            dic.keys.forEach({ (key) in
                defualt.removeObject(forKey: key)
                defualt.synchronize()
            })
            defualt.synchronize()
            let store =  TWTRTwitter.sharedInstance().sessionStore //Twitter.sharedInstance().sessionStore
            if let userID = store.session()?.userID {
                store.logOutUserID(userID)
            }
            let manager = LoginManager()
            manager.logOut()
            GIDSignIn.sharedInstance()?.signOut()
            appDelegate.checkLoginStatus()
        }
        alertController.addAction(action2)
        alertController.addAction(action3)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Profile Pick Events
    // MARK: -
    @objc func openCamera() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerController.SourceType.camera
        myPickerController.allowsEditing = true
        
        self.present(myPickerController, animated: true, completion: nil)
        
        NSLog("Camera");
    }
    
    func checkCamera() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        
        if authStatus == .authorized
        {
            self.openCamera()
        }
        else if authStatus == .notDetermined
        {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted
                {
                    self.openCamera()
                }
                else
                {
                    self.alertToEncourageCameraAccessInitially()
                }
            })
        }
        else if authStatus == .denied
        {
            self.alertPromptToAllowCameraAccessViaSetting()
        }
    }
    
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Camera access required for capturing photos!",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Camera access required for capturing photos!",
            preferredStyle: UIAlertController.Style.alert
        )
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel) { alert in
            
            let captureDeviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .unspecified)
            
            let captureDevices = captureDeviceDiscoverySession.devices
            
            if captureDevices.count > 0 {
                AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                    DispatchQueue.main.async() {
                        self.alertToEncourageCameraAccessInitially()
                    }
                }
            }
        })
        
        present(alert, animated: true, completion: nil)
    }
    
    func openPhotoGallary() {
        
        let authorizedStatus = PHPhotoLibrary.authorizationStatus()
        
        if authorizedStatus == PHAuthorizationStatus.authorized
        {
            if UIImagePickerController.isSourceTypeAvailable(
                UIImagePickerController.SourceType.savedPhotosAlbum) {
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType =
                    UIImagePickerController.SourceType.photoLibrary
                imagePicker.mediaTypes = [kUTTypeImage as String]
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true,
                             completion: nil)
            }
        }
        else if authorizedStatus == PHAuthorizationStatus.denied
        {
            let alertMessage = UIAlertController(title: "Error" , message: "Please provide gallery permissions in settings" , preferredStyle: .alert)
            
            alertMessage.addAction(UIAlertAction(title: "Ok" , style: .default
                , handler: { (action) in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
            }))
            alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            
            self.present(alertMessage, animated: false, completion: nil)
        }
        else if authorizedStatus == PHAuthorizationStatus.notDetermined
        {
            PHPhotoLibrary.requestAuthorization { (status) in
                
                if status == PHAuthorizationStatus.authorized
                {
                    
                    if UIImagePickerController.isSourceTypeAvailable(
                        UIImagePickerController.SourceType.savedPhotosAlbum) {
                        let imagePicker = UIImagePickerController()
                        
                        imagePicker.delegate = self
                        imagePicker.sourceType =
                            UIImagePickerController.SourceType.photoLibrary
                        imagePicker.mediaTypes = [kUTTypeImage as String]
                        imagePicker.allowsEditing = true
                        self.present(imagePicker, animated: true,
                                     completion: nil)
                    }
                }
                else
                {
                    let alertMessage = UIAlertController(title: "Error" , message: "Please provide gallery permissions in settings" , preferredStyle: .alert)
                    
                    alertMessage.addAction(UIAlertAction(title: "Ok" , style: .default
                        , handler: { (action) in
                            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                return
                            }
                            
                            if UIApplication.shared.canOpenURL(settingsUrl) {
                                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                    print("Settings opened: \(success)") // Prints true
                                })
                            }
                    }))
                    alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    self.present(alertMessage, animated: false, completion: nil)
                }
            }
        }
        else
        {
            
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ProfileVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let selectedImage = info[.editedImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        imgProfilePic.image = selectedImage
        self.UploadPhotoWebservice()
        dismiss(animated: true, completion: nil)
    }
    
    func UploadPhotoWebservice() {
        let url = BASE_URL + API_EDIT_PROFILE
        let userDetail = nsuserDefault.getUserDetails()
        
        let param = ["user_id":userDetail.userId as String]
        
        let img = imgProfilePic.image!.jpegData(compressionQuality: 0.2)
        
        if img != nil
        {
            appDelegate.showHUD()
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(img!, withName: "profile_picture",fileName: "ios_Profile_image.png", mimeType: "image/png")
                
                for (key,value) in param
                {
                    multipartFormData.append((value).data(using: String.Encoding.utf8)!, withName: key)
                }
                
            },to:url)
            { (result) in
                
                switch result {
                    
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (Progress) in
                        print("Upload Progress: \(Progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        appDelegate.hideHUD()
                        
                        if  response.result.value != nil
                        {
                            print(response.result.value!)
                            
                            let res = response.result.value as! NSDictionary
                            let status = res["status"] as! Bool
                            let msg = res["message"] as! String
                            
                            if status
                            {
                                appDelegate.showToast(title: "Success", message: msg)
                                
                                let data = res.object(forKey: "data") as? [String:Any]
                                nsuserDefault.setUserDetails(logindata: LoginClass(fromDictionary: data!))
                                nsuserDefault.setIsLoggin(isLoggin: true)
                                self.imgProfilePic.af_setImage(withURL: URL(string: data!["profile_picture"] as? String ?? "")!)
                                if let _ = parentSp_MenuVC {
                                    parentSp_MenuVC.sp_menu_table.reloadData()
                                }
                                
                            }
                            else
                            {
                                appDelegate.showToast(title: "Error", message: msg)
                            }
                        }
                        else
                        {
                            appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
                        }
                    }
                    
                case .failure(let encodingError):
                    appDelegate.hideHUD()
                    
                    print(encodingError.localizedDescription)
                    appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
            
                }
            }
        }
        
    }
    
    @objc func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo:UnsafeRawPointer) {
        
        if error != nil {
            
            let alert = UIAlertController(title: "",
                                          message: "Failed to save image",
                                          preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "OK",
                                             style: .cancel, handler: nil)
            
            alert.addAction(cancelAction)
            self.present(alert, animated: true,
                         completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}

