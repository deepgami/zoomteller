//
//  ChangePasswordVC.swift
//  ZoomTeller
//
//  Created by ZerOnes on 27/04/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import NetCorePush

class ChangePasswordVC: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewConfPassword: UIView!

    public var oldPassword:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtConfirmPassword.delegate = self
        self.txtPassword.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "edit profile activity open", payload: payloadDict, block: nil)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtPassword.resignFirstResponder()
        self.txtConfirmPassword.resignFirstResponder()
        return true
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func onClickNext(_ sender: Any) {
        let password = txtPassword.text
        let Cpassword = txtConfirmPassword.text
        
        
        if (password?.isEmpty)!
        {
            self.alertView(message: "Please enter password")
            self.viewPassword.borderColor = invalid_line_color
        }else if (Cpassword?.isEmpty)!
        {
            self.alertView(message: "Please enter confirm password")
            self.viewConfPassword.borderColor = invalid_line_color
        }else if password != Cpassword{
            appDelegate.showToast(title: "Error", message: "Confirm password can't match")
            
        }else {
            self.view.endEditing(true)
            
            self.viewPassword.borderColor = UIColor.clear
            self.viewConfPassword.borderColor = UIColor.clear
            
            if(Reachability.isConnectedToNetwork() == true)
            {
                CheckCurrentPassword()
            }
            else
            {
                appDelegate.showToast(title: "Error", message: "Please check internet connection")
            }
        }

    }
    
    func CheckCurrentPassword() {
        
        appDelegate.showHUD()
        
        let url = BASE_URL + API_CHANGE_PASSWORD
        
        let userDetail = nsuserDefault.getUserDetails()
        
        let param = ["user_id":userDetail.userId as String,
                     "old_password": oldPassword,
                     "new_password":self.txtPassword.text!]
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param as! [String : String]) { (response, status) in
            appDelegate.hideHUD()
            if status == nil
            {
                let res = response
                if res?.value(forKey: "status") as! Bool == true
                {
                    
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                    self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)

                }
                else
                {
                    appDelegate.showToast(title: "Error", message: res!.value(forKey: "message") as! String)
                }
            }
            else
            {
                appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
            }
        }
    }
    
}
