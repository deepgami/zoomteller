//
//  HomeVC.swift
//  ZoomTeller
//
//  Created by ZERONES on 18/03/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import NetCorePush

class HomeVC: UIViewController {
    
    @IBOutlet weak var btnLogout: UIButton!
    var previousView : UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Enter Home", payload: nil, block: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    //MARK: - Button Click Events
    
    @IBAction func btnLogoutClicked(_ sender: Any)
    {
        self.sideMenuController?.showLeftViewAnimated()
        
       // self.logoutPopup()
    }
    
    // MARK: - Custom Events
    
    func logoutPopup()  {
        
        let alert = UIAlertController(title: "Are you sure you want to logout?", message: "", preferredStyle: .alert)
        
        let CANCEL = UIAlertAction(title: "Cancel", style: .default) { (NO) in
        }
        
        let LOGOUT = UIAlertAction(title: "Logout", style: .default) { (YES) in
            self.sideMenuController?.hideLeftViewAnimated()
            
            nsuserDefault.setIsLoggin(isLoggin: false)
            nsuserDefault.removeUserDetails()
            appDelegate.checkLoginStatus()

        }
        
        alert.addAction(CANCEL)
        alert.addAction(LOGOUT)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
