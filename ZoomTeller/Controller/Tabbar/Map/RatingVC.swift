//
//  RatingVC.swift
//  ZoomTeller
//
//  Created by Deep Zerones on 5/9/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import FloatRatingView
import NetCorePush

class RatingVC: UIViewController {

    @IBOutlet weak var viewRating: FloatRatingView!
    @IBOutlet weak var btnSubmit: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnSubmit.isHidden = true
        self.viewRating.delegate = self

        self.viewRating.contentMode = UIView.ContentMode.scaleAspectFit
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        payloadDict["user_name"] = nsuserDefault.getUserDetails().name ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Rating Activity Open", payload: payloadDict, block: nil)
    }
    
    @IBAction func btnSubmitClicked(_ sender: Any)
    {

        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        payloadDict["user_name"] = nsuserDefault.getUserDetails().name ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Rating Submit Button Click", payload: payloadDict, block: nil)
        
        print("hi")
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is UITabBarController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RatingVC: FloatRatingViewDelegate {
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        print("hi")
        self.btnSubmit.isHidden = false

    }
    
    
   
    
}
