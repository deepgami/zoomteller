//
//  ATMImageVC.swift
//  ZoomTeller
//
//  Created by Deep Zerones on 5/20/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit

class ATMImageVC: UIViewController {

    @IBOutlet weak var collectionForATMImage: UICollectionView!

    var currentPage: Int = 0

    var strATMImage : String = ""
    var imageVieww : UIImageView!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func onClickBack(_ sender: Any) {
     
        self.navigationController?.popViewController(animated: false)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ATMImageVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return 10
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
          let cell = self.collectionForATMImage.dequeueReusableCell(withReuseIdentifier: "ATMImageCell", for: indexPath) as! ATMImageCell
        

        cell.imgvForATM.af_setImage(withURL: URL.init(string: strATMImage )!)
        return cell
        
        }
        
    
    

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       
    }
}


