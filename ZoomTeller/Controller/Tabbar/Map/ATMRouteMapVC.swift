//
//  ATMRouteMapVC.swift
//  ZoomTeller
//
//  Created by Deep Zerones on 5/8/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import AVFoundation
import Polyline
import Alamofire
import NetCorePush

class ATMRouteMapVC: UIViewController , GMSMapViewDelegate{
    
    
    @IBOutlet var atmMapRoute: GMSMapView!
    var poly = GMSPolyline()
    let currentMarker = GMSMarker()
    
    @IBOutlet weak var lblSourceLocation: UILabel!
    @IBOutlet weak var lblDestinationLocation: UILabel!
    @IBOutlet weak var lblForTimeAndDistance: UILabel!
    @IBOutlet weak var lblForTimeToReach: UILabel!
    
    @IBOutlet weak var btnStartExit: UIButton!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var imgvForATM: UIImageView!

    var strSourceAddress : String = ""
    var strDestinationAddress : String = ""
    var timer : Timer?

    var strAATMLati : String = ""
    var strAATMLongi : String = ""
    var strATMID : String = ""
    var strSeconds : String = ""
    var strATMType : String = ""
    var strQueueType : String = ""
    var strATMNumberFromLeft : String = ""

    var strATMTypeee: String = ""
    var strDurationInSec : String = ""
    var strTimeToReach : String = ""
    var strDistanceInKM : String = ""
    var isNavigationStart : Bool = false
    var isDefaultZoom : Bool = true
    var strATMImage : String = ""

    var is500Speak : Bool = false
    var is200Speak : Bool = false
    var is0Speak : Bool = false
    
    
    var markers = [GMSMarker]()
    
    @IBOutlet weak var lblTitleForMarker: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        atmMapRoute.delegate = self
        self.getPolylineRoute(from: CLLocationCoordinate2D(latitude: Double(USERDEFAULT.value(forKey: "latitude") as! String)!, longitude: Double(USERDEFAULT.value(forKey: "longitude") as! String)!), to: CLLocationCoordinate2D(latitude: Double(strAATMLati)!, longitude: Double(strAATMLongi)!))
        
        self.lblSourceLocation.text = strSourceAddress
        self.lblDestinationLocation.text = strDestinationAddress
        
        self.imgvForATM.af_setImage(withURL: URL.init(string: strATMImage )!)
        
        if(Int(self.strATMNumberFromLeft) == 0)
        {
            self.lblForTimeToReach.text = "1st ATM from left to right"
        }
        else if(Int(self.strATMNumberFromLeft) == 1)
        {
            self.lblForTimeToReach.text = "2nd ATM from left to right"
        }
        else if(Int(self.strATMNumberFromLeft) == 2)
        {
            self.lblForTimeToReach.text = "3rd ATM from left to right"
        }
        else if(Int(self.strATMNumberFromLeft) == 3)
        {
            self.lblForTimeToReach.text = "4th ATM from left to right"
        }
        else if(Int(self.strATMNumberFromLeft) == 4)
        {
            self.lblForTimeToReach.text = "5th ATM from left to right"
        }
        else if(Int(self.strATMNumberFromLeft) == 5)
        {
            self.lblForTimeToReach.text = "6th ATM from left to right"
        }
        else if(Int(self.strATMNumberFromLeft) == 6)
        {
            self.lblForTimeToReach.text = "7th ATM from left to right"
        }
        
        putATMMarkersOnMap()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Direction Activity Open", payload: payloadDict, block: nil)
        
    }
    
    // MARK: - IBAction method

    @IBAction func btnATMImageClicked(_ sender: Any)
    {
        self.performSegue(withIdentifier: "segueToATMImage", sender: self)

    }
    
    @IBAction func btnRecenterClicked(_ sender: Any)
    {
        isDefaultZoom = true
        
        
        let locSource = CLLocation(latitude: Double(USERDEFAULT.value(forKey: "latitude") as! String)!, longitude: Double(USERDEFAULT.value(forKey: "longitude") as! String)!)
        
        let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: locSource.coordinate.latitude, longitude: locSource.coordinate.longitude), zoom: 16.0)
        
        
        self.atmMapRoute.camera = camera
        
    }
    
    @IBAction func btnStartClicked(_ sender: Any)
    {
        
        isDefaultZoom = true
        
        if isNavigationStart
        {
            isNavigationStart = false
            stopTimer()
            if appDelegate.isSkipUser() != nil {
                self.historyWebService()
            }
            self.direction(track: "exit")
            self.performSegue(withIdentifier: "segueToRating", sender: self)
        }
        else
        {
            isNavigationStart = true
            self.btnStartExit.setImage(UIImage(named: "exit"), for: .normal)
            
            let nowDate = Date()
            let calendar = Calendar.current
            let dateToReach = calendar.date(byAdding: .second, value: Int(self.strSeconds)!, to: nowDate)
            print(dateToReach!)
            
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "h:mm a"
            
            self.lblForTimeToReach.text = self.strDistanceInKM + " • " + dateFormatterGet.string(from: dateToReach!)
            
            self.lblForTimeAndDistance.text = self.strDurationInSec
            
            
            let string = "A t m is " + self.strDistanceInKM + "away . You should be there in " + self.strDurationInSec
            let utterance = AVSpeechUtterance(string: string)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)
            
            ///////////
            ///////////
            ///////////
            
            atmMapRoute.isMyLocationEnabled = true
            
            self.getPolylineRoute(from: CLLocationCoordinate2D(latitude: Double(USERDEFAULT.value(forKey: "latitude") as! String)!, longitude: Double(USERDEFAULT.value(forKey: "longitude") as! String)!), to: CLLocationCoordinate2D(latitude: Double(strAATMLati)!, longitude: Double(strAATMLongi)!))
            
            let loc = CLLocation(latitude: Double(USERDEFAULT.value(forKey: "latitude") as! String)!, longitude: Double(USERDEFAULT.value(forKey: "longitude") as! String)!)
            
            
            let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude), zoom: 16.0)
            
            self.atmMapRoute.camera = camera
            
            startTimer()
            self.direction(track: "start")
            
            
        }
        
    }
    
    
    func direction(track : String)  {

        let currentDT = Date()
        let dateF = DateFormatter()
        dateF.dateFormat = "hh:mm a"
        let time = dateF.string(from: currentDT)
        dateF.dateFormat = "dd-MM-yyyy"
        let date = dateF.string(from: currentDT)
        
        let payloadDict = NSMutableDictionary()
        payloadDict["user_email"] = nsuserDefault.getUserDetails().email ?? ""
        payloadDict["user_name"]  = nsuserDefault.getUserDetails().name ?? ""
        payloadDict["atm_aaddress"]  = self.strDestinationAddress
        payloadDict["ATM"]  = self.strATMTypeee
        payloadDict["bank_code"]  = self.strATMType
        payloadDict["time"]  = time
        payloadDict["date"]  = date
        payloadDict["distance"]  = self.strDistanceInKM
        payloadDict["distance_time"]  = self.strDurationInSec
        payloadDict["destination_latitude"] = self.strAATMLati
        payloadDict["destination_longitude"] = self.strAATMLongi
        payloadDict["user_latitude"] = USERDEFAULT.value(forKey: "latitude") as? String ?? ""
        payloadDict["user_longitude"] = USERDEFAULT.value(forKey: "longitude") as? String ?? ""
        
        if "start" == track {
            NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Start Direction", payload: payloadDict, block: nil)
        }else if "exit" == track {
            NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Exit Direction", payload: payloadDict, block: nil)
        }
        
    }
    
    @IBAction func btnBackClicked(_ sender: Any)
    {
        stopTimer()
        self.navigationController?.popViewController(animated: false)
    }
    
    // MARK: - Custom method
    
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        
        isDefaultZoom = false
    }
    
    
    
    @objc func updateMapWithRoute() {
        
        self.getRouteTimeAndDistance(from: CLLocationCoordinate2D(latitude: Double(USERDEFAULT.value(forKey: "latitude") as! String)!, longitude: Double(USERDEFAULT.value(forKey: "longitude") as! String)!), to: CLLocationCoordinate2D(latitude: Double(strAATMLati)!, longitude: Double(strAATMLongi)!))
        
        let locSource = CLLocation(latitude: Double(USERDEFAULT.value(forKey: "latitude") as! String)!, longitude: Double(USERDEFAULT.value(forKey: "longitude") as! String)!)
        
        
        let head = appDelegate.locationManager.location?.course ?? 0
        
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.5)
        currentMarker.rotation = head
        currentMarker.position = CLLocationCoordinate2D(latitude: locSource.coordinate.latitude, longitude: locSource.coordinate.longitude)
        CATransaction.commit()
        
        
        
        let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: locSource.coordinate.latitude, longitude: locSource.coordinate.longitude), zoom: 16.0)
        
        if(self.isDefaultZoom)
            
        {
            self.atmMapRoute.camera = camera
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func putATMMarkersOnMap() {
        
        
        let locDesti = CLLocation(latitude: Double(strAATMLati)!, longitude: Double(strAATMLongi)!)
        
        
        let locSource = CLLocation(latitude: Double(USERDEFAULT.value(forKey: "latitude") as! String)!, longitude: Double(USERDEFAULT.value(forKey: "longitude") as! String)!)
        
        ///////////////////
        ///////////////////
        ///////////////////
        ///////////////////
        
        let marker = GMSMarker()
        
        marker.position = CLLocationCoordinate2D(latitude: locDesti.coordinate.latitude, longitude: locDesti.coordinate.longitude)
        
        let myView = UIView(frame: CGRect(x: 0, y: 0, width:100, height: 50))
        myView.backgroundColor = .clear
        
        let myImageView = UIImageView(frame: CGRect(x: 0, y: 0, width:100, height: 50))
        myImageView.image = UIImage(named: "infowindow_purple")
        
        
        let atmIcon = UIImageView(frame: CGRect(x: 17, y: 10, width:15, height: 15))
        atmIcon.image = UIImage(named: "atmIcon_white")
        
        let label = UILabel(frame: CGRect(x: 35, y: 8, width: 50, height: 20))
        label.textAlignment = .center
        label.textColor = .white
        label.font = self.lblTitleForMarker.font
        label.text = strATMType.uppercased()
        
        
        let labelQueue = UILabel(frame: CGRect(x: 20, y: 27, width: 40, height: 15))
        labelQueue.textAlignment = .right
        labelQueue.textColor = .white
        labelQueue.font = UIFont(name: "Arial", size: 10)
        labelQueue.text = "Queue : "
        
        let queueIcon = UIImageView(frame: CGRect(x: 65, y: 30, width:10, height: 10))
        
        
        if(self.strQueueType  == "green")
        {
            queueIcon.image = UIImage(named: "green")!
        }
        else if(self.strQueueType == "red")
        {
            queueIcon.image = UIImage(named: "red")!
        }
        else
        {
            queueIcon.image = UIImage(named: "orange")!
        }
        
        
        
        myView.addSubview(myImageView)
        myView.addSubview(atmIcon)
        myView.addSubview(label)
        myView.addSubview(labelQueue)
        myView.addSubview(queueIcon)
        
        marker.iconView = myView
        marker.map = self.atmMapRoute
        
        
        ///////////////////
        ///////////////////
        ///////////////////
        ///////////////////
        
        
        
        let markerImage = UIImage(named: "myself")!.withRenderingMode(.alwaysOriginal)
        
        
        let markerView = UIImageView(frame: CGRect(x: -30, y: -25, width:59, height: 56))
        //markerView.backgroundColor = .black
        markerView.image = markerImage
        currentMarker.position = CLLocationCoordinate2D(latitude: locSource.coordinate.latitude, longitude: locSource.coordinate.longitude)
        
        currentMarker.iconView = markerView
        
        currentMarker.map = self.atmMapRoute
        markers.append(currentMarker)
        
        markers.append(marker)
        
        if(isNavigationStart)
        {
            let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: locSource.coordinate.latitude, longitude: locSource.coordinate.longitude), zoom: 16.0)
            
            self.atmMapRoute.camera = camera
            
        }
        else
        {
            var bounds = GMSCoordinateBounds()
            for marker in markers
            {
                bounds = bounds.includingCoordinate(marker.position)
            }
            let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
            self.atmMapRoute.animate(with: update)
        }
    }
    
    //MARK: - Timer settlement methods
    //MARK: -
    
    func startTimer () {
        
        if self.timer == nil {
            self.timer =  Timer.scheduledTimer(
                timeInterval: TimeInterval(3),
                target      : self,
                selector    : #selector(self.updateMapWithRoute),
                userInfo    : nil,
                repeats     : true)
        }
    }
    
    func stopTimer() {
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    //MARK: - Web Service
    
    func historyWebService() {
        
        let lat = USERDEFAULT.value(forKey: "latitude") as! String
        let long = USERDEFAULT.value(forKey: "longitude") as! String
        
        let url = BASE_URL + API_HISTORY
        
        let param = ["user_id" : nsuserDefault.getUserDetails().userId,
                     "atm_id" : self.strATMID,
                     "km" : self.strDistanceInKM,
                     "source_lat" : lat,
                     "source_long" : long,
                     "destination_lat" : self.strAATMLati,
                     "destination_long" : self.strAATMLongi,
                     "time" : self.strDurationInSec,
        ] as! [String:String]
        
        print(param)
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, error) in
            if error == nil {
                print(response ?? NSDictionary())
            }
        }
        
    }
    
    
    //MARK: - Draw Path
    
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        
        let origin = "\(source.latitude),\(source.longitude)"
        let dest = "\(destination.latitude),\(destination.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(dest)&mode=walking&key=\(appDelegate.googleApiKey)"
        
        Alamofire.request(url).responseJSON { response in
            
            do
            {
                if let json : [String:Any] = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String: Any]
                {
                    
                    guard let routes = json["routes"] as? NSArray else {
                        
                        DispatchQueue.main.async {
                            print("Error occurs , Didn't get Routes")
                        }
                        return
                    }
                    
                    for route in routes
                    {
                        
                        //
                        
                        let overview_polyline = route as? NSDictionary
                        
                        let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                        let points = dictPolyline?.object(forKey: "points") as? String
                        
                        let path = GMSPath.init(fromEncodedPath: points!)
                        let distanceInMeters = path?.length(of: .geodesic)
                        
                        print(distanceInMeters!)
                        
                        //self.poly.map = nil
                        
                        let s =   String(format: "%.1f", Float(distanceInMeters!)/1000)
                        self.strDistanceInKM =  s + " km"
                        
                        
                        self.poly = GMSPolyline(path: path)
                        self.poly.strokeColor = map_route_color
                        self.poly.strokeWidth = 8.0
                        self.poly.map = self.atmMapRoute
                        
                        
                        if let legs = routes.value(forKey: "legs") as? NSArray
                        {
                            if let duration = legs.value(forKey: "duration") as? NSArray
                            {
                                for i in 0..<duration.count
                                {
                                    let timeDuration = duration[i] as! NSArray
                                    
                                    if let time = timeDuration.value(forKey: "value") as? NSArray
                                    {
                                        print(time[0])
                                        self.strSeconds = String(format: "%@", time[0] as! CVarArg)
                                        self.strDurationInSec = String(format: "%@", time[0] as! CVarArg)
                                        
                                        let (h,m,s) = self.secondsToHoursMinutesSeconds(seconds: Int(self.strDurationInSec)!)
                                        print(s)
                                        
                                        if(h>0)
                                        {
                                            self.strDurationInSec = String(h) + " hr " + String(m) + " min"
                                        }
                                        else
                                        {
                                            self.strDurationInSec =  String(m) + " min "  + String(s) + " sec"
                                        }
                                        
                                        if(self.isNavigationStart)
                                        {
                                            let nowDate = Date()
                                            let calendar = Calendar.current
                                            let dateToReach = calendar.date(byAdding: .second, value: Int(self.strSeconds)!, to: nowDate)
                                            print(dateToReach!)
                                            
                                            
                                            let dateFormatterGet = DateFormatter()
                                            dateFormatterGet.dateFormat = "h:mm a"
                                            
                                            
                                            
                                            self.lblForTimeAndDistance.text = self.strDurationInSec
                                            self.lblForTimeToReach.text = self.strDistanceInKM + " • " + dateFormatterGet.string(from: dateToReach!)
                                            
                                            if(Float(distanceInMeters!)>400 && Float(distanceInMeters!)<500)
                                            {
                                                
                                                if(!self.is500Speak)
                                                {
                                                    print("speck 500")
                                                    self.is500Speak = true
                                                    let string = "A t m is 500 meters away . You should be there in " + self.strDurationInSec
                                                    let utterance = AVSpeechUtterance(string: string)
                                                    utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
                                                    let synth = AVSpeechSynthesizer()
                                                    synth.speak(utterance)
                                                }
                                            }
                                            else  if(Float(distanceInMeters!)>100 && Float(distanceInMeters!)<150)
                                            {
                                                if(!self.is200Speak)
                                                {
                                                    print("speck 100")
                                                    self.is200Speak = true
                                                    let string = "A t m is 200 meters away . You should be there in " + self.strDurationInSec
                                                    let utterance = AVSpeechUtterance(string: string)
                                                    utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
                                                    let synth = AVSpeechSynthesizer()
                                                    synth.speak(utterance)
                                                }
                                            }
                                            else  if(Float(distanceInMeters!)>0 && Float(distanceInMeters!)<20)
                                            {
                                                if(!self.is0Speak)
                                                {
                                                    print("speck 0")
                                                    self.is0Speak = true
                                                    let string = "You have reached at your A T M"
                                                    let utterance = AVSpeechUtterance(string: string)
                                                    utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
                                                    let synth = AVSpeechSynthesizer()
                                                    synth.speak(utterance)
                                                    
                                                    self.isNavigationStart = false
                                                    self.stopTimer()
                                                    self.performSegue(withIdentifier: "segueToRating", sender: self)
                                                }
                                                
                                            }
                                        }
                                        else
                                        {
                                            self.lblForTimeAndDistance.text = self.strDurationInSec + " (" + self.strDistanceInKM + ")"
                                        }
                                    }
                                }
                            }
                        }
                        
                        
                    }
                }
            }
            catch
            {
                print("Error in JSONSerialization")
            }
        }
    }
    
    
    
    func getRouteTimeAndDistance(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        
        
        
        let origin = "\(source.latitude),\(source.longitude)"
        let dest = "\(destination.latitude),\(destination.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(dest)&mode=walking&key=\(appDelegate.googleApiKey)"
        
        Alamofire.request(url).responseJSON { response in
            
            do
            {
                if let json : [String:Any] = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String: Any]
                {
                    
                    guard let routes = json["routes"] as? NSArray else {
                        
                        DispatchQueue.main.async {
                            print("Error occurs , Didn't get Routes")
                        }
                        return
                    }
                    
                    for route in routes
                    {
                        
                        //
                        
                        let overview_polyline = route as? NSDictionary
                        
                        let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                        let points = dictPolyline?.object(forKey: "points") as? String
                        
                        let path = GMSPath.init(fromEncodedPath: points!)
                        let distanceInMeters = path?.length(of: .geodesic)
                        
                        print(distanceInMeters!)
                        
                        //self.poly.map = nil
                        
                        let s =   String(format: "%.1f", Float(distanceInMeters!)/1000)
                        self.strDistanceInKM =  s + " km"
                        
                        
                        
                        
                        if let legs = routes.value(forKey: "legs") as? NSArray
                        {
                            if let duration = legs.value(forKey: "duration") as? NSArray
                            {
                                for i in 0..<duration.count
                                {
                                    let timeDuration = duration[i] as! NSArray
                                    
                                    if let time = timeDuration.value(forKey: "value") as? NSArray
                                    {
                                        print(time[0])
                                        self.strSeconds = String(format: "%@", time[0] as! CVarArg)
                                        self.strDurationInSec = String(format: "%@", time[0] as! CVarArg)
                                        
                                        let (h,m,s) = self.secondsToHoursMinutesSeconds(seconds: Int(self.strDurationInSec)!)
                                        print(s)
                                        
                                        if(h>0)
                                        {
                                            self.strDurationInSec = String(h) + " hr " + String(m) + " min"
                                        }
                                        else
                                        {
                                            self.strDurationInSec =  String(m) + " min "  + String(s) + " sec"
                                        }
                                        
                                        if(self.isNavigationStart)
                                        {
                                            let nowDate = Date()
                                            let calendar = Calendar.current
                                            let dateToReach = calendar.date(byAdding: .second, value: Int(self.strSeconds)!, to: nowDate)
                                            print(dateToReach!)
                                            
                                            
                                            let dateFormatterGet = DateFormatter()
                                            dateFormatterGet.dateFormat = "h:mm a"
                                            
                                            
                                            
                                            self.lblForTimeAndDistance.text = self.strDurationInSec
                                            self.lblForTimeToReach.text = self.strDistanceInKM + " • " + dateFormatterGet.string(from: dateToReach!)
                                            
                                            if(Float(distanceInMeters!)>400 && Float(distanceInMeters!)<500)
                                            {
                                                if(!self.is500Speak)
                                                {
                                                    print("speck 500")
                                                    self.is500Speak = true
                                                    let string = "A t m is around 500 meters away . You will there at" + dateFormatterGet.string(from: dateToReach!)
                                                    let utterance = AVSpeechUtterance(string: string)
                                                    utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
                                                    let synth = AVSpeechSynthesizer()
                                                    synth.speak(utterance)
                                                }
                                            }
                                            else  if(Float(distanceInMeters!)>100 && Float(distanceInMeters!)<150)
                                            {
                                                if(!self.is200Speak)
                                                {
                                                    print("speck 500")
                                                    self.is200Speak = true
                                                    let string = "A t m is around 100 meters away . You will there at" + dateFormatterGet.string(from: dateToReach!)
                                                    let utterance = AVSpeechUtterance(string: string)
                                                    utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
                                                    let synth = AVSpeechSynthesizer()
                                                    synth.speak(utterance)
                                                }
                                            }
                                            else  if(Float(distanceInMeters!)>0 && Float(distanceInMeters!)<20)
                                            {
                                                if(!self.is0Speak)
                                                {
                                                    print("speck 500")
                                                    self.is500Speak = true
                                                    let string = "You have reached at your ATM"
                                                    let utterance = AVSpeechUtterance(string: string)
                                                    utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
                                                    let synth = AVSpeechSynthesizer()
                                                    synth.speak(utterance)
                                                }
                                                self.isNavigationStart = false
                                                self.stopTimer()
                                                self.performSegue(withIdentifier: "segueToRating", sender: self)
                                                
                                            }
                                        }
                                        else
                                        {
                                            self.lblForTimeAndDistance.text = self.strDurationInSec + " (" + self.strDistanceInKM + ")"
                                        }
                                    }
                                }
                            }
                        }
                        
                        
                    }
                }
            }
            catch
            {
                print("Error in JSONSerialization")
            }
        }
    }
    
    
     // MARK: - Navigation
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueToATMImage"
        {
            let dest = segue.destination as! ATMImageVC
            dest.strATMImage = strATMImage
        }
     }
    
}
