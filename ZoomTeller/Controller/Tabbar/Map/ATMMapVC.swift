//
//  ATMMapVC.swift
//  ZoomTeller
//
//  Created by ZerOnes on 26/04/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Polyline
import NetCorePush



class ATMMapVC: UIViewController , GMSMapViewDelegate{
    
    @IBOutlet weak var imgTabBrowse: UIImageView!
    @IBOutlet weak var imgTabMap: UIImageView!
    @IBOutlet weak var imgTabProfile: UIImageView!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCurrentAddress: UILabel!
    @IBOutlet weak var viewATMAnimation: UIView!
    @IBOutlet weak var viewForCurrentLocation: UIView!
    @IBOutlet weak var viewForQuickSuggestion: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var AtlCollectionView: UICollectionView!
    
    @IBOutlet weak var imgBankLogo: UIImageView!
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblTitleForMarker: UILabel!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnGetDirection: UIButton!
    
    @IBOutlet weak var btnPreviousForBank: UIButton!
    @IBOutlet weak var btnNextForBank: UIButton!
    
    
    @IBOutlet var atmDisplayMapView: GMSMapView!
    @IBOutlet weak var imgvNoATMFound: UIImageView!
    
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var sliderForDistance: CustomUISlider!
    
    @IBOutlet weak var collectionForBankFilter: UICollectionView!
    @IBOutlet weak var collectionForMinimumLimit: UICollectionView!
    @IBOutlet weak var collectionForMaximumLimit: UICollectionView!
    @IBOutlet weak var viewForFilter: UIView!
    @IBOutlet weak var viewLocationEnabel: UIView!
    @IBOutlet weak var viewFilterWithOutSubscription: UIView!
    @IBOutlet weak var viewATMWithOutSubscription: UIView!
    
    var arrForMinimumLimit : NSMutableArray = NSMutableArray()
    var arrForMaximumLimit : NSMutableArray = NSMutableArray()
    
    let tripMarker = GMSMarker()
    var currentPage: Int = 0
    var currentPageForBank: Int = 0
    
    var arrForBank : NSMutableArray = NSMutableArray()
    var arrForAtm : NSMutableArray = NSMutableArray()
    var arrAtmFound = [Any]()
    
    var strDestinationAddress : String = ""
    var strAATMLati : String = ""
    var strAATMLongi : String = ""
    var strATMID : String = ""
    var strATMType : String = ""
    var strATMImage : String = ""
    var strQueueType : String = ""
    var strATMTypeee : String = ""
    
    var markers = [GMSMarker]()
    
    var timerCheckLocation : Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        parentATMMapVC = self
        
        
        //if !CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .denied  {
        if appDelegate.locationPermissionIsValid() == true {
            viewLocationEnabel.isHidden = false
        }else {
            viewLocationEnabel.isHidden = true
        }
        
        imgLocation.image = imgLocation.image?.withRenderingMode(.alwaysTemplate)
        imgLocation.tintColor = .gray
        
        atmDisplayMapView.delegate = self
        self.lblTitle.text = ""
        self.getBankList()
        if appDelegate.isSkipUser() != nil {
            
        }else {
            
        }
        
        if appDelegate.isSkipUser() != nil && appDelegate.isRestorePurchasedPlan() == true  && appDelegate.isUserLoginRestoreSubcription() == false {
            let endDate = appDelegate.expiryPlanDateSkipUser()
            let dateFormate = DateFormatter()
            dateFormate.dateFormat = "yyyy-MM-dd HH:mm:ss z"
            let dDate = dateFormate.date(from: endDate) ?? Date()
            dateFormate.dateFormat = "yyyy-MM-dd"
            let dString = dateFormate.string(from: dDate)
            
            if appDelegate.restoreSubscriptionPlan() == "com.zoomteller.fulltwo"  {
                self.subscriptionWebService(planId: "7", planName: "Two Months", transactionId: "", endDate:dString)
            }else if  appDelegate.restoreSubscriptionPlan() == "com.zoomteller.fullfour" {
                self.subscriptionWebService(planId: "8", planName: "Four Months", transactionId: "", endDate:dString)
            }else if appDelegate.restoreSubscriptionPlan() == "com.zoomteller.fullsix" {
                self.subscriptionWebService(planId: "5", planName: "Six Months", transactionId: "", endDate:dString)
            }else if appDelegate.restoreSubscriptionPlan() == "com.zoomteller.fulloneyear" {
                self.subscriptionWebService(planId: "6", planName: "One Year", transactionId: "", endDate:dString)
            }
        }

        //if CLLocationManager.locationServicesEnabled() {
        
        //}
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Nearest Atm On Map", payload: payloadDict, block: nil)
        
        DispatchQueue.main.async {
            if appDelegate.isSkipUser() != nil {
                if activePlanDetails(dic: (nsuserDefault.getUserDetails().activePlan)).plan_type == "Paid" {
                    //appDelegate.receiptValidation()
                }
            }
        }
        
        parentATMMapVC = self
        
        if appDelegate.isSkipUser() != nil {
            getUserDetails()
        }else {
            self.checkSubscriptionDetails()
        }
        
        viewWill()
        
        
        //if !CLLocationManager.locationServicesEnabled() {
        
        //appDelegate.getInitialLocation()
        
        //}else {
        //viewWill()
        //}
        
    }
    
    @objc func viewWill() {
        
        self.viewATMAnimation.isHidden = true
        self.viewForQuickSuggestion.isHidden = true
        
        arrForMinimumLimit = ["500","1000"]
        arrForMaximumLimit = ["10000","20000","30000","40000","50000"]
        
        self.viewForFilter.isHidden = true
        
        self.lblTitle.text = ""
        
        //if (!CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .denied) == false {
        if appDelegate.locationPermissionIsValid() == true {
            
            appDelegate.startLocationManager()
            
            if appDelegate.getLocation() != nil {
                
                if timerCheckLocation != nil {
                    timerCheckLocation.invalidate()
                }
                
                viewLocationEnabel.isHidden = true
                getATMList()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    
                    self.getAddress() { (returnAddress) in
                        
                        print("\(returnAddress)")
                        self.lblCurrentAddress.text = returnAddress
                    }
                })
            }else {
                timerCheckLocation = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(viewWill), userInfo: nil, repeats: false)
            }
            
        }else {
            
            viewLocationEnabel.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.viewATMAnimation.transform = CGAffineTransform.identity
        
        
    }
    // MARK: - IBAction method
    
    @IBAction func onClickLocationEnabel(_ sender: Any) {
        appDelegate.openAppLocationSetting()
    }
    
    //segueToSubscription
    
    @IBAction func onClickSubscriptionVC(_ sender: Any) {
        self.performSegue(withIdentifier: "segueToSubscription", sender: self)
    }
    
    @IBAction func onClickMenu(_ sender: Any) {
        self.sideMenuController?.showLeftViewAnimated()
    }
    
    @IBAction func btnSaveFilteredClicked(_ sender: Any) {
        
        
        self.viewForFilter.isHidden = true
        
        let top = CGAffineTransform(translationX: 0, y: -self.view.frame.height)
        let bottom = CGAffineTransform(translationX: 0, y:0)
        
        
        UIView.animate(withDuration: 0, animations: {
            self.viewForFilter.transform = bottom
            
        }) { (finished) in
            UIView.animate(withDuration: 0.5, animations: {
                self.viewForFilter.transform = top
                self.viewForFilter.isHidden = false
                
            })
        }
        //if (!CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .denied) == false{
        if appDelegate.locationPermissionIsValid() == true {
            viewLocationEnabel.isHidden = true
            self.getATMList()
        }else {
            viewLocationEnabel.isHidden = false
        }
    }
    
    
    @IBAction func onClickHideFilter(_ sender: Any) {
        
        self.viewForFilter.isHidden = true
        
        let top = CGAffineTransform(translationX: 0, y: -self.view.frame.height)
        let bottom = CGAffineTransform(translationX: 0, y:0)
        
        UIView.animate(withDuration: 0, animations: {
            self.viewForFilter.transform = bottom
            
        }) { (finished) in
            UIView.animate(withDuration: 0.5, animations: {
                self.viewForFilter.transform = top
                self.viewForFilter.isHidden = false
                
            })
        }
    }
    
    @IBAction func onClickFilter(_ sender: Any) {
            self.setFilterData()
            
            self.btnSave.isEnabled = false
            self.btnSave.setTitleColor(filter_pink_disable, for: .normal)
            
            let top = CGAffineTransform(translationX: 0, y: -self.view.frame.height)
            let bottom = CGAffineTransform(translationX: 0, y:0)
            
            
            UIView.animate(withDuration: 0, animations: {
                self.viewForFilter.transform = top
                
            }) { (finished) in
                UIView.animate(withDuration: 0.5, animations: {
                    self.viewForFilter.transform = bottom
                    self.viewForFilter.isHidden = false
                    
                })
            }
    }
    
    
    @IBAction func sliderValueChanged(sender: CustomUISlider) {
        
        let currentValue = Int(sender.value)
        print(currentValue)
        appDelegate.strFilterDistance = String(currentValue)
        self.btnSave.isEnabled = true
        self.btnSave.setTitleColor(filter_pink, for: .normal)
    }
    
    @objc func btnMinimumAmountClicked(_ sender : UIButton)
    {
        self.btnSave.isEnabled = true
        self.btnSave.setTitleColor(filter_pink, for: .normal)
        
        if(appDelegate.strFilterMinimumDispense == self.arrForMinimumLimit[sender.tag] as! String)
        {
            appDelegate.strFilterMinimumDispense = "0"
        }
        else
        {
            appDelegate.strFilterMinimumDispense = self.arrForMinimumLimit[sender.tag] as! String
        }
        self.collectionForMinimumLimit.reloadData()
    }
    @objc func btnmaximumAmountClicked(_ sender : UIButton)
    {
        self.btnSave.isEnabled = true
        self.btnSave.setTitleColor(filter_pink, for: .normal)
        
        if(appDelegate.strFilterMaximumDispense == self.arrForMaximumLimit[sender.tag] as! String)
        {
            appDelegate.strFilterMaximumDispense = "0"
        }
        else
        {
            appDelegate.strFilterMaximumDispense = self.arrForMaximumLimit[sender.tag] as! String
        }
        self.collectionForMaximumLimit.reloadData()
        
    }
    
    @IBAction func onClickPreviousForBank(_ sender: Any) {
        
        if(currentPageForBank > 0)
        {
            currentPageForBank -= 1
            setBankDataOnNextPrev(indexF: currentPageForBank)
        }
        
        if(currentPageForBank == 0)
        {
            self.btnPreviousForBank.backgroundColor = .clear
            self.btnPreviousForBank.layer.cornerRadius = 5
            self.btnPreviousForBank.layer.borderWidth = 1
            self.btnPreviousForBank.layer.borderColor = UIColor.white.withAlphaComponent(0.7).cgColor
            self.btnPreviousForBank.setTitleColor(UIColor.white.withAlphaComponent(0.7), for: .normal)
        }
        else
        {
            self.btnPreviousForBank.backgroundColor = UIColor.rgb(246, green: 246, blue: 246)
            self.btnPreviousForBank.layer.cornerRadius = 5
            self.btnPreviousForBank.layer.borderWidth = 1
            self.btnPreviousForBank.layer.borderColor = UIColor.rgb(190, green: 190, blue: 190).cgColor
            self.btnPreviousForBank.setTitleColor(UIColor.rgb(95, green: 93, blue: 112), for: .normal)
        }
        
        if(currentPageForBank < arrForAtm.count-1)
        {
            self.btnNextForBank.backgroundColor = UIColor.rgb(246, green: 246, blue: 246)
            self.btnNextForBank.layer.cornerRadius = 5
            self.btnNextForBank.layer.borderWidth = 1
            self.btnNextForBank.layer.borderColor = UIColor.rgb(190, green: 190, blue: 190).cgColor
            self.btnNextForBank.setTitleColor(UIColor.rgb(95, green: 93, blue: 112), for: .normal)
        }
        
    }
    @IBAction func onClickNextForBank(_ sender: Any) {
        
        if(currentPageForBank < self.arrForAtm.count-1)
        {
            currentPageForBank += 1
            setBankDataOnNextPrev(indexF: currentPageForBank)
        }
        
        if(currentPageForBank < self.arrForAtm.count-1)
        {
            
            self.btnNextForBank.backgroundColor = UIColor.rgb(246, green: 246, blue: 246)
            self.btnNextForBank.layer.cornerRadius = 5
            self.btnNextForBank.layer.borderWidth = 1
            self.btnNextForBank.layer.borderColor = UIColor.rgb(190, green: 190, blue: 190).cgColor
            self.btnNextForBank.setTitleColor(UIColor.rgb(95, green: 93, blue: 112), for: .normal)
        }
        else
        {
            self.btnNextForBank.backgroundColor = .clear
            self.btnNextForBank.layer.cornerRadius = 5
            self.btnNextForBank.layer.borderWidth = 1
            self.btnNextForBank.layer.borderColor = UIColor.white.withAlphaComponent(0.7).cgColor
            self.btnNextForBank.setTitleColor(UIColor.white.withAlphaComponent(0.7), for: .normal)
            
        }
        
        
        if(currentPageForBank != 0)
        {
            self.btnPreviousForBank.backgroundColor = UIColor.rgb(246, green: 246, blue: 246)
            self.btnPreviousForBank.layer.cornerRadius = 5
            self.btnPreviousForBank.layer.borderWidth = 1
            self.btnPreviousForBank.layer.borderColor = UIColor.rgb(190, green: 190, blue: 190).cgColor
            self.btnPreviousForBank.setTitleColor(UIColor.rgb(95, green: 93, blue: 112), for: .normal)
        }
    }
    
    
    @IBAction func onClickPrevious(_ sender: Any) {
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "ATM Direction on Map prev button click", payload: payloadDict, block: nil)
        
        if(currentPage > 0)
        {
            currentPage -= 1
            AtlCollectionView.scrollToPreviousItem()
            pageControl.currentPage = currentPage
            btnNext.setImage(UIImage(named: "next"), for: .normal)
            
        }
        
        
        if(currentPage == 0)
        {
            btnPrevious.setImage(UIImage(named: "prev_end"), for: .normal)
            
        }
        else
        {
            btnPrevious.setImage(UIImage(named: "prev"), for: .normal)
            
        }
    }
    
    @IBAction func onClickNext(_ sender: Any) {
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "ATM Direction on Map next button click", payload: payloadDict, block: nil)
        
        if(currentPage < arrAtmFound.count-1)
        {
            currentPage += 1
            AtlCollectionView.scrollToNextItem()
            pageControl.currentPage = currentPage
            btnPrevious.setImage(UIImage(named: "prev"), for: .normal)
            
        }
        
        if(currentPage != arrAtmFound.count-1)
        {
            btnNext.setImage(UIImage(named: "next"), for: .normal)
        }
        else
        {
            btnNext.setImage(UIImage(named: "next_end"), for: .normal)
        }
    }
    
    
    @IBAction func btnDirectionClicked(_ sender: UIButton)
    {
        
        let currentDT = Date()
        let dateF = DateFormatter()
        dateF.dateFormat = "hh:mm a"
        let time = dateF.string(from: currentDT)
        dateF.dateFormat = "dd-MM-yyyy"
        let date = dateF.string(from: currentDT)
        
        let payloadDict = NSMutableDictionary()
        payloadDict["user_email"] = nsuserDefault.getUserDetails().email ?? ""
        payloadDict["user_name"]  = nsuserDefault.getUserDetails().name ?? ""
        payloadDict["atm_aaddress"]  = self.strDestinationAddress
        payloadDict["ATM"]  = self.strATMTypeee
        payloadDict["bank_code"]  = self.strATMType
        payloadDict["time"]  = time
        payloadDict["date"]  = date
        payloadDict["destination_latitude"] = self.strAATMLati
        payloadDict["destination_longitude"] = self.strAATMLongi
        payloadDict["user_latitude"] = USERDEFAULT.value(forKey: "latitude") as? String ?? ""
        payloadDict["user_longitude"] = USERDEFAULT.value(forKey: "longitude") as? String ?? ""
        
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "ATM Direction on Map Direction Button Click", payload: payloadDict, block: nil)
        
        self.performSegue(withIdentifier: "segueToMapRoute", sender: self)
        
    }
    @IBAction func btnCrossClicked(_ sender: Any)
    {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.viewATMAnimation.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            
        }) { (finished) in
            
            self.viewATMAnimation.isHidden = true
            self.viewForQuickSuggestion.isHidden = true
            
        }
        
        
    }
    
    @IBAction func btnTabBarClicked(_ sender: Any)
    {
        let button = sender as! UIButton
        if appDelegate.isSkipUser() != nil {
            tabBarController?.selectedIndex = button.tag
        }else {
            if button.tag == 2 {
                appDelegate.appSkipAlert(vc: self)
            }else {
                tabBarController?.selectedIndex = button.tag
            }
        }
    }
    
    // MARK: - Custom method
    
    
    func setBankDataOnNextPrev(indexF: Int)
    {
        self.viewATMAnimation.isHidden = false
        self.viewForQuickSuggestion.isHidden = false
        
        
        self.imgvNoATMFound.isHidden = true
        self.atmDisplayMapView.isHidden = false
        self.viewForCurrentLocation.isHidden = false
        
        print(indexF)
        
        self.arrAtmFound = self.arrForAtm[indexF] as! [Any]
        print(self.arrAtmFound)
        
        let dictData = self.arrAtmFound[0] as! [String:Any]
        
        self.strDestinationAddress = (dictData["atm_address"] as! String)
        self.strAATMLati = (dictData["latitude"] as! String)
        self.strAATMLongi = (dictData["longitude"] as! String)
        self.strATMID = (dictData["atm_id"] as! String)
        self.strATMType = (dictData["bank_code"] as! String)
        self.strQueueType = (dictData["queue"] as! String)
        self.strATMTypeee = (dictData["atm_type"] as! String)
        self.strATMImage = (dictData["atm_image"] as! String)
        
        //
        
        let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: Double((dictData["latitude"] as! String))!, longitude: Double((dictData["longitude"] as! String))!), zoom: 15.0)
        
        self.atmDisplayMapView.animate(to: camera)
        
        let mapInsets = UIEdgeInsets(top: 230.0, left: 0.0, bottom: 0, right: 0.0)
        
        self.atmDisplayMapView.padding = mapInsets
        //
        
        
        let urlImage = dictData["atm_image"]
        
        self.imgBankLogo.af_setImage(withURL: URL.init(string: urlImage as! String)!)
        self.lblBankName.text = (dictData["atm_type"] as! String)
        
        
        if self.arrAtmFound.count <= 1{
            self.btnNext.isHidden = true
            self.btnPrevious.isHidden = true
            self.pageControl.isHidden = true
        }
        else
        {
            self.btnNext.isHidden = false
            self.btnPrevious.isHidden = false
            self.pageControl.isHidden = false
        }
        
        
        self.pageControl.numberOfPages = self.arrAtmFound.count
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.init(red: 218/255.0, green: 217/255.0, blue: 226/255.0, alpha: 1)
        
        self.pageControl.pageIndicatorTintColor = UIColor.init(red: 218/255.0, green: 217/255.0, blue: 226/255.0, alpha: 1)
        self.pageControl.currentPageIndicatorTintColor = UIColor.init(red: 155/255.0, green: 153/255.0, blue: 169/255.0, alpha: 1)
        
        
        self.AtlCollectionView.reloadData()
    }
    
    func setFilterData(){
        
        self.sliderForDistance.value = (appDelegate.strFilterDistance as NSString).floatValue
    }
    
    func getAddress(currentAdd : @escaping ( _ returnAddress :String)->Void){
        
        if !CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .denied
        {
            
            
            let alert = UIAlertController.init(title: "Location Services Disabled!", message: "Please enable Location Based Services for better results! We promise to keep your location private", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Settings", style: .default)
            { (action) in
                if !CLLocationManager.locationServicesEnabled()
                {
                    self.openDeviceLocationSetting()
                }
                else
                {
                    self.openAppLocationSetting()
                }
            }
            
            let cancel = UIAlertAction(title: "Try again", style: .default) { (_) in
                self.getATMList()
            }
            
            alert.addAction(ok)
            alert.addAction(cancel)
            
            //self.present(alert, animated: true, completion: nil)
        }
        else
        {
            
            let geocoder = GMSGeocoder()
            let coordinate = CLLocationCoordinate2DMake(Double(USERDEFAULT.value(forKey: "latitude") as! String)!,Double(USERDEFAULT.value(forKey: "longitude") as! String)!)
            
            var currentAddress = String()
            
            geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
                if let address = response?.firstResult() {
                    let lines = address.lines! as [String]
                    
                    currentAddress = lines.joined(separator: "\n")
                    
                    currentAdd(currentAddress)
                }
            }
        }
    }
    
    func checkSubscriptionDetails() {
        if appDelegate.isSkipUser() != nil {
            
            DispatchQueue.main.async{
                let plan = nsuserDefault.getUserDetails().activePlan
                let details = activePlanDetails(dic: plan!)
                
                let endDate = nsuserDefault.getUserDetails().subScribeEndDate
                let day = endDate!.countDay(date:endDate!)
                
                
                if ( details.plan_type == "Paid" && appDelegate.subscriptionPlanActiveTime() > 0.0) || ((details.plan_type == "trial" || details.plan_type == "promo") && day > 0){
                    USERDEFAULT.set(true, forKey: IS_PLAN_SUBSCRIBE)
                    DispatchQueue.main.async {
                        self.viewATMWithOutSubscription.isHidden = true
                        self.viewFilterWithOutSubscription.isHidden = true
                        self.btnGetDirection.isEnabled = true
                    }
                }else {
                    if day > 0 {
                        USERDEFAULT.set(true, forKey: IS_PLAN_SUBSCRIBE)
                        DispatchQueue.main.async {
                            self.viewATMWithOutSubscription.isHidden = true
                            self.viewFilterWithOutSubscription.isHidden = true
                            self.btnGetDirection.isEnabled = true
                        }
                    }else {
                        DispatchQueue.main.async {
                            USERDEFAULT.set(false, forKey: IS_PLAN_SUBSCRIBE)
                            self.viewATMWithOutSubscription.isHidden = false
                            self.viewFilterWithOutSubscription.isHidden = false
                            self.btnGetDirection.isEnabled = false
                        }
                    }
                    
                    //DispatchQueue.main.async {
                    //    USERDEFAULT.set(false, forKey: IS_PLAN_SUBSCRIBE)
                    //    self.viewATMWithOutSubscription.isHidden = false
                    //    self.viewFilterWithOutSubscription.isHidden = false
                    //    self.btnGetDirection.isEnabled = false
                    //}
                }
                USERDEFAULT.synchronize()
            }
        }else {
            if appDelegate.restoreSubscriptionPlan() != nil && appDelegate.subscriptionPlanActiveTimeSkipUser() > 0.0{
                USERDEFAULT.set(true, forKey: IS_PLAN_SUBSCRIBE)
                DispatchQueue.main.async {
                    self.viewATMWithOutSubscription.isHidden = true
                    self.viewFilterWithOutSubscription.isHidden = true
                    self.btnGetDirection.isEnabled = true
                }
            }else {
                DispatchQueue.main.async {
                    USERDEFAULT.set(false, forKey: IS_PLAN_SUBSCRIBE)
                    self.viewATMWithOutSubscription.isHidden = false
                    self.viewFilterWithOutSubscription.isHidden = false
                    self.btnGetDirection.isEnabled = false
                }
            }
        }
    }
    
    // MARK: - APIs method
    
    func getUserDetails() {
        
        appDelegate.showHUD()
        
        let url = BASE_URL + API_EDIT_PROFILE
        
        let param = ["user_id":nsuserDefault.getUserDetails().userId as String]
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, status) in
            appDelegate.hideHUD()
            if status == nil
            {
                let res = response
                if res?.value(forKey: "status") as! Bool == true
                {
                    let data = res?.value(forKey: "data") as? [String:Any] ?? [String:Any]()
                    nsuserDefault.setUserDetails(logindata: LoginClass(fromDictionary: data))
                    self.checkSubscriptionDetails()
                    USERDEFAULT.set(data["today"] as? String ?? "", forKey: "todayDate")
                    USERDEFAULT.synchronize()
                }
                else
                {
                    let notAllow = "user not available in database"
                    if res?.value(forKey: "status") as! Bool == false && res?.value(forKey: "message") as! String == notAllow {
                        nsuserDefault.removeUserDetails()
                        nsuserDefault.setIsLoggin(isLoggin: false)
                        appDelegate.checkLoginStatus()
                    }else {
                        appDelegate.showToast(title: "Error", message: res!.value(forKey: "message") as! String)
                        self.checkSubscriptionDetails()
                    }
                }
            }
            else
            {
                appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
                self.checkSubscriptionDetails()
                
            }
        }
    }
    
    
    
    func getBankList() {
        
        
        let url = BASE_URL + API_GET_BANK
        let userDetail = nsuserDefault.getUserDetails()
        
        let param = ["user_id":userDetail.userId as String]
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, status) in
            
            appDelegate.hideHUD()
            
            if status == nil
            {
                let res = response
                if res?.value(forKey: "status") as! Bool == true
                {
                    let tagArray = res!["data"] as! NSArray
                    self.arrForBank = NSMutableArray(array:tagArray)
                    self.collectionForBankFilter.reloadData()
                }
                else
                {
                    appDelegate.showToast(title: "Error", message: res!.value(forKey: "message") as! String)
                }
            }
            else
            {
                appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
            }
        }
    }
    func getATMList() {
        
        
        if !CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .denied
        {
            
            let alert = UIAlertController.init(title: "Location Services Disabled!", message: "Please enable Location Based Services for better results! We promise to keep your location private", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Settings", style: .default)
            { (action) in
                if !CLLocationManager.locationServicesEnabled()
                {
                    self.openDeviceLocationSetting()
                }
                else
                {
                    self.openAppLocationSetting()
                }
            }
            
            let cancel = UIAlertAction(title: "Try again", style: .default) { (_) in
                self.getATMList()
            }
            
            alert.addAction(ok)
            alert.addAction(cancel)
            
            //self.present(alert, animated: true, completion: nil)
        }
        else
        {
            
            let url = BASE_URL + API_GET_ATM
            let userDetail = nsuserDefault.getUserDetails()
            
            let param = ["user_id":userDetail.userId as String,
                         "latitude":USERDEFAULT.value(forKey: "latitude") as! String,
                         "longitude":USERDEFAULT.value(forKey: "longitude") as! String,
                         "bank_id":appDelegate.strFilterBank,
                         "min_amount" : appDelegate.strFilterMinimumDispense,
                         "max_amount":appDelegate.strFilterMaximumDispense,
                         "km":appDelegate.strFilterDistance]
            
            
            print(param)
            
            ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, status) in
                
                appDelegate.hideHUD()
                
                if status == nil
                {
                    let res = response
                    if res?.value(forKey: "status") as! Bool == true
                    {
                        let tagArray = res!["data"] as! NSArray
                        self.arrForAtm = NSMutableArray(array:tagArray)
                        self.putATMMarkersOnMap()
                        
                        
                        if(self.arrForAtm.count>0)
                        {
                            
                            if(self.arrForAtm.count > 1)
                            {
                                self.viewForQuickSuggestion.isHidden = false
                            }
                            
                            self.viewATMAnimation.isHidden = false
                            self.viewATMAnimation.transform = CGAffineTransform.identity
                            
                            
                            self.lblTitle.text = ((res!["count_atm"] as! NSString) as String) + " ATMS FOUND"
                            self.imgvNoATMFound.isHidden = true
                            self.atmDisplayMapView.isHidden = false
                            self.viewForCurrentLocation.isHidden = false
                            
                            self.arrAtmFound = self.arrForAtm[0] as! [Any]
                            print(self.arrAtmFound)
                            
                            let dictData = self.arrAtmFound[0] as! [String:Any]
                            
                            self.strDestinationAddress = (dictData["atm_address"] as! String)
                            self.strAATMLati = (dictData["latitude"] as! String)
                            self.strAATMLongi = (dictData["longitude"] as! String)
                            self.strATMID = (dictData["atm_id"] as! String)
                            self.strATMType = (dictData["bank_code"] as! String)
                            self.strQueueType = (dictData["queue"] as! String)
                            self.strATMImage = (dictData["atm_image"] as! String)
                            self.strATMTypeee = (dictData["atm_type"] as! String)
                            
                            
                            //
                            
                            let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: Double((dictData["latitude"] as! String))!, longitude: Double((dictData["longitude"] as! String))!), zoom: 15.0)
                            
                            self.atmDisplayMapView.camera = camera
                            
                            let mapInsets = UIEdgeInsets(top: 230.0, left: 0.0, bottom: 0, right: 0.0)
                            
                            self.atmDisplayMapView.padding = mapInsets
                            //
                            
                            
                            let urlImage = dictData["atm_image"]
                            
                            self.imgBankLogo.af_setImage(withURL: URL.init(string: urlImage as! String)!)
                            self.lblBankName.text = (dictData["atm_type"] as! String)
                            
                            
                            if self.arrAtmFound.count <= 1{
                                self.btnNext.isHidden = true
                                self.btnPrevious.isHidden = true
                                self.pageControl.isHidden = true
                            }
                            else
                            {
                                self.btnNext.isHidden = false
                                self.btnPrevious.isHidden = false
                                self.pageControl.isHidden = false
                            }
                            
                            
                            self.pageControl.numberOfPages = self.arrAtmFound.count
                            self.pageControl.currentPage = 0
                            self.pageControl.tintColor = UIColor.init(red: 218/255.0, green: 217/255.0, blue: 226/255.0, alpha: 1)
                            
                            self.pageControl.pageIndicatorTintColor = UIColor.init(red: 218/255.0, green: 217/255.0, blue: 226/255.0, alpha: 1)
                            self.pageControl.currentPageIndicatorTintColor = UIColor.init(red: 155/255.0, green: 153/255.0, blue: 169/255.0, alpha: 1)
                            
                            
                            self.btnPreviousForBank.backgroundColor = .clear
                            self.btnPreviousForBank.layer.cornerRadius = 5
                            self.btnPreviousForBank.layer.borderWidth = 1
                            self.btnPreviousForBank.layer.borderColor = UIColor.white.withAlphaComponent(0.7).cgColor
                            self.btnPreviousForBank.setTitleColor(UIColor.white.withAlphaComponent(0.7), for: .normal)
                            
                            self.AtlCollectionView.reloadData()
                            
                        }
                        else
                        {
                            self.imgvNoATMFound.isHidden = false
                            self.atmDisplayMapView.isHidden = true
                            self.lblTitle.text = "0 ATMS FOUND"
                            self.viewForCurrentLocation.isHidden = true
                            self.viewATMAnimation.isHidden = true
                            self.viewForQuickSuggestion.isHidden = true
                            
                            
                        }
                        
                        
                        let payloadDict = NSMutableDictionary()
                        payloadDict["user_email"] = nsuserDefault.getUserDetails().email ?? ""
                        payloadDict["user_name"]  = nsuserDefault.getUserDetails().name ?? ""
                        payloadDict["bank_id"]  = appDelegate.strFilterBank
                        payloadDict["bank_name"]  = appDelegate.strFilterBankCode
                        payloadDict["min_amount"]  = appDelegate.strFilterMinimumDispense
                        payloadDict["max_amount"]  = appDelegate.strFilterMaximumDispense
                        payloadDict["meter"]  = appDelegate.strFilterDistance
                        payloadDict["latitude"] = USERDEFAULT.value(forKey: "latitude") as? String ?? ""
                        payloadDict["longitude"] = USERDEFAULT.value(forKey: "longitude") as? String ?? ""
                        
                        
                        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Apply ATM Filter", payload: payloadDict, block: nil)
                        
                    }
                    else
                    {
                        appDelegate.showToast(title: "Error", message: res!.value(forKey: "message") as! String)
                        self.imgvNoATMFound.isHidden = false
                        self.atmDisplayMapView.isHidden = true
                        self.lblTitle.text = "0 ATMS FOUND"
                        self.viewForCurrentLocation.isHidden = true
                        self.viewATMAnimation.isHidden = true
                        self.viewForQuickSuggestion.isHidden = true
                        
                        
                    }
                }
                else
                {
                    appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
                    self.imgvNoATMFound.isHidden = false
                    self.atmDisplayMapView.isHidden = true
                    self.lblTitle.text = "0 ATMS FOUND"
                    self.viewForCurrentLocation.isHidden = true
                    self.viewATMAnimation.isHidden = true
                    self.viewForQuickSuggestion.isHidden = true
                    
                    
                }
            }
        }
    }
    
    
    
    func subscriptionWebService(planId:String, planName:String, transactionId:String, endDate:String) {
        
        let url = BASE_URL + API_SUBSCRIPTION
        
        appDelegate.showHUD()
        
        let param = ["user_id" : nsuserDefault.getUserDetails().userId,
                     "plan_id" : planId,
                     "plan_name" : planName,
                     "transaction_id" : transactionId,
                     "device_type":"iPhone",
                     "subscription_end_date" : endDate] as [String:String]
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, error) in
            
            if error == nil {
                
                let res = response
                
                if res?.value(forKey: "status") as! Bool == true {
                    
                    let data = res?.object(forKey: "data") as? [String:Any]
                    nsuserDefault.setUserDetails(logindata: LoginClass(fromDictionary: data!))
                    //self.arrGetPlan = self.arrGetPlanDuplicated
                    //self.collectionForSubscription.reloadData()
                    appDelegate.hideHUD()
                    USERDEFAULT.set(true, forKey: "isUserLoginRestoreSubcription")
                    let plan = activePlanDetails(dic: nsuserDefault.getUserDetails().activePlan)
                    let payloadDict = NSMutableDictionary()
                    payloadDict["user_email"] = nsuserDefault.getUserDetails().email ?? ""
                    payloadDict["user_name"]  = nsuserDefault.getUserDetails().name ?? ""
                    payloadDict["plan_id"] = plan.plan_id
                    payloadDict["plan_amount"]  = plan.amount
                    payloadDict["plan_amount_currency"] = plan.currency
                    payloadDict["plan_name"]  = plan.plan_name
                    payloadDict["plan_duration"] = plan.duration
                    payloadDict["plan_type"]  = plan.plan_type
                    payloadDict["plan_save_amount"] = plan.save_amount
                    payloadDict["plan_expire_date"]  = nsuserDefault.getUserDetails().subScribeEndDate ?? ""
                    
                    NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Subscribe Plan", payload: payloadDict, block: nil)
                    
                }else {
                    appDelegate.hideHUD()
                    appDelegate.showToast(title: "", message: res?.value(forKey: "message") as? String ?? "")
                }
                
            }else {
                
            }
        }
    }
    
    //MARK: - GMSMapView delegate
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        
        currentPageForBank = 0
        
        self.btnNextForBank.sendActions(for: .touchUpInside)
        
        mapView.delegate = self
        
        self.viewATMAnimation.isHidden = false
        self.viewForQuickSuggestion.isHidden = false
        
        
        
        UIView.animate(withDuration: 0, animations: {
            self.viewATMAnimation.transform = CGAffineTransform(scaleX: 0, y: 0)
        }) { (finished) in
            UIView.animate(withDuration: 0.5, animations: {
                self.viewATMAnimation.transform = CGAffineTransform.identity
            })
        }
        
        if(self.arrForAtm.count > 0)
        {
            self.arrAtmFound = self.arrForAtm[Int(marker.zIndex)] as! [Any]
            print(arrAtmFound)
            
            let dictData = arrAtmFound[0] as! [String:Any]
            
            strDestinationAddress = (dictData["atm_address"] as! String)
            strAATMLati = (dictData["latitude"] as! String)
            strAATMLongi = (dictData["longitude"] as! String)
            strATMID = (dictData["atm_id"] as! String)
            strATMType = (dictData["bank_code"] as! String)
            self.strQueueType = (dictData["queue"] as! String)
            self.strATMImage = (dictData["atm_image"] as! String)
            
            
            //
            
            let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: Double((dictData["latitude"] as! String))!, longitude: Double((dictData["longitude"] as! String))!), zoom: 15.0)
            
            self.atmDisplayMapView.camera = camera
            
            
            let mapInsets = UIEdgeInsets(top: 230.0, left: 0.0, bottom: 0, right: 0.0)
            
            self.atmDisplayMapView.padding = mapInsets
            
            
            //
            
            
            let urlImage = dictData["atm_image"]
            
            imgBankLogo.af_setImage(withURL: URL.init(string: urlImage as! String)!)
            lblBankName.text = (dictData["atm_type"] as! String)
            
            
            if self.arrAtmFound.count <= 1{
                self.btnNext.isHidden = true
                self.btnPrevious.isHidden = true
                self.pageControl.isHidden = true
            }
            else
            {
                self.btnNext.isHidden = false
                self.btnPrevious.isHidden = false
                self.pageControl.isHidden = false
            }
            
            
            self.pageControl.numberOfPages = self.arrAtmFound.count
            self.pageControl.currentPage = 0
            self.pageControl.tintColor = UIColor.init(red: 218/255.0, green: 217/255.0, blue: 226/255.0, alpha: 1)
            
            self.pageControl.pageIndicatorTintColor = UIColor.init(red: 218/255.0, green: 217/255.0, blue: 226/255.0, alpha: 1)
            self.pageControl.currentPageIndicatorTintColor = UIColor.init(red: 155/255.0, green: 153/255.0, blue: 169/255.0, alpha: 1)
            
            
            self.AtlCollectionView.reloadData()
        }
        print("You tapped : \(marker.position.latitude),\(marker.position.longitude)")
        
        return true
        
    }
    
    //MARK: - Current location
    //MARK: -
    
    func putATMMarkersOnMap() {
        
        for marker in markers
        {
            marker.map = nil
        }
        self.markers.removeAll()
        
        
        self.putCurrentLocation()
        
        for i in 0..<arrForAtm.count {
            
            self.arrAtmFound = self.arrForAtm[i] as! [Any]
            
            let dictData = arrAtmFound[0] as! [String:Any]
            lblLocation.text = (dictData["atm_address"] as! String)
            
            
            let locDesti = CLLocation(latitude: Double((dictData["latitude"] as! String))!, longitude: Double((dictData["longitude"] as! String))!)
            /*
             let marker = GMSMarker()
             
             marker.zIndex = Int32(i)
             marker.position = CLLocationCoordinate2D(latitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude)
             
             let myView = UIView(frame: CGRect(x: 0, y: 0, width:100, height: 50))
             myView.backgroundColor = .clear
             
             let myImageView = UIImageView(frame: CGRect(x: 0, y: 0, width:100, height: 50))
             myImageView.image = UIImage(named: "infowindow")
             
             
             let atmIcon = UIImageView(frame: CGRect(x: 20, y: 17, width:15, height: 15))
             atmIcon.image = UIImage(named: "atmIcon")
             
             let label = UILabel(frame: CGRect(x: 40, y: 12.5, width: 60, height: 25))
             label.textAlignment = .left
             label.font = self.lblTitleForMarker.font
             label.text = (dictData["bank_code"] as! String).uppercased()
             */
            
            let marker = GMSMarker()
            
            marker.zIndex = Int32(i)
            
            marker.position = CLLocationCoordinate2D(latitude: locDesti.coordinate.latitude, longitude: locDesti.coordinate.longitude)
            
            let myView = UIView(frame: CGRect(x: 0, y: 0, width:100, height: 50))
            myView.backgroundColor = .clear
            
            let myImageView = UIImageView(frame: CGRect(x: 0, y: 0, width:100, height: 50))
            myImageView.image = UIImage(named: "infowindow_purple")
            
            
            let atmIcon = UIImageView(frame: CGRect(x: 17, y: 10, width:15, height: 15))
            atmIcon.image = UIImage(named: "atmIcon_white")
            
            let label = UILabel(frame: CGRect(x: 35, y: 8, width: 50, height: 20))
            label.textAlignment = .center
            label.textColor = .white
            label.font = self.lblTitleForMarker.font
            label.text = (dictData["bank_code"] as! String).uppercased()
            
            
            let labelQueue = UILabel(frame: CGRect(x: 20, y: 27, width: 40, height: 15))
            labelQueue.textAlignment = .right
            labelQueue.textColor = .white
            labelQueue.font = UIFont(name: "Arial", size: 10)
            labelQueue.text = "Queue : "
            
            let queueIcon = UIImageView(frame: CGRect(x: 65, y: 30, width:10, height: 10))
            
            if((dictData["queue"] as! String) == "green")
            {
                queueIcon.image = UIImage(named: "green")!
            }
            else if((dictData["queue"] as! String) == "red")
            {
                queueIcon.image = UIImage(named: "red")!
            }
            else
            {
                queueIcon.image = UIImage(named: "orange")!
            }
            
            
            
            myView.addSubview(myImageView)
            myView.addSubview(atmIcon)
            myView.addSubview(label)
            myView.addSubview(labelQueue)
            myView.addSubview(queueIcon)
            
            
            
            marker.iconView = myView
            
            marker.map = self.atmDisplayMapView
            
            markers.append(marker)
            
        }
        
        
        var bounds = GMSCoordinateBounds()
        for marker in markers
        {
            bounds = bounds.includingCoordinate(marker.position)
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
        self.atmDisplayMapView.animate(with: update)
        
    }
    
    func putCurrentLocation() {
        
        
        if !CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .denied
        {
            
            
            let alert = UIAlertController.init(title: "Location Services Disabled!", message: "Please enable Location Based Services for better results! We promise to keep your location private", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Settings", style: .default)
            { (action) in
                if !CLLocationManager.locationServicesEnabled()
                {
                    self.openDeviceLocationSetting()
                }
                else
                {
                    self.openAppLocationSetting()
                }
            }
            
            let cancel = UIAlertAction(title: "Try again", style: .default) { (_) in
                self.putCurrentLocation()
            }
            
            alert.addAction(ok)
            alert.addAction(cancel)
            
            //self.present(alert, animated: true, completion: nil)
        }
        else
        {
            
            let loc = CLLocation(latitude: Double(USERDEFAULT.value(forKey: "latitude") as! String)!, longitude: Double(USERDEFAULT.value(forKey: "longitude") as! String)!)
            //            self.atmDisplayMapView.clear()
            
            if CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways
            {
                let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude), zoom: 12.0)
                
                self.atmDisplayMapView.camera = camera
                
                let markerImage = UIImage(named: "myself")!.withRenderingMode(.alwaysOriginal)
                
                let markerView = UIImageView(image: markerImage)
                markerView.frame.size.height = 59.0
                markerView.frame.size.width = 56.0
                
                tripMarker.position = CLLocationCoordinate2D(latitude: Double(USERDEFAULT.value(forKey: "latitude") as! String)!, longitude: Double(USERDEFAULT.value(forKey: "longitude") as! String)!)
                
                tripMarker.iconView = markerView
                
                tripMarker.map = self.atmDisplayMapView
                
                self.atmDisplayMapView.selectedMarker = tripMarker
                markers.append(tripMarker)
                
                var bounds = GMSCoordinateBounds()
                for marker in markers
                {
                    bounds = bounds.includingCoordinate(marker.position)
                }
                let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
                self.atmDisplayMapView.animate(with: update)
                
            }
            else
            {
                
            }
        }
    }
    
    func openDeviceLocationSetting()
    {
        UIApplication.shared.open(URL.init(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
    }
    
    func openAppLocationSetting()
    {
        UIApplication.shared.open(URL.init(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueToMapRoute"
        {
            let dest = segue.destination as! ATMRouteMapVC
            
            dest.strSourceAddress = self.lblCurrentAddress.text!
            dest.strDestinationAddress = strDestinationAddress
            dest.strAATMLati = strAATMLati
            dest.strAATMLongi = strAATMLongi
            dest.strATMID = strATMID
            dest.strATMType = strATMType
            dest.strATMImage = strATMImage
            dest.strQueueType = strQueueType
            dest.strATMTypeee = strATMTypeee
            dest.strATMNumberFromLeft = String(self.pageControl.currentPage)
            
        }
    }
    
}

extension ATMMapVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout 
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(collectionView == self.AtlCollectionView)
        {
            return arrAtmFound.count; //self.arrAtmFound.count
            
        }
        else if(collectionView == self.collectionForBankFilter)
        {
            return arrForBank.count
        }
        else if(collectionView == self.collectionForMinimumLimit)
        {
            return arrForMinimumLimit.count
        }
        else
        {
            return arrForMaximumLimit.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == self.AtlCollectionView)
        {
            let cell = AtlCollectionView.dequeueReusableCell(withReuseIdentifier: "ATMListCollectionCell", for: indexPath) as! ATMListCollectionCell
            
            let dictData = arrAtmFound[indexPath.row] as! [String:Any]
            lblLocation.text = (dictData["atm_address"] as! String)
            
            cell.lblMinAmount.text = "Min: ₦" + (dictData["min_amount"] as! String)
            cell.lblMaxAmount.text = "Max: ₦" + (dictData["max_amount"] as! String)
            
            
            cell.lblEastimatedTime.text = "ETT: " + (dictData["ETT"] as! String) + " mins"
            
            if((dictData["queue"] as! String) == "green")
            {
                cell.imgQueueStatus.image = UIImage(named: "green")!
            }
            else if((dictData["queue"] as! String) == "red")
            {
                cell.imgQueueStatus.image = UIImage(named: "red")!
            }
            else
            {
                cell.imgQueueStatus.image = UIImage(named: "orange")!
            }
            
            return cell
        }
        else if(collectionView == self.collectionForBankFilter)
        {
            let cell = self.collectionForBankFilter.dequeueReusableCell(withReuseIdentifier: "BankFilterCell", for: indexPath) as! BankFilterCell
            
            cell.backgroundColor = UIColor.clear
            
            let dictData = self.arrForBank.object(at: indexPath.row) as! NSDictionary
            
            if let urlImage =  dictData["bank_image"]
            {
                
                cell.imgvBankIcon.af_setImage(withURL: URL.init(string: urlImage as! String)!)
            }
            
            let arrBankFilter = appDelegate.strFilterBank.components(separatedBy: ",")
            if arrBankFilter.contains(dictData["bank_id"] as! String) {
                
                cell.imgvCheck.isHidden = false
            }
            else
            {
                cell.imgvCheck.isHidden = true
                
            }
            return cell
        }
        else if(collectionView == self.collectionForMinimumLimit)
            
        {
            
            let cell = self.collectionForMinimumLimit.dequeueReusableCell(withReuseIdentifier: "DispenseLimitCell", for: indexPath) as! DispenseLimitCell
            
            cell.backgroundColor = UIColor.clear
            
            let strText = self.arrForMinimumLimit.object(at: indexPath.row) as! NSString
            
            cell.btnAmount.setTitle("₦ " + (strText as String) as String,for: .normal)
            
            cell.btnAmount.tag = indexPath.row
            cell.btnAmount.addTarget(self, action: #selector(self
                .btnMinimumAmountClicked(_:)), for: .touchUpInside)
            
            if(appDelegate.strFilterMinimumDispense == strText as String)
            {
                cell.btnAmount.backgroundColor = UIColor.rgb(95, green: 93, blue: 112)
                cell.btnAmount.layer.cornerRadius = 5
                cell.btnAmount.layer.borderWidth = 1
                cell.btnAmount.layer.borderColor = UIColor.rgb(246, green: 246, blue: 246).cgColor
                cell.btnAmount.setTitleColor(.white, for: .normal)
                
            }
            else
            {
                cell.btnAmount.backgroundColor = UIColor.rgb(246, green: 246, blue: 246)
                cell.btnAmount.layer.cornerRadius = 5
                cell.btnAmount.layer.borderWidth = 1
                cell.btnAmount.layer.borderColor = UIColor.rgb(190, green: 190, blue: 190).cgColor
                cell.btnAmount.setTitleColor(UIColor.rgb(95, green: 93, blue: 112), for: .normal)
            }
            
            return cell
        }
        else
        {
            let cell = self.collectionForMaximumLimit.dequeueReusableCell(withReuseIdentifier: "DispenseLimitCell", for: indexPath) as! DispenseLimitCell
            
            cell.backgroundColor = UIColor.clear
            
            
            let strText = self.arrForMaximumLimit.object(at: indexPath.row) as! NSString
            
            cell.btnAmount.setTitle("₦ " + (strText as String) as String,for: .normal)
            
            cell.btnAmount.tag = indexPath.row
            cell.btnAmount.addTarget(self, action: #selector(self
                .btnmaximumAmountClicked(_:)), for: .touchUpInside)
            
            
            if(appDelegate.strFilterMaximumDispense == strText as String)
            {
                cell.btnAmount.backgroundColor = UIColor.rgb(95, green: 93, blue: 112)
                cell.btnAmount.layer.cornerRadius = 5
                cell.btnAmount.layer.borderWidth = 1
                cell.btnAmount.layer.borderColor = UIColor.rgb(246, green: 246, blue: 246).cgColor
                cell.btnAmount.setTitleColor(.white, for: .normal)
                
            }
            else
            {
                cell.btnAmount.backgroundColor = UIColor.rgb(246, green: 246, blue: 246)
                cell.btnAmount.layer.cornerRadius = 5
                cell.btnAmount.layer.borderWidth = 1
                cell.btnAmount.layer.borderColor = UIColor.rgb(190, green: 190, blue: 190).cgColor
                cell.btnAmount.setTitleColor(UIColor.rgb(95, green: 93, blue: 112), for: .normal)
            }
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(collectionView == self.AtlCollectionView)
        {
            return CGSize(width: collectionView.frame.size.width, height: 60)
        }
        else if(collectionView == self.collectionForBankFilter)
            
        {
            return CGSize(width: 50, height: 50)
        }
        else
        {
            return CGSize(width: 80, height: 35)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(collectionView == self.collectionForBankFilter)
        {
            self.btnSave.isEnabled = true
            self.btnSave.setTitleColor(filter_pink, for: .normal)
            
            let dictSelectedBank = self.arrForBank[indexPath.row] as! NSDictionary
            let strSelectedBank = dictSelectedBank["bank_id"] as! NSString
            let strSelectedBankCode = dictSelectedBank["bank_name"] as? String ?? ""
            
            if(appDelegate.strFilterBank.count>0)
            {
                var arrBankFilter = appDelegate.strFilterBank.components(separatedBy: ",")
                var arrBankCodeFilter = appDelegate.strFilterBankCode.components(separatedBy: ",")
                
                if arrBankFilter.contains(strSelectedBank as String) {
                    print("yes")
                    let indexOfA = arrBankFilter.index(of: strSelectedBank as String)
                    arrBankFilter.remove(at: indexOfA!)
                    arrBankCodeFilter.remove(at: indexOfA!)
                }
                else
                {
                    arrBankFilter.append(strSelectedBank as String)
                    arrBankCodeFilter.append(strSelectedBankCode as String)
                }
                
                appDelegate.strFilterBankCode = arrBankCodeFilter.joined(separator: ",")
                appDelegate.strFilterBank = arrBankFilter.joined(separator: ",")
                print(appDelegate.strFilterBankCode)
                print(appDelegate.strFilterBank)
                self.collectionForBankFilter.reloadData()
            }
            else
            {
                var arrBankFilter = [String]()
                var arrBankCodeFilter = [String]()
                
                if arrBankFilter.contains(strSelectedBank as String) {
                    print("yes")
                    let indexOfA = arrBankFilter.index(of: strSelectedBank as String)
                    arrBankFilter.remove(at: indexOfA!)
                    arrBankCodeFilter.remove(at: indexOfA!)
                }
                else
                {
                    arrBankFilter.append(strSelectedBank as String)
                    arrBankCodeFilter.append(strSelectedBankCode as String)
                }
                
                appDelegate.strFilterBankCode = arrBankCodeFilter.joined(separator: ",")
                appDelegate.strFilterBank = arrBankFilter.joined(separator: ",")
                
                print(appDelegate.strFilterBankCode)
                print(appDelegate.strFilterBank)
                self.collectionForBankFilter.reloadData()
            }
        }
    }
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        
        
        var visibleRect = CGRect()
        
        visibleRect.origin = AtlCollectionView.contentOffset
        visibleRect.size = AtlCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = AtlCollectionView.indexPathForItem(at: visiblePoint) else { return }
        
        print(indexPath)
        self.pageControl.currentPage = indexPath.row
        currentPage = indexPath.row
        
        if(currentPage == 0)
        {
            btnPrevious.setImage(UIImage(named: "prev_end"), for: .normal)
            
        }
        else
        {
            btnPrevious.setImage(UIImage(named: "prev"), for: .normal)
            
        }
        if(currentPage != arrAtmFound.count-1)
        {
            btnNext.setImage(UIImage(named: "next"), for: .normal)
        }
        else
        {
            btnNext.setImage(UIImage(named: "next_end"), for: .normal)
        }
        
        
        let cell = AtlCollectionView.cellForItem(at: indexPath) as! ATMListCollectionCell
        print(cell)
        let dictData = arrAtmFound[indexPath.row] as! [String:Any]
        lblLocation.text = (dictData["atm_address"] as! String)
        
        
    }
}


