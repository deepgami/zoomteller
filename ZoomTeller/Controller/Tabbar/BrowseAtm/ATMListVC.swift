//
//  ATMListVC.swift
//  ZoomTeller
//
//  Created by ZerOnes on 26/04/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import NetCorePush

class ATMListVC: UIViewController , CLLocationManagerDelegate{
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var sliderForDistance: CustomUISlider!
    
    
    @IBOutlet weak var tblAtmList: UITableView!
    
    @IBOutlet weak var collectionForBankFilter: UICollectionView!
    @IBOutlet weak var collectionForMinimumLimit: UICollectionView!
    @IBOutlet weak var collectionForMaximumLimit: UICollectionView!
    
    @IBOutlet weak var viewForFilter: UIView!
    
    @IBOutlet weak var imgTabBrowse: UIImageView!
    @IBOutlet weak var imgTabMap: UIImageView!
    @IBOutlet weak var imgTabProfile: UIImageView!
    @IBOutlet weak var imgvNoATMFound: UIImageView!
    @IBOutlet weak var imgLocation: UIImageView!
    
    @IBOutlet weak var viewFilterWithOutSubscription: UIView!
    @IBOutlet weak var viewLocationEnabel: UIView!
    
    var refreshControl = UIRefreshControl()
    
    
    
    var arrAtmList = [Any]()
    var arrForBank : NSMutableArray = NSMutableArray()
    var arrForMinimumLimit : NSMutableArray = NSMutableArray()
    var arrForMaximumLimit : NSMutableArray = NSMutableArray()
    
    
    var strSourceAddress : String = ""
    var strDestinationAddress : String = ""
    var strAATMLati : String = ""
    var strAATMLongi : String = ""
    var strATMID : String = ""
    var strATMImage : String = ""

    var timerCheckLocation : Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        paerntATMListVC = self
        
        //if !CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .denied  {
        if appDelegate.locationPermissionIsValid() == true {
            viewLocationEnabel.isHidden = false
        }else {
            viewLocationEnabel.isHidden = true
        }

        self.tabBarController?.tabBar.isHidden = true
        
        self.viewForFilter.isHidden = true
        
        
        imgLocation.image = imgLocation.image?.withRenderingMode(.alwaysTemplate)
        imgLocation.tintColor = .gray
        
        
        refreshControl.attributedTitle = NSAttributedString(string: "Loading ATMs...")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tblAtmList.addSubview(refreshControl)
        
        arrForMinimumLimit = ["500","1000"]
        arrForMaximumLimit = ["10000","20000","30000","40000","50000"]
        self.collectionForMinimumLimit.reloadData()
        self.collectionForMaximumLimit.reloadData()
        self.getBankList()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Atm List Activity open", payload: payloadDict, block: nil)
        
        viewFilterWithOutSubscription.isHidden = appDelegate.isPlanSubscribe()
        
        if appDelegate.isSkipUser() != nil {
        }else {
            
        }
        viewWillList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
     
        if(UserDefaults.standard.bool(forKey: "isShowSecondTab"))
        {
            UserDefaults.standard.set(false, forKey: "isShowSecondTab")
            self.tabBarController?.selectedIndex = 1
        }
    }
    
    @objc func viewWillList() {
        
        self.lblTitle.text = ""
        
        //if (!CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .denied) == false  {
        if appDelegate.locationPermissionIsValid() == true {
            appDelegate.startLocationManager()
            
            if appDelegate.getLocation() != nil {
                
                if timerCheckLocation != nil {
                    timerCheckLocation.invalidate()
                }
                
                viewLocationEnabel.isHidden = true
                getATMList()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    
                    self.getAddress() { (returnAddress) in
                        
                        print("\(returnAddress)")
                        self.strSourceAddress = returnAddress
                    }
                })
            }else {
                timerCheckLocation = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(viewWillList), userInfo: nil, repeats: false)
            }
            
        }else {
            viewLocationEnabel.isHidden = false
        }
    }
    // MARK: - Custom method
    
    func openDeviceLocationSetting()
    {
         UIApplication.shared.open(URL.init(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
    }
    
    func openAppLocationSetting()
    {
        UIApplication.shared.open(URL.init(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
    }
    
    func setFilterData(){
        
        self.sliderForDistance.value = (appDelegate.strFilterDistance as NSString).floatValue
    }
    
    @objc func refresh(sender:AnyObject) {
        
        refreshControl.endRefreshing()
        
        //if (!CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .denied) == false  {
        if appDelegate.locationPermissionIsValid() == true {
             viewLocationEnabel.isHidden = true
            getATMList()
        }else {
            viewLocationEnabel.isHidden = false
        }
    }
    
    
    func getAddress(currentAdd : @escaping ( _ returnAddress :String)->Void){
        
        if !CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .denied
        {
            
            
            let alert = UIAlertController.init(title: "Location Services Disabled!", message: "Please enable Location Based Services for better results! We promise to keep your location private", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Settings", style: .default)
            { (action) in
                if !CLLocationManager.locationServicesEnabled()
                {
                    self.openDeviceLocationSetting()
                }
                else
                {
                    self.openAppLocationSetting()
                }
            }
            
            let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            alert.addAction(cancel)
            
            //self.present(alert, animated: true, completion: nil)
        }
        else
        {
            
            let geocoder = GMSGeocoder()
            let coordinate = CLLocationCoordinate2DMake(Double(USERDEFAULT.value(forKey: "latitude") as! String)!,Double(USERDEFAULT.value(forKey: "longitude") as! String)!)
            
            var currentAddress = String()
            
            geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
                if let address = response?.firstResult() {
                    let lines = address.lines! as [String]
                    
                    currentAddress = lines.joined(separator: "\n")
                    
                    currentAdd(currentAddress)
                }
            }
        }
        
    }
    
    // MARK: - APIs method
    
    func getBankList() {
        
        
        let url = BASE_URL + API_GET_BANK
        let userDetail = nsuserDefault.getUserDetails()
        
        let param = ["user_id":userDetail.userId as String]
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, status) in
            
            appDelegate.hideHUD()
            
            if status == nil
            {
                let res = response
                if res?.value(forKey: "status") as! Bool == true
                {
                    let tagArray = res!["data"] as! NSArray
                    self.arrForBank = NSMutableArray(array:tagArray)
                    
                    self.collectionForBankFilter.reloadData()
                }
                else
                {
                    appDelegate.showToast(title: "Error", message: res!.value(forKey: "message") as! String)
                    
                }
            }
            else
            {
                appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
            }
        }
    }
    func getATMList() {
        
        
        if !CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .denied
        {
            
            
            let alert = UIAlertController.init(title: "Location Services Disabled!", message: "Please enable Location Based Services for better results! We promise to keep your location private", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Settings", style: .default)
            { (action) in
                if !CLLocationManager.locationServicesEnabled()
                {
                    self.openDeviceLocationSetting()
                }
                else
                {
                    self.openAppLocationSetting()
                }
            }
            
            let cancel = UIAlertAction(title: "Try again", style: .default) { (_) in
                self.getATMList()
            }
            
            alert.addAction(ok)
            alert.addAction(cancel)
            
            //self.present(alert, animated: true, completion: nil)
        }
        else
        {
            
            appDelegate.showHUD()
            
            let url = BASE_URL + API_GET_ATM
            let userDetail = nsuserDefault.getUserDetails()
            
            
            let param = ["user_id":userDetail.userId as String,
                         "latitude":USERDEFAULT.value(forKey: "latitude") as! String,
                         "longitude":USERDEFAULT.value(forKey: "longitude") as! String,
                         "bank_id":appDelegate.strFilterBank,
                         "min_amount" : appDelegate.strFilterMinimumDispense,
                         "max_amount":appDelegate.strFilterMaximumDispense,
                         "km":appDelegate.strFilterDistance]
            
            print(param)
            ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, status) in
                
                appDelegate.hideHUD()
                
                if status == nil
                {
                    let res = response
                    if res?.value(forKey: "status") as! Bool == true
                    {
                        // appDelegate.showToast(title: "Successful", message: res!.value(forKey: "message") as! String)
                        
                        self.arrAtmList = res?.object(forKey: "data") as! [Any]
                        
                        if(self.arrAtmList.count>0)
                        {
                            self.lblTitle.text = ((res!["count_atm"] as! NSString) as String) + " ATMS FOUND"
                            self.tblAtmList.reloadData()
                            self.imgvNoATMFound.isHidden = true
                            self.tblAtmList.isHidden = false
                            
                        }
                        else
                        {
                            self.imgvNoATMFound.isHidden = false
                            self.tblAtmList.isHidden = true
                            self.lblTitle.text = "0 ATMS FOUND"
                        }
                        
                        
                    }
                    else
                    {
                        appDelegate.showToast(title: "Error", message: res!.value(forKey: "message") as! String)
                        self.imgvNoATMFound.isHidden = false
                        self.tblAtmList.isHidden = true
                        self.lblTitle.text = "0 ATMS FOUND"
                    }
                    
                    let payloadDict = NSMutableDictionary()
                    payloadDict["user_email"] = nsuserDefault.getUserDetails().email ?? ""
                    payloadDict["user_name"]  = nsuserDefault.getUserDetails().name ?? ""
                    payloadDict["bank_id"]  = appDelegate.strFilterBank
                    payloadDict["bank_name"]  = appDelegate.strFilterBankCode
                    payloadDict["min_amount"]  = appDelegate.strFilterMinimumDispense
                    payloadDict["max_amount"]  = appDelegate.strFilterMaximumDispense
                    payloadDict["meter"]  = appDelegate.strFilterDistance
                    payloadDict["latitude"] = USERDEFAULT.value(forKey: "latitude") as? String ?? ""
                    payloadDict["longitude"] = USERDEFAULT.value(forKey: "longitude") as? String ?? ""
                    
                    NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Apply ATM Filter", payload: payloadDict, block: nil)
                    
                }
                else
                {
                    appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
                    self.imgvNoATMFound.isHidden = false
                    self.tblAtmList.isHidden = true
                    self.lblTitle.text = "0 ATMS FOUND"
                }
            }
        }
    }
    
    
    // MARK: - Button Action method
    
    @IBAction func onClickLocationEnabel(_ sender: Any) {
        appDelegate.openAppLocationSetting()
    }
    
    @IBAction func btnSaveFilteredClicked(_ sender: Any) {
        
        
        self.viewForFilter.isHidden = true
        
        let top = CGAffineTransform(translationX: 0, y: -self.view.frame.height)
        let bottom = CGAffineTransform(translationX: 0, y:0)
        
        
        UIView.animate(withDuration: 0, animations: {
            self.viewForFilter.transform = bottom
            
        }) { (finished) in
            UIView.animate(withDuration: 0.5, animations: {
                self.viewForFilter.transform = top
                self.viewForFilter.isHidden = false
                
            })
        }
        
        //if (!CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .denied) == false {
        if appDelegate.locationPermissionIsValid() == true {
             viewLocationEnabel.isHidden = true
            self.getATMList()
        }else {
            viewLocationEnabel.isHidden = false
        }
        
        
    }
    
    @objc func btnMinimumAmountClicked(_ sender : UIButton)
    {
        self.btnSave.isEnabled = true
        self.btnSave.setTitleColor(filter_pink, for: .normal)
        
        if(appDelegate.strFilterMinimumDispense == self.arrForMinimumLimit[sender.tag] as! String)
        {
            appDelegate.strFilterMinimumDispense = "0"
        }
        else
        {
            appDelegate.strFilterMinimumDispense = self.arrForMinimumLimit[sender.tag] as! String
        }
        self.collectionForMinimumLimit.reloadData()
    }
    @objc func btnmaximumAmountClicked(_ sender : UIButton)
    {
        self.btnSave.isEnabled = true
        self.btnSave.setTitleColor(filter_pink, for: .normal)
        
        if(appDelegate.strFilterMaximumDispense == self.arrForMaximumLimit[sender.tag] as! String)
        {
            appDelegate.strFilterMaximumDispense = "0"
        }
        else
        {
            appDelegate.strFilterMaximumDispense = self.arrForMaximumLimit[sender.tag] as! String
        }
        self.collectionForMaximumLimit.reloadData()
        
    }
    
    @IBAction func sliderValueChanged(sender: CustomUISlider) {
        
        let currentValue = Int(sender.value)
        print(currentValue)
        appDelegate.strFilterDistance = String(currentValue)
        self.btnSave.isEnabled = true
        self.btnSave.setTitleColor(filter_pink, for: .normal)
    }
    
    @objc func btnGetDirectionClicked(_ sender : UIButton)
    {
        
        var arrAtmFound = [Any]()
        
        arrAtmFound = self.arrAtmList[sender.tag] as! [Any]
        
        let dictData = arrAtmFound[0] as! [String:Any]
        
        strDestinationAddress = (dictData["atm_address"] as! String)
        strAATMLati = (dictData["latitude"] as! String)
        strAATMLongi = (dictData["longitude"] as! String)
        strATMID = (dictData["atm_id"] as! String)
        self.strATMImage = (dictData["atm_image"] as! String)
        
        let currentDT = Date()
        let dateF = DateFormatter()
        dateF.dateFormat = "hh:mm a"
        let time = dateF.string(from: currentDT)
        dateF.dateFormat = "dd-MM-yyyy"
        let date = dateF.string(from: currentDT)
        
        let payloadDict = NSMutableDictionary()
        payloadDict["user_email"] = nsuserDefault.getUserDetails().email ?? ""
        payloadDict["user_name"]  = nsuserDefault.getUserDetails().name ?? ""
        payloadDict["atm_aaddress"]  = dictData["atm_address"] as? String ?? ""
        payloadDict["ATM"]  = dictData["atm_type"] as? String ?? ""
        payloadDict["bank_code"]  = dictData["bank_code"] as? String ?? ""
        payloadDict["time"]  = time
        payloadDict["date"]  = date
        payloadDict["destination_latitude"] = dictData["latitude"] as? String ?? ""
        payloadDict["destination_longitude"] = dictData["longitude"] as? String ?? ""
        payloadDict["user_latitude"] = USERDEFAULT.value(forKey: "latitude") as? String ?? ""
        payloadDict["user_longitude"] = USERDEFAULT.value(forKey: "longitude") as? String ?? ""
        
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "ATM List Direction Button", payload: payloadDict, block: nil)
        
       
        self.performSegue(withIdentifier: "segueToMapRoute", sender: self)
        
        
    }
    
    @IBAction func onClickSubscriptionVC(_ sender: Any) {
        self.performSegue(withIdentifier: "segueToSubscription", sender: self)
    }
    
    @IBAction func onClickHideFilter(_ sender: Any) {
        
        self.viewForFilter.isHidden = true
        
        let top = CGAffineTransform(translationX: 0, y: -self.view.frame.height)
        let bottom = CGAffineTransform(translationX: 0, y:0)
        
        
        UIView.animate(withDuration: 0, animations: {
            self.viewForFilter.transform = bottom
            
        }) { (finished) in
            UIView.animate(withDuration: 0.5, animations: {
                self.viewForFilter.transform = top
                self.viewForFilter.isHidden = false
                
            })
        }
    }
    
    @IBAction func onClickFilter(_ sender: Any) {
            self.setFilterData()
            
            self.btnSave.isEnabled = false
            self.btnSave.setTitleColor(filter_pink_disable, for: .normal)
            
            let top = CGAffineTransform(translationX: 0, y: -self.view.frame.height)
            let bottom = CGAffineTransform(translationX: 0, y:0)
            
            
            UIView.animate(withDuration: 0, animations: {
                self.viewForFilter.transform = top
                
            }) { (finished) in
                UIView.animate(withDuration: 0.5, animations: {
                    self.viewForFilter.transform = bottom
                    self.viewForFilter.isHidden = false
                    
                })
            }
    }
    
    @IBAction func onClickMenu(_ sender: Any) {
        self.sideMenuController?.showLeftViewAnimated()
    }
    
    @IBAction func btnTabBarClicked(_ sender: Any) {
        let button = sender as! UIButton
        if appDelegate.isSkipUser() != nil {
            tabBarController?.selectedIndex = button.tag
        }else {
            if button.tag == 2 {
                appDelegate.appSkipAlert(vc: self)
            }else {
                tabBarController?.selectedIndex = button.tag
            }
        }
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueToMapRoute"
        {
            let dest = segue.destination as! ATMRouteMapVC
            
            dest.strSourceAddress = self.strSourceAddress
            dest.strDestinationAddress = strDestinationAddress
            dest.strAATMLati = strAATMLati
            dest.strAATMLongi = strAATMLongi
            dest.strATMID = strATMID
            dest.strATMImage = strATMImage
        }
    }
}

// MARK: - UITableViewDelegate method


extension ATMListVC : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.arrAtmList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //jj
        
        let cell = tblAtmList.dequeueReusableCell(withIdentifier:  "ATMListTabelCell", for: indexPath) as! ATMListTabelCell
        
        
        cell.selectionStyle = .none;
        
        cell.arrAtmFound = self.arrAtmList[indexPath.row] as! [Any]
        
        cell.setDatatoCell()
        cell.viewListATMWithOutSubscription.isHidden = appDelegate.isPlanSubscribe()
        cell.btnGetDirection.isEnabled = appDelegate.isPlanSubscribe()
        
        cell.btnGetDirection.tag = indexPath.row
        cell.btnGetDirection.addTarget(self, action: #selector(self
            .btnGetDirectionClicked(_:)), for: .touchUpInside)
        cell.btnSubscribe.tag = indexPath.row
        cell.btnSubscribe.addTarget(self, action: #selector(btnSubscriptionSegueToSubscription), for: .touchUpInside)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 196.0
    }
    
    @objc func btnSubscriptionSegueToSubscription() {
        self.performSegue(withIdentifier: "segueToSubscription", sender: self)
    }
    
}


// MARK: - UICollectionViewDelegate method

extension ATMListVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if(collectionView == self.collectionForBankFilter)
        {
            return arrForBank.count
        }
        else if(collectionView == self.collectionForMinimumLimit)
        {
            return arrForMinimumLimit.count
        }
        else
        {
            return arrForMaximumLimit.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == self.collectionForBankFilter)
        {
            let cell = self.collectionForBankFilter.dequeueReusableCell(withReuseIdentifier: "BankFilterCell", for: indexPath) as! BankFilterCell
            
            cell.backgroundColor = UIColor.clear
            
            let dictData = self.arrForBank.object(at: indexPath.row) as! NSDictionary
            
            if let urlImage =  dictData["bank_image"]
            {
                
                cell.imgvBankIcon.af_setImage(withURL: URL.init(string: urlImage as! String)!)
            }
            
            let arrBankFilter = appDelegate.strFilterBank.components(separatedBy: ",")
            
            if arrBankFilter.contains(dictData["bank_id"] as! String) {
                
                cell.imgvCheck.isHidden = false
            }
            else
            {
                cell.imgvCheck.isHidden = true
                
            }
            return cell
        }
        else if(collectionView == self.collectionForMinimumLimit)
            
        {
            
            let cell = self.collectionForMinimumLimit.dequeueReusableCell(withReuseIdentifier: "DispenseLimitCell", for: indexPath) as! DispenseLimitCell
            
            cell.backgroundColor = UIColor.clear
            
            let strText = self.arrForMinimumLimit.object(at: indexPath.row) as! NSString
            
            cell.btnAmount.setTitle("₦ " + (strText as String) as String,for: .normal)
            
            cell.btnAmount.tag = indexPath.row
            cell.btnAmount.addTarget(self, action: #selector(self
                .btnMinimumAmountClicked(_:)), for: .touchUpInside)
            
            if(appDelegate.strFilterMinimumDispense == strText as String)
            {
                cell.btnAmount.backgroundColor = UIColor.rgb(95, green: 93, blue: 112)
                cell.btnAmount.layer.cornerRadius = 5
                cell.btnAmount.layer.borderWidth = 1
                cell.btnAmount.layer.borderColor = UIColor.rgb(246, green: 246, blue: 246).cgColor
                cell.btnAmount.setTitleColor(.white, for: .normal)
                
            }
            else
            {
                cell.btnAmount.backgroundColor = UIColor.rgb(246, green: 246, blue: 246)
                cell.btnAmount.layer.cornerRadius = 5
                cell.btnAmount.layer.borderWidth = 1
                cell.btnAmount.layer.borderColor = UIColor.rgb(190, green: 190, blue: 190).cgColor
                cell.btnAmount.setTitleColor(UIColor.rgb(95, green: 93, blue: 112), for: .normal)
            }
            
            return cell
        }
        else
        {
            let cell = self.collectionForMaximumLimit.dequeueReusableCell(withReuseIdentifier: "DispenseLimitCell", for: indexPath) as! DispenseLimitCell
            
            cell.backgroundColor = UIColor.clear
            
            
            let strText = self.arrForMaximumLimit.object(at: indexPath.row) as! NSString
            
            cell.btnAmount.setTitle("₦ " + (strText as String) as String,for: .normal)
            
            cell.btnAmount.tag = indexPath.row
            cell.btnAmount.addTarget(self, action: #selector(self
                .btnmaximumAmountClicked(_:)), for: .touchUpInside)
            
            
            if(appDelegate.strFilterMaximumDispense == strText as String)
            {
                cell.btnAmount.backgroundColor = UIColor.rgb(95, green: 93, blue: 112)
                cell.btnAmount.layer.cornerRadius = 5
                cell.btnAmount.layer.borderWidth = 1
                cell.btnAmount.layer.borderColor = UIColor.rgb(246, green: 246, blue: 246).cgColor
                cell.btnAmount.setTitleColor(.white, for: .normal)
                
            }
            else
            {
                cell.btnAmount.backgroundColor = UIColor.rgb(246, green: 246, blue: 246)
                cell.btnAmount.layer.cornerRadius = 5
                cell.btnAmount.layer.borderWidth = 1
                cell.btnAmount.layer.borderColor = UIColor.rgb(190, green: 190, blue: 190).cgColor
                cell.btnAmount.setTitleColor(UIColor.rgb(95, green: 93, blue: 112), for: .normal)
            }
            
            return cell
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.btnSave.isEnabled = true
        self.btnSave.setTitleColor(filter_pink, for: .normal)
        
        let dictSelectedBank = self.arrForBank[indexPath.row] as! NSDictionary
        let strSelectedBank = dictSelectedBank["bank_id"] as! NSString
        let strSelectedBankCode = dictSelectedBank["bank_name"] as? String ?? ""
        
        if(appDelegate.strFilterBank.count>0)
        {
            var arrBankFilter = appDelegate.strFilterBank.components(separatedBy: ",")
            var arrBankCodeFilter = appDelegate.strFilterBankCode.components(separatedBy: ",")
            
            if arrBankFilter.contains(strSelectedBank as String) {
                print("yes")
                let indexOfA = arrBankFilter.index(of: strSelectedBank as String)
                arrBankFilter.remove(at: indexOfA!)
                arrBankCodeFilter.remove(at: indexOfA!)
            }
            else
            {
                arrBankFilter.append(strSelectedBank as String)
                arrBankCodeFilter.append(strSelectedBankCode as String)
            }
            
            appDelegate.strFilterBank = arrBankFilter.joined(separator: ",")
            appDelegate.strFilterBankCode = arrBankCodeFilter.joined(separator: ",")
            print(appDelegate.strFilterBankCode)
            print(appDelegate.strFilterBank)
            self.collectionForBankFilter.reloadData()
        }
        else
        {
            var arrBankFilter = [String]()
            var arrBankCodeFilter = [String]()
            
            if arrBankFilter.contains(strSelectedBank as String) {
                print("yes")
                let indexOfA = arrBankFilter.index(of: strSelectedBank as String)
                arrBankFilter.remove(at: indexOfA!)
                arrBankCodeFilter.remove(at: indexOfA!)
            }
            else
            {
                arrBankFilter.append(strSelectedBank as String)
                arrBankCodeFilter.append(strSelectedBankCode as String)
            }
            
            appDelegate.strFilterBank = arrBankFilter.joined(separator: ",")
            appDelegate.strFilterBankCode = arrBankCodeFilter.joined(separator: ",")
            print(appDelegate.strFilterBankCode)
            print(appDelegate.strFilterBank)
            self.collectionForBankFilter.reloadData()
        }
    }
    
}
