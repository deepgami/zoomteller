//
//  ATMListTabelCell.swift
//  ZoomTeller
//
//  Created by ZerOnes on 26/04/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit

class ATMListTabelCell: UITableViewCell {
    
    @IBOutlet weak var imgBankLogo: UIImageView!
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSubscribe: UIButton!
    @IBOutlet weak var AtlCollectionView: UICollectionView!
    @IBOutlet weak var viewListATMWithOutSubscription: UIView!
    
    
    var currentPage: Int = 0
    
    var arrAtmFound = [Any]()
    
    @IBOutlet weak var btnGetDirection: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func onClickPrevious(_ sender: Any) {
        
        if(currentPage > 0)
        {
            currentPage -= 1
            AtlCollectionView.scrollToPreviousItem()
            pageControl.currentPage = currentPage
            btnNext.setImage(UIImage(named: "next"), for: .normal)

        }
        
        
        if(currentPage == 0)
        {
            btnPrevious.setImage(UIImage(named: "prev_end"), for: .normal)

        }
        else
        {
            btnPrevious.setImage(UIImage(named: "prev"), for: .normal)

        }
    }
    
    @IBAction func onClickNext(_ sender: Any) {
        
        if(currentPage < arrAtmFound.count-1)
        {
            currentPage += 1
            AtlCollectionView.scrollToNextItem()
            pageControl.currentPage = currentPage
            btnPrevious.setImage(UIImage(named: "prev"), for: .normal)

        }
            
        if(currentPage != arrAtmFound.count-1)
        {
            btnNext.setImage(UIImage(named: "next"), for: .normal)
        }
        else
        {
            btnNext.setImage(UIImage(named: "next_end"), for: .normal)
        }
    }
    
    func setDatatoCell(){
        if arrAtmFound.count <= 1{
            self.btnNext.isHidden = true
            self.btnPrevious.isHidden = true
            pageControl.isHidden = true
        }
        else
        {
            self.btnNext.isHidden = false
            self.btnPrevious.isHidden = false
            pageControl.isHidden = false
        }
        let dictData = arrAtmFound[0] as! [String:Any]
        let urlImage = dictData["atm_image"]
        
        imgBankLogo.af_setImage(withURL: URL.init(string: urlImage as! String)!)
        lblBankName.text = (dictData["atm_type"] as! String)
        
        pageControl.numberOfPages = arrAtmFound.count
        pageControl.currentPage = 0
        pageControl.tintColor = UIColor.init(red: 218/255.0, green: 217/255.0, blue: 226/255.0, alpha: 1)
        
        pageControl.pageIndicatorTintColor = UIColor.init(red: 218/255.0, green: 217/255.0, blue: 226/255.0, alpha: 1)
        pageControl.currentPageIndicatorTintColor = UIColor.init(red: 155/255.0, green: 153/255.0, blue: 169/255.0, alpha: 1)
        
        btnPrevious.setImage(UIImage(named: "prev_end"), for: .normal)
        btnNext.setImage(UIImage(named: "next"), for: .normal)
        
        self.AtlCollectionView.reloadData()
        currentPage = 0
    }
}

extension ATMListTabelCell : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrAtmFound.count; //self.arrAtmFound.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = AtlCollectionView.dequeueReusableCell(withReuseIdentifier: "ATMListCollectionCell", for: indexPath) as! ATMListCollectionCell
        
        let dictData = arrAtmFound[indexPath.row] as! [String:Any]
        lblLocation.text = (dictData["atm_address"] as! String)
        
        cell.lblMinAmount.text = "Min: ₦" + (dictData["min_amount"] as! String)
        cell.lblMaxAmount.text = "Max: ₦" + (dictData["max_amount"] as! String)

        
        cell.lblEastimatedTime.text = "ETT: " + (dictData["ETT"] as! String) + " mins"

        if((dictData["queue"] as! String) == "green")
        {
            cell.imgQueueStatus.image = UIImage(named: "green")!
        }
        else if((dictData["queue"] as! String) == "red")
        {
            cell.imgQueueStatus.image = UIImage(named: "red")!
        }
        else
        {
            cell.imgQueueStatus.image = UIImage(named: "orange")!
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 65)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        var visibleRect = CGRect()
        
        visibleRect.origin = AtlCollectionView.contentOffset
        visibleRect.size = AtlCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = AtlCollectionView.indexPathForItem(at: visiblePoint) else { return }
        
        print(indexPath)
        self.pageControl.currentPage = indexPath.row
        currentPage = indexPath.row

        if(currentPage == 0)
        {
            btnPrevious.setImage(UIImage(named: "prev_end"), for: .normal)
            
        }
        else
        {
            btnPrevious.setImage(UIImage(named: "prev"), for: .normal)
            
        }
        if(currentPage != arrAtmFound.count-1)
        {
            btnNext.setImage(UIImage(named: "next"), for: .normal)
        }
        else
        {
            btnNext.setImage(UIImage(named: "next_end"), for: .normal)
        }
        
        
        let cell = AtlCollectionView.cellForItem(at: indexPath) as! ATMListCollectionCell
        print(cell)
        let dictData = arrAtmFound[indexPath.row] as! [String:Any]
        lblLocation.text = (dictData["atm_address"] as! String)
       
      
    }
}


extension UICollectionView {
    func scrollToNextItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.x + self.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
        //self.pageControl.currentPage = self.currentPage

    }
    
    func scrollToPreviousItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.x - self.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)

    }
    
    func moveToFrame(contentOffset : CGFloat) {
        self.setContentOffset(CGPoint(x: contentOffset, y: self.contentOffset.y), animated: true)
        self.reloadData()
    }
}
