//
//  ATMListCollectionCell.swift
//  ZoomTeller
//
//  Created by ZerOnes on 26/04/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit

class ATMListCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgQueueStatus: UIImageView!
    @IBOutlet weak var lblMinAmount: UILabel!
    @IBOutlet weak var lblEastimatedTime: UILabel!
    @IBOutlet weak var lblMaxAmount: UILabel!
}
