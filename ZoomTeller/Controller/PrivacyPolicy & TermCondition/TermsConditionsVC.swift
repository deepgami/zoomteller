//
//  TermsConditions.swift
//  ZoomTeller
//
//  Created by ZerOnes on 25/07/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit

class TermsConditionsVC: UIViewController {

    @IBOutlet weak var webViewPrivacypolicy : UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let url = URL(string: "http://zoomteller.com/terms_of_use.html") {
            webViewPrivacypolicy.loadRequest(URLRequest(url: url))
        }
    }
    
    @IBAction func btnNavigationPopUp(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
