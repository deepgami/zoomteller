//
//  AboutVC.swift
//  ZoomTeller
//
//  Created by ZerOnes on 03/07/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import NetCorePush

class AboutVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "About Activity Open", payload: payloadDict, block: nil)
        
        
    }
    
    @IBAction func btnNavigationPopUp(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
