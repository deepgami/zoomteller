
//
//  IntroVC.swift
//  ZoomTeller
//
//  Created by ZERONES on 19/03/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import ImageSlideshow
import Alamofire

class IntroVC: UIViewController {

    @IBOutlet var propertyImageSlideShow: ImageSlideshow!

    let arrImages = [UIImage(named: "welcome_1_bg"), UIImage(named: "welcome_2_bg"), UIImage(named: "welcome_3_bg")]

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblDescr: UILabel!
    
    @IBOutlet weak var pageIndicator: UIPageControl!
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    
    var arrHeader = ["Discover ATMs around you.",
                     "Skip the queue",
                     "Choose your denomination."]
    
    var arrDescr = ["Search, locate and get directions to Auto Teller Machines around you.",
                    "Avoid long crowd lines at the ATM.",
                    "Narrow your search to your prefered cash denomination."]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        propertyImageSlideShow.setImageInputs([
            ImageSource(image: arrImages[0]!),
            ImageSource(image: arrImages[1]!),
            ImageSource(image: arrImages[2]!)])
        
       
        self.propertyImageSlideShow.contentScaleMode = UIView.ContentMode.scaleAspectFill
        
        propertyImageSlideShow.pageIndicator = nil
        

//        propertyImageSlideShow.pageIndicatorPosition = PageIndicatorPosition(horizontal: .center, vertical: .customBottom(padding: 8))
        
        self.pageIndicator.numberOfPages = 3
        self.propertyImageSlideShow.circular = false
        
        
        self.propertyImageSlideShow.currentPageChanged = { [weak self] page in
            self?.lblHeader.text = self?.arrHeader[page]
            self?.lblDescr.text = self?.arrDescr[page]
            
            if page == 0
            {
                self!.btnSkip.isHidden = false
                
            }
            else if page == self!.arrImages.count-1
            {
                self!.btnSkip.isHidden = true
            }
            else
            {
                self!.btnSkip.isHidden = false
            }
            self!.pageIndicator.currentPage = page

        }

    }
    
    @IBAction func btnNextAclicked(_ sender: UIButton) {
        
        
        if (self.pageIndicator?.page)! < self.arrImages.count
        {
            if self.pageIndicator?.page == self.arrImages.count-1
            {
                self.performSegue(withIdentifier: "homeSegue", sender: self)
            }
            
            
            self.pageIndicator?.page+=1
            
            if self.pageIndicator?.page == 0
            {
                self.btnSkip.isHidden = false
            }
            else if self.pageIndicator?.page == self.arrImages.count-1
            {
                self.btnSkip.isHidden = true
            }
            else
            {
                self.btnSkip.isHidden = false
            }
            
            self.lblHeader.text = self.arrHeader[(self.pageIndicator?.page)!]
            self.lblDescr.text = self.arrDescr[(self.pageIndicator?.page)!]
            
            self.propertyImageSlideShow.setCurrentPage((self.pageIndicator?.page)!, animated: true)
            
            self.pageIndicator.currentPage = (self.pageIndicator?.page)!
            
            print("onClickNext , SelectedIndex = \(self.pageIndicator?.page ?? 0)")
            
        }
    }
    
    
    @IBAction func btnSkipClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: "homeSegue", sender: self)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
