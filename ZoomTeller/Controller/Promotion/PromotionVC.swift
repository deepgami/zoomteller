//
//  PromotionVC.swift
//  ZoomTeller
//
//  Created by Deep Gami on 24/06/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import NetCorePush

class PromotionVC: UIViewController {
    
    @IBOutlet weak var constraintCodeCopiedtoClipboardTop: NSLayoutConstraint!
   // @IBOutlet weak var constraintPromoImageHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintCheckImageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtFieldPromocod1: UITextField!
    @IBOutlet weak var txtFieldPromocod2: UITextField!
    @IBOutlet weak var txtFieldPromocod3: UITextField!
    @IBOutlet weak var txtFieldPromocod4: UITextField!
    @IBOutlet weak var txtFieldPromocod5: UITextField!
    @IBOutlet weak var txtFieldPromocod6: UITextField!
    
    @IBOutlet weak var viewPromocode1: UIView!
    @IBOutlet weak var viewPromocode2: UIView!
    @IBOutlet weak var viewPromocode3: UIView!
    @IBOutlet weak var viewPromocode4: UIView!
    @IBOutlet weak var viewPromocode5: UIView!
    @IBOutlet weak var viewPromocode6: UIView!
    
    @IBOutlet weak var lblMyPromocode: UILabel!
    @IBOutlet weak var lblPromocodeDetails: UILabel!
    
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var viewPromo: UIView!
    @IBOutlet weak var viewValidPromocode: UIView!
    
    @IBOutlet weak var lblPromoFreeDays: UILabel!
    @IBOutlet weak var lblMessagePromo: UILabel!
    
    var preveius:String!
    
    var countPromo = 0
    var promotext = [false ,false ,false ,false ,false ,false ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblMyPromocode.text = nsuserDefault.getUserDetails().promoCode
        imgCheck.image = imgCheck.image?.withRenderingMode(.alwaysTemplate)
        imgCheck.tintColor = .black
        self.constraintCodeCopiedtoClipboardTop.constant = -60
        self.viewValidPromocode.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        txtFieldPromocod1.delegate = self
        txtFieldPromocod2.delegate = self
        txtFieldPromocod3.delegate = self
        txtFieldPromocod4.delegate = self
        txtFieldPromocod5.delegate = self
        txtFieldPromocod6.delegate = self
        
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Promotion Activity Open", payload: payloadDict, block: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //constraintPromoImageHeight.constant = view.frame.width / 2.5
        constraintCheckImageHeight.constant = viewPromo.frame.width / 4
        self.preveius = self.lblPromocodeDetails.text
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("textFieldShouldBeginEditing")
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        print("textFieldDidBeginEditingEditing")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("shouldChangeCharactersIn")
        return true
    }
    
    @IBAction func txtFieldPC1(_ sender: UITextField) {
        
            if (txtFieldPromocod1.text?.count)! <= 0 {
                txtFieldPromocod1.becomeFirstResponder()
            }else if (txtFieldPromocod1.text?.count)! == 1 {
                txtFieldPromocod2.becomeFirstResponder()
            }else {
                txtFieldPromocod1.text?.removeLast()
                txtFieldPromocod2.becomeFirstResponder()
                copyPromoPast(promo: UIPasteboard.general.string ?? "\(sender.text ?? "")")
            }
        
            
    }
    
    @IBAction func txtFieldPC2(_ sender: UITextField) {
            
            if (txtFieldPromocod2.text?.count)! <= 0 {
                txtFieldPromocod1.becomeFirstResponder()
            }else if (txtFieldPromocod2.text?.count)! == 1 {
                txtFieldPromocod3.becomeFirstResponder()
            }else {
                txtFieldPromocod2.text?.removeLast()
                txtFieldPromocod3.becomeFirstResponder()
                copyPromoPast(promo: UIPasteboard.general.string ?? "\(sender.text ?? "")")
            }
    }
    
    @IBAction func txtFieldPC3(_ sender: UITextField) {
        
            if (txtFieldPromocod3.text?.count)! <= 0 {
                txtFieldPromocod2.becomeFirstResponder()
            }else if (txtFieldPromocod3.text?.count)! == 1 {
                txtFieldPromocod4.becomeFirstResponder()
            }else {
                txtFieldPromocod3.text?.removeLast()
                txtFieldPromocod4.becomeFirstResponder()
                copyPromoPast(promo: UIPasteboard.general.string ?? "\(sender.text ?? "")")
            }
    }
    
    @IBAction func txtFieldPC4(_ sender: UITextField) {
        
            if (txtFieldPromocod4.text?.count)! <= 0 {
                txtFieldPromocod3.becomeFirstResponder()
            }else if (txtFieldPromocod4.text?.count)! == 1 {
                txtFieldPromocod5.becomeFirstResponder()
            }else {
                txtFieldPromocod4.text?.removeLast()
                txtFieldPromocod5.becomeFirstResponder()
                copyPromoPast(promo: UIPasteboard.general.string ?? "\(sender.text ?? "")")
            }
    }
    
    @IBAction func txtFieldPC5(_ sender: UITextField) {
        
        
            if (txtFieldPromocod5.text?.count)! <= 0 {
                txtFieldPromocod4.becomeFirstResponder()
            }else if (txtFieldPromocod5.text?.count)! == 1 {
                txtFieldPromocod6.becomeFirstResponder()
            }else {
                txtFieldPromocod5.text?.removeLast()
                txtFieldPromocod6.becomeFirstResponder()
                copyPromoPast(promo: UIPasteboard.general.string ?? "\(sender.text ?? "")")
            }
    }
    
    @IBAction func txtFieldPC6(_ sender: UITextField) {

            if (txtFieldPromocod6.text?.count)! <= 0 {
                txtFieldPromocod5.becomeFirstResponder()
            }else if (txtFieldPromocod6.text?.count)! == 1 {
                txtFieldPromocod6.resignFirstResponder()
            }else {
                txtFieldPromocod6.text?.removeLast()
                txtFieldPromocod6.resignFirstResponder()
                copyPromoPast(promo: UIPasteboard.general.string ?? "\(sender.text ?? "")")
            }

    }
    
    @IBAction func btnCopyPromocode(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.constraintCodeCopiedtoClipboardTop.constant = 0
            self.view.layoutIfNeeded()
        }) { (_) in
            UIView.animate(withDuration: 0.3, delay: 2, options: .curveLinear, animations: {
                self.constraintCodeCopiedtoClipboardTop.constant = -60
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
        UIPasteboard.general.string = nsuserDefault.getUserDetails().promoCode
    }
    
    
    
    
    @IBAction func btnApply(_ sender: UIButton) {
        
        let promo = self.setOfPromocode()
        print(promo.count)
        
        if promo.count == 6 {
           
            self.promoApplyWebService(promo)
            //if (nsuserDefault.getUserDetails().isPromoCode == "yes") {
                
           // }else {
            //    appDelegate.showToast(title: "", message: "promo code already applied")
            //}
            
        }else {
            viewPromocode1.backgroundColor = .red
            viewPromocode2.backgroundColor = .red
            viewPromocode3.backgroundColor = .red
            viewPromocode4.backgroundColor = .red
            viewPromocode5.backgroundColor = .red
            viewPromocode6.backgroundColor = .red
        }
    }
    
    @IBAction func viewValidPromocodeHide(_ sender: UIButton) {
        self.viewValidPromocode.isHidden = true
        txtFieldPromocod1.text = ""
        txtFieldPromocod2.text = ""
        txtFieldPromocod3.text = ""
        txtFieldPromocod4.text = ""
        txtFieldPromocod5.text = ""
        txtFieldPromocod6.text = ""
        
        viewPromocode1.backgroundColor = .black
        viewPromocode2.backgroundColor = .black
        viewPromocode3.backgroundColor = .black
        viewPromocode4.backgroundColor = .black
        viewPromocode5.backgroundColor = .black
        viewPromocode6.backgroundColor = .black
        
        lblPromocodeDetails.text = preveius
    }
    
    
    @IBAction func btnSubscriptions(_ sender: UIButton) {
        self.performSegue(withIdentifier: "subscriptionsSegue", sender: self)
    }
    
    @IBAction func btnNavigationPopUp(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    fileprivate func promoApplyWebService(_ promo: String) {
        appDelegate.showHUD()
        
        let url = BASE_URL + API_APPLY_PROMOCODE
        let id = nsuserDefault.getUserDetails().userId
        
        let param = ["user_id" : id!,
                     "promo_code" : promo]
        
        if appDelegate.connected() == true {
            
            ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, status) in
                
                appDelegate.hideHUD()
                
                if status == nil
                {
                    let res = response
                    
                    if res?.value(forKey: "status") as! Bool == true
                    {
                        let data = res?.object(forKey: "data") as? [String:Any]
                        nsuserDefault.setUserDetails(logindata: LoginClass(fromDictionary: data!))
                        
                        print(data!["days"] as? String ?? "")
                        
                        if (data!["days"] as? String ?? "") == "0" {
                            appDelegate.showToast(title: "Successful", message: res!.value(forKey: "message") as! String)
                        }else {
                            self.promoCodeValid(status: true)
                            appDelegate.showToast(title: "Successful", message: res!.value(forKey: "message") as! String)
                            self.lblMessagePromo.text = "\(res!.value(forKey: "message") as? String ?? "")"
                            self.lblPromoFreeDays.text = "Free for \(data!["days"] as? String ?? "") days"
                        }
                        
                        let currentDT = Date()
                        let dateF = DateFormatter()
                        dateF.dateFormat = "hh:mm a"
                        let time = dateF.string(from: currentDT)
                        dateF.dateFormat = "dd-MM-yyyy"
                        let date = dateF.string(from: currentDT)
                        
                        let payloadDict = NSMutableDictionary()
                        payloadDict["user_email"] = nsuserDefault.getUserDetails().email ?? ""
                        payloadDict["user_name"]  = nsuserDefault.getUserDetails().name ?? ""
                        payloadDict["promo_code"] = promo
                        payloadDict["time"]  = time
                        payloadDict["date"]  = date
                        payloadDict["user_latitude"] = USERDEFAULT.value(forKey: "latitude") as? String ?? ""
                        payloadDict["user_longitude"] = USERDEFAULT.value(forKey: "longitude") as? String ?? ""
                        
                        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Apply Promo Code", payload: payloadDict, block: nil)
                        
                    }
                    else
                    {
                        self.promoCodeValid(status: false)
                        appDelegate.showToast(title: "Error", message: res!.value(forKey: "message") as! String)
                        
                    }
                }
                else
                {
                    appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
                }
            }
        }else {
            appDelegate.hideHUD()
        }
    }
    
    func promoCodeValid(status : Bool ) {
        if status == true {
            self.viewValidPromocode.isHidden = false
            txtFieldPromocod1.text = ""
            txtFieldPromocod2.text = ""
            txtFieldPromocod3.text = ""
            txtFieldPromocod4.text = ""
            txtFieldPromocod5.text = ""
            txtFieldPromocod6.text = ""
        }else {
            lblPromocodeDetails.text = "Invalid Code"
            lblPromocodeDetails.textColor = .red
            
            viewPromocode1.backgroundColor = .red
            viewPromocode2.backgroundColor = .red
            viewPromocode3.backgroundColor = .red
            viewPromocode4.backgroundColor = .red
            viewPromocode5.backgroundColor = .red
            viewPromocode6.backgroundColor = .red
        }
    }
    
    
    
    func setOfPromocode() -> String {
        let p1 = txtFieldPromocod1.text ?? ""
        let p2 = txtFieldPromocod2.text ?? ""
        let p3 = txtFieldPromocod3.text ?? ""
        let p4 = txtFieldPromocod4.text ?? ""
        let p5 = txtFieldPromocod5.text ?? ""
        let p6 = txtFieldPromocod6.text ?? ""
        let promocode = p1 + p2 + p3 + p4 + p5 + p6
        return promocode
    }
    
    
    func copyPromoPast(promo:String) {
        
        if promo.count == 6 {
            txtFieldPromocod1.text = String(Array(promo)[0])
            txtFieldPromocod2.text = String(Array(promo)[1])
            txtFieldPromocod3.text = String(Array(promo)[2])
            txtFieldPromocod4.text = String(Array(promo)[3])
            txtFieldPromocod5.text = String(Array(promo)[4])
            txtFieldPromocod6.text = String(Array(promo)[5])
        }else if promo.count == 1 {
            txtFieldPromocod1.text = String(Array(promo)[0])
        }else if promo.count == 2 {
            txtFieldPromocod1.text = String(Array(promo)[0])
            txtFieldPromocod2.text = String(Array(promo)[1])
        }else if promo.count == 3 {
            txtFieldPromocod1.text = String(Array(promo)[0])
            txtFieldPromocod2.text = String(Array(promo)[1])
            txtFieldPromocod3.text = String(Array(promo)[2])
        }else if promo.count == 4 {
            txtFieldPromocod1.text = String(Array(promo)[0])
            txtFieldPromocod2.text = String(Array(promo)[1])
            txtFieldPromocod3.text = String(Array(promo)[2])
            txtFieldPromocod4.text = String(Array(promo)[3])
        }else if promo.count == 5 {
            txtFieldPromocod1.text = String(Array(promo)[0])
            txtFieldPromocod2.text = String(Array(promo)[1])
            txtFieldPromocod3.text = String(Array(promo)[2])
            txtFieldPromocod4.text = String(Array(promo)[3])
            txtFieldPromocod5.text = String(Array(promo)[4])
        }else {
            txtFieldPromocod1.text = ""
            txtFieldPromocod2.text = ""
            txtFieldPromocod3.text = ""
            txtFieldPromocod4.text = ""
            txtFieldPromocod5.text = ""
            txtFieldPromocod6.text = ""
        }
    }
}


extension PromotionVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtFieldPromocod1.resignFirstResponder()
        txtFieldPromocod2.resignFirstResponder()
        txtFieldPromocod3.resignFirstResponder()
        txtFieldPromocod4.resignFirstResponder()
        txtFieldPromocod5.resignFirstResponder()
        txtFieldPromocod6.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
}
