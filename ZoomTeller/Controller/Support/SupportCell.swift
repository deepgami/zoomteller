//
//  SupportCell.swift
//  ZoomTeller
//
//  Created by Deep Gami on 14/06/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit

class SupportCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgSideArrow: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
