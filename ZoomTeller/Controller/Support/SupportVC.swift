//
//  SupportVC.swift
//  ZoomTeller
//
//  Created by Deep Gami on 14/06/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import NetCorePush

class SupportVC: UIViewController {
    
    @IBOutlet weak var tblSupport: UITableView!
    var arrForSupportText : NSMutableArray = NSMutableArray()

    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        self.tblSupport.sectionHeaderHeight = 50
        self.arrForSupportText = ["Account", "Editing Profile information" , "Getting Zoom Teller Newslatter", "Requesting Your Data", "Collecting, Proccessing Personal Data", "Deleting Your Account", "Using Zoom Teller", "Having a Technical Issue", "Estimated Transaction Time", "Changing Current Location", "Understanding Queue Identicator", "Using Filters", "Subscription And Promo", "Issue With Promotion", "Using a Promo Code", "Setting a Plan", "Issue With Payment"]
        tblSupport.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "Support Activity Open", payload: payloadDict, block: nil)
        
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
}

// MARK: - UITableViewDelegate method


extension SupportVC : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  17
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //jj
        
        let cell = tblSupport.dequeueReusableCell(withIdentifier:  "SupportCell", for: indexPath) as! SupportCell
        
        cell.selectionStyle = .none;
        
        if(indexPath.row == 0 || indexPath.row == 6 || indexPath.row == 12)
        {
            cell.backgroundColor = light_grey
            cell.imgSideArrow.isHidden = true
            cell.lblTitle.textColor = slate_grey
            cell.lblTitle.font = UIFont(name:"Arial",size:14)

        }
        else
        {
            cell.backgroundColor = .white
            cell.imgSideArrow.isHidden = false
            cell.lblTitle.textColor = charcol_grey
            cell.lblTitle.font = UIFont(name:"Arial",size:16)

        }
        cell.lblTitle.text = self.arrForSupportText[indexPath.row] as? String
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let type = self.arrForSupportText[indexPath.row] as? String ?? ""
        
        if type == "Account" {
            ////////////
        }else if type == "Editing Profile information" {
            if let url = URL(string: "http://zoomteller.com/support#editprofile") { 
                UIApplication.shared.open(url)
            }
        }else if type == "Getting Zoom Teller Newslatter" {
            if let url = URL(string: "http://zoomteller.com/support#getnewsletter") {
                UIApplication.shared.open(url)
            }
        }else if type == "Requesting Your Data" {
            if let url = URL(string: "http://zoomteller.com/support#requestdata") {
                UIApplication.shared.open(url)
            }
        }else if type == "Collecting, Proccessing Personal Data" {
            if let url = URL(string: "http://zoomteller.com/support#collectprocessdata") {
                UIApplication.shared.open(url)
            }
        }else if type == "Deleting Your Account" {
            if let url = URL(string: "http://zoomteller.com/support#deleteaccount") {
                UIApplication.shared.open(url)
            }
        }else if type == "Using Zoom Teller" {
             ////////////
        }else if type == "Having a Technical Issue" {
            if let url = URL(string: "http://zoomteller.com/support#techissues") {
                UIApplication.shared.open(url)
            }
        }else if type == "Estimated Transaction Time" {
            if let url = URL(string: "http://zoomteller.com/support#ett") {
                UIApplication.shared.open(url)
            }
        }else if type == "Changing Current Location" {
            if let url = URL(string: "http://zoomteller.com/support#changelocation") {
                UIApplication.shared.open(url)
            }
        }else if type == "Understanding Queue Identicator" {
            if let url = URL(string: "http://zoomteller.com/support#queueindicator") {
                UIApplication.shared.open(url)
            }
        }else if type == "Using Filters" {
            if let url = URL(string: "http://zoomteller.com/support#filters") {
                UIApplication.shared.open(url)
            }
        }else if type == "Subscription And Promo" {
             ////////////
        }else if type == "Issue With Promotion" {
            if let url = URL(string: "http://zoomteller.com/support#promotionissues") {
                UIApplication.shared.open(url)
            }
        }else if type == "Using a Promo Code" {
            if let url = URL(string: "http://zoomteller.com/support#promocode") {
                UIApplication.shared.open(url)
            }
        }else if type == "Setting a Plan" {
            if let url = URL(string: "http://zoomteller.com/support#setplan") {
                UIApplication.shared.open(url)
            }
        }else if type == "Issue With Payment" {
            if let url = URL(string: "http://zoomteller.com/support#paymentissues") {
                UIApplication.shared.open(url)
            }
        }else  {
            
        }
        
    }
    
    
}

//"Account", "Editing Profile information" , "Getting Zoom Teller Newslatter", "Requesting Your Data", "Collecting, Proccessing Personal Data", "Deleting Your Account", "Using Zoom Teller", "Having a Technical Issue", "Estimated Transaction Time", "Changing Current Location", "Understanding Queue Identicator", "Using Filters", "Subscription And Promo", "Issue With Promotion", "Using a Promo Code", "Setting a Plan", "Issue With Payment"

//zoomteller.com/support#editprofile
//
//zoomteller.com/support#getnewsletter
//
//zoomteller.com/support#requestdata
//
//zoomteller.com/support#collectprocessdata
//
//zoomteller.com/support#deleteaccount
//
//zoomteller.com/support#techissues
//
//zoomteller.com/support#ett
//
//zoomteller.com/support#changelocation
//
//zoomteller.com/support#queueindicator
//
//zoomteller.com/support#filters
//
//zoomteller.com/support#promotionissues
//
//zoomteller.com/support#promocode
//
//zoomteller.com/support#setplan
//
//zoomteller.com/support#paymentissues
//
//
//About Us
//
//zoomteller.com/about
