//
//  HistoryDetailVC.swift
//  ZoomTeller
//
//  Created by Deep Gami on 12/06/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import AVFoundation
import Polyline
import Alamofire
import NetCorePush

class HistoryDetailVC: UIViewController {

    var dictForHistory : NSDictionary = NSDictionary()

    @IBOutlet var atmMapRoute: GMSMapView!
    var poly = GMSPolyline()
    let currentMarker = GMSMarker()

    
    var markers = [GMSMarker]()

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblSource: UILabel!
    @IBOutlet weak var lblDestination: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        print(dictForHistory)

       
        
        let strDate = (dictForHistory["createdAt"] as? String)!
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateH = dateFormatter.date(from: strDate)!
        dateFormatter.timeZone = TimeZone.current
        
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        let now = df.string(from: dateH)
        
        
        let dft = DateFormatter()
        dft.dateFormat = "h:mm a"
        let nowt = dft.string(from: dateH)
        
        self.lblDistance.text = (dictForHistory["km"] as? String)! + "." + nowt

        self.lblDate.text = now

        let strSLati = (dictForHistory["source_lat"] as? String)!
        let strSLongi = (dictForHistory["source_long"] as? String)!
        let strDLati = (dictForHistory["destination_lat"] as? String)!
        let strDLongi = (dictForHistory["destination_long"] as? String)!

        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Double(dictForHistory["source_lat"] as! String)!,Double(dictForHistory["source_long"] as! String)!)
        
        var currentAddress = String()
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                currentAddress = lines.joined(separator: "\n")
                self.lblSource.text = currentAddress
            }
        }

        self.lblTitle.text = dictForHistory["atm_type"] as? String
        self.lblDestination.text = dictForHistory["atm_address"] as? String
        

        
          self.getPolylineRoute(from: CLLocationCoordinate2D(latitude: Double(strSLati)!, longitude: Double(strSLongi)!), to: CLLocationCoordinate2D(latitude: Double(strDLati)!, longitude: Double(strDLongi)!))
        putATMMarkersOnMap()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "History Detail Activity Open", payload: payloadDict, block: nil)
        
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func putATMMarkersOnMap() {
        
        
        let strSLati = (dictForHistory["source_lat"] as? String)!
        let strSLongi = (dictForHistory["source_long"] as? String)!
        let strDLati = (dictForHistory["destination_lat"] as? String)!
        let strDLongi = (dictForHistory["destination_long"] as? String)!
        
        
        
        let locDesti = CLLocation(latitude: Double(strDLati)!, longitude: Double(strDLongi)!)
        
        
        let locSource = CLLocation(latitude: Double(strSLati)!, longitude: Double(strSLongi)!)
        
        ///////////////////
        ///////////////////
        ///////////////////
        ///////////////////
        
        let marker = GMSMarker()
        
        marker.position = CLLocationCoordinate2D(latitude: locDesti.coordinate.latitude, longitude: locDesti.coordinate.longitude)
        
        let myView = UIView(frame: CGRect(x: 0, y: 0, width:100, height: 50))
        myView.backgroundColor = .clear
        
        let myImageView = UIImageView(frame: CGRect(x: 0, y: 0, width:100, height: 50))
        myImageView.image = UIImage(named: "infowindow_purple")
        
        
        let atmIcon = UIImageView(frame: CGRect(x: 17, y: 10, width:15, height: 15))
        atmIcon.image = UIImage(named: "atmIcon_white")
        
        let label = UILabel(frame: CGRect(x: 35, y: 8, width: 50, height: 20))
        label.textAlignment = .center
        label.textColor = .white
        label.text = dictForHistory["atm_type"] as? String
        
        
        let labelQueue = UILabel(frame: CGRect(x: 20, y: 27, width: 40, height: 15))
        labelQueue.textAlignment = .right
        labelQueue.textColor = .white
        labelQueue.font = UIFont(name: "Arial", size: 10)
        labelQueue.text = "Queue : "
        
        let queueIcon = UIImageView(frame: CGRect(x: 65, y: 30, width:10, height: 10))
        
        let strQueue = dictForHistory["queue"] as? String
        
        if(strQueue  == "green")
        {
            queueIcon.image = UIImage(named: "green")!
        }
        else if(strQueue == "red")
        {
            queueIcon.image = UIImage(named: "red")!
        }
        else
        {
            queueIcon.image = UIImage(named: "orange")!
        }
        
        
        
        myView.addSubview(myImageView)
        myView.addSubview(atmIcon)
        myView.addSubview(label)
        myView.addSubview(labelQueue)
        myView.addSubview(queueIcon)
        
        marker.iconView = myView
        marker.map = self.atmMapRoute
        
        
        ///////////////////
        ///////////////////
        ///////////////////
        ///////////////////
        
        
        
        let markerImage = UIImage(named: "myself")!.withRenderingMode(.alwaysOriginal)
        
        
        let markerView = UIImageView(frame: CGRect(x: -30, y: -25, width:59, height: 56))
        //markerView.backgroundColor = .black
        markerView.image = markerImage
        currentMarker.position = CLLocationCoordinate2D(latitude: locSource.coordinate.latitude, longitude: locSource.coordinate.longitude)
        
        currentMarker.iconView = markerView
        
        currentMarker.map = self.atmMapRoute
        markers.append(currentMarker)
        
        markers.append(marker)
        
      
            var bounds = GMSCoordinateBounds()
            for marker in markers
            {
                bounds = bounds.includingCoordinate(marker.position)
            }
            let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
            self.atmMapRoute.animate(with: update)
        
    }
    
    //MARK: - Draw Path
    
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        
        let origin = "\(source.latitude),\(source.longitude)"
        let dest = "\(destination.latitude),\(destination.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(dest)&mode=walking&key=\(appDelegate.googleApiKey)"
        
        Alamofire.request(url).responseJSON { response in
            
            do
            {
                if let json : [String:Any] = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String: Any]
                {
                    
                    guard let routes = json["routes"] as? NSArray else {
                        
                        DispatchQueue.main.async {
                            print("Error occurs , Didn't get Routes")
                        }
                        return
                    }
                    
                    for route in routes
                    { let overview_polyline = route as? NSDictionary
                        
                        let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                        let points = dictPolyline?.object(forKey: "points") as? String
                        
                        let path = GMSPath.init(fromEncodedPath: points!)
                        let distanceInMeters = path?.length(of: .geodesic)
                        
                      
                        self.poly = GMSPolyline(path: path)
                        self.poly.strokeColor = map_route_color
                        self.poly.strokeWidth = 8.0
                        self.poly.map = self.atmMapRoute
                        
                        
                                }}
            }
            catch
            {
                print("Error in JSONSerialization")
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
