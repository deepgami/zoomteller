//
//  HistoryListVC.swift
//  ZoomTeller
//
//  Created by Deep Gami on 11/06/19.
//  Copyright © 2019 ZERONES. All rights reserved.
//

import UIKit
import NetCorePush

class HistoryListVC: UIViewController {

    var arrForHistory : NSMutableArray = NSMutableArray()

    var dictForHistory : NSDictionary = NSDictionary()

    @IBOutlet weak var tblHistory: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        getMyHistory()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let payloadDict = NSMutableDictionary()
        payloadDict["email"] = nsuserDefault.getUserDetails().email ?? ""
        NetCoreAppTracking.sharedInstance()?.trackEvent(withCustomPayload: "History Activity Open", payload: payloadDict, block: nil)
        
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    // MARK: - APIs method
    
    func getMyHistory() {
        
        
        appDelegate.showHUD()

        
        let url = BASE_URL + API_GET_HISTORY
        let userDetail = nsuserDefault.getUserDetails()
        
        let param = ["user_id":userDetail.userId as String]
        
        ApiHelper.sharedInstance.CallWebservice(url: url, param: param) { (response, status) in
            
            appDelegate.hideHUD()
            
            if status == nil
            {
                let res = response
                if res?.value(forKey: "status") as! Bool == true
                {
                    let tagArray = res!["data"] as! NSArray
                    self.arrForHistory = NSMutableArray(array:tagArray)
                    
                    self.tblHistory.reloadData()
                }
                else
                {
                    appDelegate.showToast(title: "Error", message: res!.value(forKey: "message") as! String)
                    
                }
            }
            else
            {
                appDelegate.showToast(title: "Warning", message: "Something went wrong.!")
            }
        }
    }
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueToHistoryDetail"
        {
            let dest = segue.destination as! HistoryDetailVC
            
            dest.dictForHistory = self.dictForHistory
           
            
        }
    }
    

}



// MARK: - UITableViewDelegate method


extension HistoryListVC : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.arrForHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //jj
        
        let cell = tblHistory.dequeueReusableCell(withIdentifier:  "HistoryCell", for: indexPath) as! HistoryCell
        
        cell.selectionStyle = .none;

        
        let dictData = self.arrForHistory.object(at: indexPath.row) as! NSDictionary
        
        
        cell.lblTitle.text = dictData["atm_type"] as? String
        cell.lblAddress.text = dictData["atm_address"] as? String
        let strDate = (dictData["createdAt"] as? String)!

        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateH = dateFormatter.date(from: strDate)!
        dateFormatter.timeZone = TimeZone.current
        
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy, hh:mm a"
        let now = df.string(from: dateH)
        
        cell.lblTime.text = now

        return cell
    }
    
  
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        dictForHistory = self.arrForHistory.object(at: indexPath.row) as! NSDictionary
        print(dictForHistory)
        self.performSegue(withIdentifier: "segueToHistoryDetail", sender: self)

    }
   
}
